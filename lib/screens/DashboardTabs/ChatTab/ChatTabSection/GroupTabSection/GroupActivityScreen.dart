import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/GroupChatScreen.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/GroupEventsScreen.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/GroupLiveStreamScreen.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/GroupProductsScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';

import 'AddEditEventListScreen.dart';
import 'AddEditProductListScreen.dart';
import 'GroupDetailsScreen.dart';

class GroupActivityScreen extends StatefulWidget {
  final String groupName, groupDp, groupId;

//  final GroupModel groupChatModel;

  GroupActivityScreen(
    this.groupName,
    this.groupDp,
    this.groupId,
    /*this.groupChatModel*/
  );

  @override
  _GroupActivityScreenState createState() => _GroupActivityScreenState();
}

class _GroupActivityScreenState extends State<GroupActivityScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            titleSpacing: widget.groupDp == null ? 0 : 0,
            backgroundColor: Colors.white,
            brightness: Brightness.light,
            elevation: 1,
            leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  child: IconButton(
                      icon: FaIcon(FontAwesomeIcons.chevronLeft,
                          color: kAppColor),
                      onPressed: () {
                        debugPrint("Pressed");
                        Navigator.of(context).pop();
                      }),
                ),
              ),
            ),
            title: Row(
              children: <Widget>[
                widget.groupDp != null
                    ? Container(
                        child: ClipOval(
                          child: Image.asset(
                            widget.groupDp,
                            width: 35,
                            height: 35,
                            fit: BoxFit.cover,
                          ),
                        ),
                      )
                    : Container(),
                Text(
                  widget.groupName,
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: kOkraMedium,
                      fontWeight: FontWeight.w500,
                      fontSize: 20),
                  overflow: TextOverflow.ellipsis,
                )
              ],
            ),
            bottom: TabBar(
                indicatorColor: kAppColor,
                indicatorWeight: 4.0,
                labelColor: kAppColor,
                unselectedLabelColor: Colors.grey,
                labelStyle: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    fontFamily: kOkraRegular),
                isScrollable: true,
                tabs: [
                  Tab(
                    text: "Chats",
                  ),
                  Tab(text: "Products"),
                  Tab(text: "Events"),
                  Tab(text: "Live "),
                ]),
            actions: <Widget>[
              PopupMenuButton(
                icon: Icon(
                  Icons.more_vert,
                  size: 30,
                  color: kAppColor,
                ),
                itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                  PopupMenuItem<String>(
                    value: 'add product',
                    child: Text(
                      'Add Product',
                      style: TextStyle(fontSize: 15, fontFamily: kOkraMedium),
                    ),
                  ),
                  PopupMenuItem<String>(
                    value: 'create event',
                    child: Text(
                      'Create Event',
                      style: TextStyle(fontSize: 15, fontFamily: kOkraMedium),
                    ),
                  ),
                  PopupMenuItem<String>(
                    value: 'group details',
                    child: Text(
                      'Group Details',
                      style: TextStyle(fontSize: 15, fontFamily: kOkraMedium),
                    ),
                  ),
                ],
                onSelected: (value) {
                  if (value == "group details") {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) => GroupDetailsScreen(
                                grpId: widget.groupId,
                              )),
                    );
                  } else if (value == "add product") {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              AddEditProductListScreen(
                                groupName: widget.groupName,
                                groupId: widget.groupId,
                              )),
                    );
                  } else if (value == "create event") {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              AddEditEventListScreen(
                                grpId: widget.groupId,
                              )),
                    );
                  }
                },
              ),
            ],
          ),
          body: TabBarView(
            children: [
              GroupChatScreen(),
              GroupProductsScreen(
                groupName: widget.groupName,
                groupDp: widget.groupDp,
                groupId: widget.groupId,
              ),
              GroupEventsScreen(),
              GroupLiveStreamScreen(),
            ],
          ),
        ),
      ),
    );
  }
}

class GroupActivityTabSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: DefaultTabController(
        length: 4,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: Image.asset(
              "assets/images/background.png",
            ).image),
          ),
          child: Scaffold(
            appBar: TabBar(
                indicatorColor: Colors.black,
                indicatorWeight: 3.0,
                unselectedLabelColor: Colors.grey,
                labelStyle: TextStyle(fontSize: 15, color: Colors.black),
                tabs: [
                  Tab(text: "Chat"),
                  Tab(text: "Products"),
                  Tab(text: "Events"),
                  Tab(text: "Live"),
                ]),
            body: TabBarView(
              children: [],
            ),
          ),
        ),
      ),
    );
  }
}
