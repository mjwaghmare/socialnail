class GroupModel {
  final int id;
  final String groupName;
  final String groupDescription;
  final String groupMember;
  final String createdDate;
  final String avatarUrl;
  final String groupRegistrationNumber;
  final String groupOption;
  final String groupType;
  final String numberOfMembersJoinFree;
  final String requestUserInformations;
  final String subscriptionsDetails;
  final String groupSubscription;
  final String groupTypeOf;
  final String requestUserProfile;
  final String verifyMyGroupStatusFiled;
  final String groupStatus;
  final int createdBy;

  GroupModel({
    this.id,
    this.groupName,
    this.groupDescription,
    this.groupMember,
    this.createdDate,
    this.avatarUrl,
    this.groupStatus,
    this.createdBy,
    this.groupRegistrationNumber,
    this.groupOption,
    this.groupType,
    this.numberOfMembersJoinFree,
    this.requestUserInformations,
    this.subscriptionsDetails,
    this.groupSubscription,
    this.groupTypeOf,
    this.requestUserProfile,
    this.verifyMyGroupStatusFiled,
  });

  factory GroupModel.fromJson(Map<String, dynamic> json) {
    return GroupModel(
      id: json['id'],
      groupName: json['groupName'],
      groupDescription: json['groupDescription'],
      groupMember: json['groupMember'],
      createdDate: json['createdDate'],
      avatarUrl: json['groupImage'],
      groupStatus: json['groupStatus'],
      createdBy: json['createdBy'],
      groupRegistrationNumber: json['groupRegistrationNumber'],
      groupOption: json['groupOption'],
      groupType: json['groupType'],
      numberOfMembersJoinFree: json['numberOfMembersJoinFree'],
      requestUserInformations: json['requestUserInformations'],
      subscriptionsDetails: json['subscriptionsDetails'],
      groupSubscription: json['groupSubscription'],
      groupTypeOf: json['groupTypeOf'],
      requestUserProfile: json['requestUserProfile'],
      verifyMyGroupStatusFiled: json['verifyMyGroupStatusFiled'],
    );
  }
}

// "id": 1,
//         "groupName": "Test1",
//         "groupDescription": "dsgbfsj",
//         "groupMember": "Dhanaji,Avinash,Akshay",
//         "groupStatus": "True",
//         "createdDate": "2020-03-23T04:00:33-07:00",
//         "createdBy": 1
