import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:micro_ecommerce/utils/constants.dart';

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar({Key key}) : super(key: key);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();

  @override
  Size get preferredSize => null;
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      height: 70,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Jennifer Anniston"),
          actions: <Widget>[
            IconButton(
                icon: FaIcon(
                  FontAwesomeIcons.search,
                  color: kAppColor,
                  size: 20,
                ),
                onPressed: () {
                  debugPrint("Pressed");
                }),
            IconButton(
                icon: FaIcon(
                  FontAwesomeIcons.ellipsisV,
                  color: kAppColor,
                  size: 20,
                ),
                onPressed: () {
                  debugPrint("Pressed");
                }),
          ],
        ),
      ),
    );
  }
}
