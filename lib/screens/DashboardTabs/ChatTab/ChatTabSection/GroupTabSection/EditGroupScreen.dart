import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:micro_ecommerce/models/UserModel.dart';
import 'package:micro_ecommerce/models/group_model.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/GroupDetailsScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomSingleButton.dart';

import 'AddMembersToGroupScreen.dart';

class EditGroupScreen extends StatefulWidget {
  final String grpId;

  EditGroupScreen({Key key, this.grpId}) : super();

  @override
  _EditGroupScreenState createState() => _EditGroupScreenState();
}

class _EditGroupScreenState extends State<EditGroupScreen> {
  List<GroupModel> groupListData = [];

  var _groupOption = ['Group Option', 'Public', 'Private'];
  var _currentGroupOption = 'Group Option';

  //Group Type
  var _groupType = ['Group Type', 'Free', 'Premium'];
  var _currentGroupType = 'Group Type';

  //Free Members
  var _groupFreeMembers = ['Select Free Members', '1', '2'];
  var _currentGroupFreeMembers = 'Select Free Members';

  //Request user info
  var _groupRequestUserInfo = ['Request User Info', 'Dob', 'email'];
  var _currentRequestUserInfo = 'Request User Info';

  var newIds;

  //Group Details
  String grpName,
      fileName,
      selectedImgFile,
      grpDesc,
      grpRegNo,
      grpOption,
      grpType,
      grpFreeMembers,
      grpReqUserInfo,
      grpSubsDetails;
  var _imageName, _imageString;

  bool loading = false;
  bool editGroupLoading = false;

  TextEditingController grpNameController = new TextEditingController();
  TextEditingController grpDescriptionController = new TextEditingController();

  List<UserModel> userListData = [];
  List<UserModel> newMembersData = [];

  @override
  void initState() {
    _getGroupDetails(widget.grpId);
    super.initState();
  }

  Future<void> _getGroupDetails(String grpId) async {
    setState(() {
      loading = true;
    });
    debugPrint(grpId);
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/Groupdetails/GroupDetailsbyId";

    //send the userId of admin
    Map<String, dynamic> data = {
      "Id": widget.grpId,
    };

    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body.replaceAll("ï»¿", ""));
      debugPrint(data.toString()); //response
      for (Map i in data) {
        groupListData.add(GroupModel.fromJson(i));
      }
      _getDetailsFromApi(groupListData);
      setState(() {
        loading = false;
      });
    } else {
      Fluttertoast.showToast(msg: "Error while fetching details");
      debugPrint(response.statusCode.toString());
    }
  }

  void _getDetailsFromApi(List<GroupModel> groupListData) {
    String grpMembers = "";
    for (var value in groupListData) {
      grpName = value.groupName;
      grpDesc = value.groupDescription;
      grpRegNo = value.groupRegistrationNumber;
      grpOption = value.groupOption;
      grpType = value.groupType;
      grpFreeMembers = value.numberOfMembersJoinFree;
      grpReqUserInfo = value.requestUserInformations;
      grpMembers = value.groupMember;
    }
    if (grpMembers == null || grpMembers == "") {
      return;
    } else {
      newIds = json.decode(grpMembers);
      _listOfMembers(newIds);
    }

    debugPrint("name- " + grpName);
    //setting values initially...
    grpNameController = TextEditingController(text: grpName);
    grpDescriptionController = TextEditingController(text: grpDesc);
    _currentGroupOption = grpOption;
    _currentGroupType = grpType;
    _currentGroupFreeMembers = grpFreeMembers;
    _currentRequestUserInfo = grpReqUserInfo;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet: editGroupLoading == false
          ? CustomSingleButton(
              title: "Edit Group",
              onTapped: _editGroup,
            )
          : Material(
              elevation: 5,
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                height: Platform.isIOS ? 70 : 60,
                child: Center(
                  child: Container(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(
                      backgroundColor: kAppColor,
                    ),
                  ),
                ),
              ),
            ),
      appBar: AppBar(
        titleSpacing: 0,
        elevation: 5,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.chevron_left,
            size: 35,
            color: kAppColor,
          ),
        ),
        title: Text(
          "Edit Group",
          style: TextStyle(
              fontSize: 18, color: kAppTextColor, fontFamily: kOkraSemiBold),
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            "assets/images/background.png",
            fit: BoxFit.fill,
          ),
          loading == true
              ? Center(
                  child: Container(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(
                      backgroundColor: kAppColor,
                    ),
                  ),
                )
              : SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Container(
                    margin: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Card(
                              elevation: 5,
                              color: Colors.white,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(4.0),
                                child: _imageName == null
                                    ? Image.network(
                                        "https://images.pexels.com/photos/807598/pexels-photo-807598.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
                                        fit: BoxFit.cover,
                                        height: 150.0,
                                        width:
                                            MediaQuery.of(context).size.width,
                                      )
                                    : Image.file(
                                        _imageName,
                                        fit: BoxFit.cover,
                                        height: 150.0,
                                        width:
                                            MediaQuery.of(context).size.width,
                                      ),
                              ),
                            ),
                            Positioned(
                              right: 10,
                              bottom: 10,
                              child: InkWell(
                                onTap: () {
                                  _getImage();
                                },
                                child: Material(
                                  color: Colors.transparent,
                                  elevation: 10,
                                  child: Icon(
                                    Icons.add_photo_alternate,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        //Group Name
                        SizedBox(height: 20),
                        TextFormField(
                          controller: grpNameController,
                          style: TextStyle(
                              fontFamily: kOkraMedium,
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 17),
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: kAppTextColor),
                            ),
                            labelText: "Group Name",
                            labelStyle: TextStyle(
                                fontFamily: kOkraMedium, color: Colors.grey),
                          ),
                        ),
                        //Group Description
                        SizedBox(height: 10),
                        TextFormField(
                          controller: grpDescriptionController,
                          style: TextStyle(
                              fontFamily: kOkraMedium,
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 17),
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: kAppTextColor),
                            ),
                            labelText: "Group Description",
                            labelStyle: TextStyle(
                                fontFamily: kOkraMedium, color: Colors.grey),
                          ),
                        ),
                        //Group Option
                        SizedBox(height: 20),
                        Text(
                          "Group Option",
                          style: TextStyle(
                              fontFamily: kOkraMedium,
                              fontSize: 17,
                              color: Colors.grey),
                        ),
                        groupOptionDropDown(),
                        //Group Type
                        SizedBox(height: 15),
                        Text(
                          "Group Type",
                          style: TextStyle(
                              fontFamily: kOkraMedium,
                              fontSize: 17,
                              color: Colors.grey),
                        ),
                        groupTypeDropDown(),
                        //Free Group Members
                        SizedBox(height: 15),
                        Text(
                          "Number of members join(Free)",
                          style: TextStyle(
                              fontFamily: kOkraMedium,
                              fontSize: 17,
                              color: Colors.grey),
                        ),
                        groupFreeMemberDropDown(),
                        //Request User Info
                        SizedBox(height: 15),
                        Text(
                          "Request user informations",
                          style: TextStyle(
                              fontFamily: kOkraMedium,
                              fontSize: 17,
                              color: Colors.grey),
                        ),
                        groupRequestInfoDropDown(),
                        SizedBox(height: 15),
                        participants(),
                        //List of Members
                        SizedBox(height: 15),
                        listOfUsers(newMembersData),
                        SizedBox(height: 50),
                      ],
                    ),
                  ),
                ),
        ],
      ),
    );
  }

  //Group Option
  Widget groupOptionDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupOption.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropGroupOptionDownItemSelected(value);
        },
        value: _currentGroupOption,
      ),
    );
  }

  void _dropGroupOptionDownItemSelected(String value) {
    setState(() {
      this._currentGroupOption = value;
    });
  }

  //Group Type
  Widget groupTypeDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupType.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropGroupTypeDownItemSelected(value);
        },
        value: _currentGroupType,
      ),
    );
  }

  void _dropGroupTypeDownItemSelected(String value) {
    setState(() {
      this._currentGroupType = value;
    });
  }

  //Group Free Member
  Widget groupFreeMemberDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupFreeMembers.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropGroupFreeMemberDownItemSelected(value);
        },
        value: _currentGroupFreeMembers,
      ),
    );
  }

  void _dropGroupFreeMemberDownItemSelected(String value) {
    setState(() {
      this._currentGroupFreeMembers = value;
    });
  }

  //Request userInfo
  Widget groupRequestInfoDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupRequestUserInfo.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropDownRequestInfoItemSelected(value);
        },
        value: _currentRequestUserInfo,
      ),
    );
  }

  void _dropDownRequestInfoItemSelected(String value) {
    setState(() {
      this._currentRequestUserInfo = value;
    });
  }

  //Participants
  Widget participants() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            "Participants List",
            style: TextStyle(
                fontFamily: kOkraSemiBold, color: Colors.black, fontSize: 17),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          AddMembersToGroupScreen(
                        grpId: widget.grpId,
                      ),
                    ),
                  );
                },
                child: RichText(
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Icon(
                          Icons.person_add,
                          size: 17,
                          color: kAppColor,
                        ),
                      ),
                      TextSpan(
                        text: " Add Members",
                        style: TextStyle(
                            fontFamily: kOkraSemiBold,
                            color: Colors.black,
                            fontSize: 12),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 10),
              GestureDetector(
                onTap: () {
                  Fluttertoast.showToast(
                      msg: "More Option", gravity: ToastGravity.CENTER);
                },
                child: RichText(
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Center(
                          child: Icon(
                            Icons.more_horiz,
                            size: 18,
                            color: kAppColor,
                          ),
                        ),
                      ),
                      TextSpan(
                        text: "More",
                        style: TextStyle(
                            fontFamily: kOkraSemiBold,
                            color: Colors.black,
                            fontSize: 12),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  //Edit Group
  Future<void> _editGroup() async {
    setState(() {
      editGroupLoading = true;
    });
    String groupName = grpNameController.text;
    String groupDesc = grpDescriptionController.text;
    setState(() {
      grpOption = _currentGroupOption;
      grpType = _currentGroupType;
      grpFreeMembers = _currentGroupFreeMembers;
      grpReqUserInfo = _currentRequestUserInfo;
    });

    if (grpName.isEmpty ||
        grpOption == "Group Option" ||
        grpType == "Group Type" ||
        grpFreeMembers == "Select Free Members" ||
        grpReqUserInfo == "Request User Info") {
      Fluttertoast.showToast(
          backgroundColor: kAppColor,
          textColor: Colors.white,
          msg: "Please complete all fields",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2);
      return;
    } else {
      const headers = {'Content-Type': 'application/json'};
      String url =
          "http://microecommerce.flicklead.com/api/Groupdetails/EditGroup";
      if (_imageString != null) {
        Map<String, dynamic> data = {
          "id": widget.grpId.toString(),
          "groupName": groupName,
          "groupDescription": groupDesc,
          "groupOption": grpOption,
          "groupType": grpType,
          "numberOfMembersJoinFree": grpFreeMembers,
          "requestUserInformations": grpReqUserInfo,
          "groupImage": _imageString,
        };

        var response = await http.put(
          url,
          headers: headers,
          body: json.encode(data),
        );
        //1st api response

        if (response.statusCode == 200) {
          String urlImg =
              "https://microecommercephoto.flicklead.com/api/UserPhoto/savephoto";
          Map<String, dynamic> data = {
            "imgname": groupName,
            "img1": _imageString == null ? "" : _imageString,
          };

          var responseImg = await http.post(
            urlImg,
            headers: headers,
            body: json.encode(data),
          );

          //2nd api response
          if (responseImg.statusCode == 200) {
            setState(() {
              editGroupLoading = false;
            });

            Fluttertoast.showToast(msg: "Group Edited Successfully");
            Navigator.of(context).pop(
              MaterialPageRoute(
                builder: (BuildContext context) => GroupDetailsScreen(
                  grpId: widget.grpId,
                ),
              ),
            );
          } else {
            setState(() {
              editGroupLoading = false;
            });
          }
        }
      } else {
        Map<String, dynamic> data = {
          "id": widget.grpId.toString(),
          "groupName": groupName,
          "groupDescription": groupDesc,
          "groupOption": grpOption,
          "groupType": grpType,
          "numberOfMembersJoinFree": grpFreeMembers,
          "requestUserInformations": grpReqUserInfo,
        };

        var response = await http.put(
          url,
          headers: headers,
          body: json.encode(data),
        );

        //1st api response
        if (response.statusCode == 200) {
          String urlImg =
              "https://microecommercephoto.flicklead.com/api/GroupPhoto/savephoto";
          Map<String, dynamic> data = {
            "imgname": groupName,
            "img1": _imageString == null ? "" : _imageString,
          };

          var responseImg = await http.post(
            urlImg,
            headers: headers,
            body: json.encode(data),
          );
          //2nd api response
          if (responseImg.statusCode == 200) {
            setState(() {
              editGroupLoading = false;
            });

            Fluttertoast.showToast(msg: "Group Edited Successfully");
            Navigator.of(context).pop(
              MaterialPageRoute(
                builder: (BuildContext context) =>
                    GroupDetailsScreen(
                      grpId: widget.grpId,
                    ),
              ),
            );
          } else {
            setState(() {
              editGroupLoading = false;
            });
          }
        }
      }
    }
  }

//Image Picker
  Future _getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      File croppedImg = await ImageCropper.cropImage(
          sourcePath: image.path,
          maxHeight: 150,
          compressQuality: 100,
          maxWidth: MediaQuery.of(context).size.width.toInt(),
          compressFormat: ImageCompressFormat.jpg,
          androidUiSettings: AndroidUiSettings(
              toolbarColor: kAppColor,
              toolbarTitle: "SocialNail Image Cropper",
              backgroundColor: Colors.white,
              lockAspectRatio: false,
              showCropGrid: true),
          iosUiSettings: IOSUiSettings(
            minimumAspectRatio: 1.0,
          ));

      String base64Image = base64Encode(
        image.readAsBytesSync(),
      );
      fileName = image.path
          .split("/")
          .last;
      _imageString = base64Image;
      this.setState(
        () {
          _imageName = croppedImg;
        },
      );
    }
  }

  //getting members using Ids and adding in list
  _listOfMembers(newIds) async {
    if (newIds.length == 0) {
      return;
    } else {
      for (int i = 0; i < newIds.length; i++) {
        const headers = {'Content-Type': 'application/json'};
        String url =
            "https://microecommerce.flicklead.com/api/UserDetail/GetUserDetails";

        Map<String, dynamic> data = {
          "Id": newIds[i].toString(),
        };

        var response = await http.post(
          url,
          headers: headers,
          body: json.encode(data),
        );

        if (response.statusCode == 200) {
          final data = jsonDecode(response.body.replaceAll("ï»¿", ""));
          newMembersData.add(
            UserModel.map(data),
          );
        } else {
          Fluttertoast.showToast(
              msg: "Unable to load Group Members",
              backgroundColor: kAppColor,
              textColor: Colors.white);
        }
      }
    }
  }

  listOfUsers(List<UserModel> newMembersData) {
    if (newMembersData.length == 0) {
      return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.all(6),
        color: Colors.grey.withOpacity(0.10),
        width: MediaQuery.of(context).size.width,
        height: 40,
        child: Text(
          "No group members added...",
          style: TextStyle(fontFamily: kOkraMedium, fontSize: 16),
        ),
      );
    } else {
      if (newMembersData.length == newIds.length) {
        return Builder(
          builder: (context) {
            return Container(
              child: ListView.separated(
                primary: false,
                shrinkWrap: true,
                itemCount: newMembersData.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: CircleAvatar(
                      radius: 20,
                      backgroundColor: Colors.white,
                      child: newMembersData[index].photoId == null ||
                              newMembersData[index].photoId == ""
                          ? Image.asset(
                              "assets/images/male_avatar.png",
                              fit: BoxFit.cover,
                            )
                          : Image.network(newMembersData[index].photoId),
                      //todo  check img url here
                    ),
                    title: Text(
                      newMembersData[index].name,
                      style: TextStyle(fontFamily: kOkraSemiBold),
                    ),
                    subtitle: Text(
                      newMembersData[index].mobileNumber,
                      style: TextStyle(fontFamily: kOkraMedium),
                    ),
                  );
                },
                separatorBuilder: (context, index) => Divider(
                  color: Colors.grey,
                ),
              ),
            );
          },
        );
      } else if (newMembersData.length < newIds.length) {
        return Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.all(6),
          color: Colors.grey.withOpacity(0.10),
          width: MediaQuery.of(context).size.width,
          height: 40,
          child: Text(
            "Loading members ...",
            style: TextStyle(fontFamily: kOkraMedium, fontSize: 16),
          ),
        );
      }
    }
  }
}
