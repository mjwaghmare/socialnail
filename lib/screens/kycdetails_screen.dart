import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:micro_ecommerce/screens/login_screen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/ButtonBottomSheet.dart';
import 'package:micro_ecommerce/widgets/UploadImageWidget.dart';
import 'package:progress_dialog/progress_dialog.dart';

class KycScreen extends StatefulWidget {
  final String id;

  KycScreen({Key key, this.id}) : super(key: key);

  @override
  _KycScreenState createState() => _KycScreenState();
}

String _kycAadharName, _kycAadharNumber, _photoId, panAadhar;
TextEditingController photoIdController = new TextEditingController();
TextEditingController paController = new TextEditingController();
TextEditingController textEditingController = new TextEditingController();
String selectedFileName;

class _KycScreenState extends State<KycScreen> {
  ProgressDialog pr;
  File cameraFile, galleryFile;
  String photoIdfileName, photoId;
  String panAdharFileName, panAdharPhoto;
  final GlobalKey<FormState> _kycKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, isDismissible: false);
    pr.style(
      message: 'Please wait...',
      progressWidget: CircularProgressIndicator(),
      borderRadius: 8.0,
      backgroundColor: Colors.white,
      messageTextStyle: TextStyle(
          color: kAppColor, fontSize: 18.0, fontWeight: FontWeight.w500),
    );
    return Scaffold(
      bottomSheet: ButtonBottomSheet(
        title1: "Register",
        onTapped1: () {
          if (!_kycKey.currentState.validate()) {
            return;
          } else {
            _registerUsingKyc();
          }
        },
        title2: "Skip & Login",
        onTapped2: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => LoginScreen(),
            ),
          );
        },
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _kycKey,
            child: Stack(
              children: <Widget>[
                Positioned(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      image: DecorationImage(
                          image: Image.asset(
                        "assets/images/background.png",
                        fit: BoxFit.cover,
                      ).image),
                    ),
                  ),
                ),
                Positioned(
                  top: 15,
                  left: 20,
                  right: 20,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * .83,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0, 1),
                          blurRadius: 1.0,
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 20.0,
                  right: 20.0,
                  child: Image.asset("assets/images/logo.png"),
                ),
                Positioned(
                  top: 150.0,
                  left: 35.0,
                  right: 30.0,
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      children: <Widget>[
                        UploadDocumentWidget(
                          title: "Upload Photo",
                          onTap: showPhotoIdImagePicker,
                          nameOfController: photoIdController,
                          fileName: selectedFileName,
                          sourceName: "photoId",
                        ),
                        Divider(thickness: 1),
                        UploadDocumentWidget(
                          title: "PAN / Aadhar",
                          onTap: showAadharPanImagePicker,
                          nameOfController: paController,
                          fileName: selectedFileName,
                          sourceName: "panaadhar",
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                    top: 370.0,
                    left: 40.0,
                    right: 40.0,
                    child: sectionDocumentsDetails())
              ],
            ),
          ),
        ),
      ),
    );
  }

  sectionDocumentsDetails() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      margin: EdgeInsets.only(top: 10),
      child: Column(
        children: <Widget>[
          sectionAadharPANName("PAN / Aadhar Name", Icons.person_outline),
          SizedBox(height: 20.0),
          sectionAadharPANNumber("PAN / Aadhar Number", Icons.person_outline),
        ],
      ),
    );
  }

  TextEditingController nameController = new TextEditingController();
  TextEditingController numberController = new TextEditingController();

  sectionAadharPANName(String name, IconData person) {
    return TextFormField(
      validator: (String value) {
        if (value.isEmpty) {
          return "Aadhar/Pan Name is required";
        }
        return null;
      },
      onSaved: (String value) {
        _kycAadharName = value;
      },
      controller: nameController,
      style: TextStyle(
          color: Colors.black, fontWeight: FontWeight.w400, fontSize: 16),
      decoration: InputDecoration(
        labelText: name,
        prefixIcon: Icon(person),
        labelStyle: TextStyle(color: Colors.grey),
      ),
    );
  }

  sectionAadharPANNumber(String number, IconData dialpad) {
    return TextFormField(
      validator: (String value) {
        if (value.isEmpty) {
          return "Aadhar/Pan Number is required";
        }
        return null;
      },
      onSaved: (String value) {
        _kycAadharNumber = value;
      },
      controller: numberController,
      style: TextStyle(
          color: Colors.black, fontWeight: FontWeight.w400, fontSize: 16),
      decoration: InputDecoration(
        labelText: number,
        prefixIcon: Icon(dialpad),
        labelStyle: TextStyle(color: Colors.grey),
      ),
    );
  }

/*
  Container registerButton() {
    return Container(
      width: 200,
      height: 50,
      child: RaisedButton(
        elevation: 2,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(60.0),
        ),
        color: Colors.orange,
        textColor: Colors.white,
        child: Text(
          "Register Now",
          style: TextStyle(fontSize: 16),
        ),
        onPressed: () {
          pr.show();
          _registerUsingKyc();

          //Send Otp to the above entered mobile number and also send the user to OTP Screen.try
          */
/*Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => LoginScreen(),
            ),
          );*/ /*

        },
      ),
    );
  }
*/

/*
  skipAndRegisterButton() {
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      width: 200,
      height: 50,
      child: RaisedButton(
        elevation: 2,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(60.0),
        ),
        color: Colors.white,
        textColor: Colors.black87,
        child: Text(
          "Skip ",
          style: TextStyle(fontSize: 16),
        ),
        onPressed: () {
          //Send Otp to the above entered mobile number and also send the user to OTP Screen.try
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => LoginScreen(),
            ),
          );
        },
      ),
    );
  }
*/

/*
  Row signInSection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "Already have an account ? ",
          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0),
        ),
        GestureDetector(
          onTap: () {
            debugPrint('navigating to signUp screen');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LoginScreen()),
            );
          },
          child: Text(
            "Log in here ",
            style: TextStyle(
                color: Colors.deepOrangeAccent,
                fontWeight: FontWeight.w500,
                fontSize: 16.0),
          ),
        ),
      ],
    );
  }
*/

  //***************** PhotoId *****************************
  showPhotoIdImagePicker() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            height: 180,
            child: Container(
              child: _buildPhotoIdBottomNavigationMenu(),
              decoration: BoxDecoration(
                color: Theme.of(context).canvasColor,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(10),
                  topRight: const Radius.circular(10),
                ),
              ),
            ),
          );
        });
  }

  Column _buildPhotoIdBottomNavigationMenu() {
    return Column(
      children: <Widget>[
        ListTile(
            leading: Icon(Icons.camera_enhance),
            title: Text('Camera'),
            onTap: () {
              Navigator.pop(context);
              _cameraPhotoIdImagePicker();
            }),
        ListTile(
          leading: Icon(Icons.camera),
          title: Text('Gallery'),
          onTap: () {
            Navigator.pop(context);
            _galleryPhotoIdImagePicker();
          },
        ),
        ListTile(
          leading: Icon(Icons.cancel),
          title: Text('Cancel'),
          onTap: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Future _cameraPhotoIdImagePicker() async {
    debugPrint('camera');
    cameraFile = await ImagePicker.pickImage(source: ImageSource.camera);
    if (cameraFile == null) return;
    String base64Image = base64Encode(cameraFile.readAsBytesSync());
    String cameraFileName = cameraFile.path.split("/").last;
    debugPrint(base64Image);
    debugPrint(cameraFileName);
    panAdharPhoto = base64Image;
    panAdharFileName = cameraFileName;

    Fluttertoast.showToast(
        msg: "File selected - $panAdharFileName",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 2);
    setState(() {
      selectedFileName = panAdharFileName;
    });
  }

  Future _galleryPhotoIdImagePicker() async {
    debugPrint('gallery');
    galleryFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (galleryFile == null) return;
    String base64Image = base64Encode(galleryFile.readAsBytesSync());
    String galleryFileName = galleryFile.path.split("/").last;

    photoId = base64Image;
    photoIdfileName = galleryFileName;
    debugPrint(photoIdfileName);
    debugPrint(photoId);

    Fluttertoast.showToast(
        msg: "File selected - $photoIdfileName",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 2);

    setState(() {
      selectedFileName = photoIdfileName;
    });
  }

  //***************** PhotoId End*****************************

  //***************** Aadhar/PAN *****************************

  showAadharPanImagePicker() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            height: 180,
            child: Container(
              child: _buildAadharPanBottomNavigationMenu(),
              decoration: BoxDecoration(
                color: Theme.of(context).canvasColor,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(10),
                  topRight: const Radius.circular(10),
                ),
              ),
            ),
          );
        });
  }

  Column _buildAadharPanBottomNavigationMenu() {
    return Column(
      children: <Widget>[
        ListTile(
            leading: Icon(Icons.camera_enhance),
            title: Text('Camera'),
            onTap: () {
              Navigator.pop(context);
              _cameraAadharPanImagePicker();
            }),
        ListTile(
          leading: Icon(Icons.camera),
          title: Text('Gallery'),
          onTap: () {
            Navigator.pop(context);
            _galleryAadharPanImagePicker();
          },
        ),
        ListTile(
          leading: Icon(Icons.cancel),
          title: Text('Cancel'),
          onTap: () => Navigator.pop(context),
        ),
      ],
    );
  }

  Future _cameraAadharPanImagePicker() async {
    debugPrint('camera');
    cameraFile = await ImagePicker.pickImage(source: ImageSource.camera);
    if (cameraFile == null) return;
    String base64Image = base64Encode(cameraFile.readAsBytesSync());
    String cameraFileName = cameraFile.path.split("/").last;
    panAdharPhoto = base64Image;
    panAdharFileName = cameraFileName;

    Fluttertoast.showToast(
        msg: "File selected - $panAdharFileName",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 2);

    setState(() {
      selectedFileName = panAdharFileName;
    });

    debugPrint(panAdharFileName);
    debugPrint(panAdharPhoto);
  }

  Future _galleryAadharPanImagePicker() async {
    debugPrint('gallery');
    galleryFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (galleryFile == null) return;
    String base64Image = base64Encode(galleryFile.readAsBytesSync());
    String galleryFileName = galleryFile.path.split("/").last;

    panAdharPhoto = base64Image;
    panAdharFileName = galleryFileName;

    Fluttertoast.showToast(
        msg: "File selected - $panAdharFileName",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 2);
    debugPrint(panAdharFileName);
    debugPrint(panAdharPhoto);

    setState(() {
      selectedFileName = panAdharFileName;
    });
  }

  _registerUsingKyc() async {
    pr.show();
    const headers = {'Content-Type': 'application/json'};
    String url =
        "https://microecommerce.flicklead.com/api/UserDetail/DetailProfileCreation";

    Map<String, dynamic> data = {
      //Registration Id
      'Id': widget.id,
      // photoId FileName
      'PhotoIdfileName': photoIdfileName,
      // photoId FileName
      'PanAdharName': panAdharFileName,
      // photoId Base64
      'PhotoId': photoId,
      // PAN/Aadhar Base64
      'PanAdharPhoto': panAdharPhoto,
      // PAN/Aadhar Name from textField
      'PanAdharFileName': nameController.text,
      // PAN/Aadhar Number from textField
      'PanAdharNumber': numberController.text,
    };
    debugPrint(data.toString());

    var response = await http.put(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      pr.hide();
      debugPrint(response.statusCode.toString());
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginScreen()),
          (Route<dynamic> route) => false);
    } else {
      pr.hide();
      debugPrint(response.statusCode.toString());
      return;
    }
  }

//***************** Aadhar/PAN End *****************************

}

/*
nameOfFile(String sourceName) {
  String empty = "Upload";
  if (sourceName == "photoId") {
    return empty = photoIdController.text;
  } else if (sourceName == "panAadhar") {
    return empty = paController.text;
  }
  return empty;
}
*/
