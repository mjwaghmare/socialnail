import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/models/UserModel.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomSingleButton.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'AddEvent.dart';

class AddMembersToEventsScreen extends StatefulWidget {
  @override
  _AddMembersToEventsScreenState createState() =>
      _AddMembersToEventsScreenState();
}

class _AddMembersToEventsScreenState extends State<AddMembersToEventsScreen> {
  TextEditingController searchController = new TextEditingController();
  bool loadingUsers = false;
  bool addingMembers = false;

  List<UserModel> userListData = [];
  List<int> eventMembersList = [];
  bool _addedMember = false;
  String title = "Add";
  String query = "";

  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _getUsers();
    super.initState();
  }

  Future<List<UserModel>> _getUsers() async {
    setState(() {
      loadingUsers = true;
    });
    const headers = {'Content-Type': 'application/json'};
    String url =
        "https://microecommerce.flicklead.com/api/UserDetail/AllUserDetails";

    var response = await http.get(
      url,
      headers: headers,
    );
    debugPrint(response.statusCode.toString());
    if (response.statusCode == 200) {
      setState(() {
        loadingUsers = false;
      });
      final data = jsonDecode(response.body.replaceAll("ï»¿", ""));

      for (Map i in data) {
        userListData.add(
          UserModel.map(i),
        );
      }
    } else {
      debugPrint(response.statusCode.toString());
    }
    return userListData;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet: addingMembers == false
          ? CustomSingleButton(
              title: "Add Members",
              onTapped: _addMemberToEvent,
            )
          : Material(
        elevation: 5,
        child: Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          color: Colors.white,
          height: Platform.isIOS ? 70 : 60,
          child: Center(
            child: Container(
              height: 30,
              width: 30,
              child: CircularProgressIndicator(
                backgroundColor: kAppColor,
              ),
            ),
          ),
        ),
      ),
      key: _scaffoldkey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 5,
        titleSpacing: 0,
        title: Text(
          "Add Event Members",
          style: TextStyle(fontFamily: kOkraMedium, color: kAppTextColor),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: kAppColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            "assets/images/background.png",
            fit: BoxFit.fill,
          ),
          loadingUsers == true
              ? Container(
            child: Center(
              child: CircularProgressIndicator(
                backgroundColor: kAppColor,
              ),
            ),
          )
              : SingleChildScrollView(
            primary: true,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  margin:
                  EdgeInsets.symmetric(horizontal: 25, vertical: 30),
                  child: Column(
                    children: <Widget>[
                      searchBar(),
                      listOfUsers(),
                    ],
                  ),
                ),
                SizedBox(height: 50),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //searchBar widget
  Widget searchBar() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          shape: BoxShape.rectangle,
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.30),
                offset: Offset(1, 1),
                spreadRadius: 1)
          ]),
      child: TextField(
        controller: searchController,
        style: TextStyle(
            fontSize: 20, fontFamily: kOkraRegular, color: Colors.grey),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Search by User Id ,Mobile Number",
          hintStyle: TextStyle(fontSize: 16, fontFamily: kOkraRegular),
          filled: true,
          fillColor: Colors.white,
          prefixIcon: IconButton(
            icon: Icon(
              Icons.search,
              size: 30,
              color: Colors.grey,
            ),
            onPressed: () {},
          ),
          suffixIcon: IconButton(
            icon: Icon(
              Icons.close,
              size: 20,
              color: Colors.grey,
            ),
            onPressed: () {
              WidgetsBinding.instance
                  .addPostFrameCallback((_) => searchController.clear());
            },
          ),
        ),
        onChanged: (val) {
          setState(() {
            query = val;
          });
        },
      ),
    );
  }

  //getting app users
  listOfUsers() {
    return Builder(
      builder: (context) {
        return ListView.separated(
          shrinkWrap: true,
          primary: false,
          itemCount: userListData.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.white,
                child: userListData[index].photoId == null ||
                    userListData[index].photoId == ""
                    ? Image.asset(
                  "assets/images/male_avatar.png",
                  fit: BoxFit.cover,
                )
                    : Image.network(''),
              ),
              title: Text(
                userListData[index].name,
                style: TextStyle(fontFamily: kOkraSemiBold),
              ),
              subtitle: Text(
                userListData[index].mobileNumber,
                style: TextStyle(fontFamily: kOkraMedium),
              ),
              trailing: GestureDetector(
                onTap: () {
                  if (userListData[index].addedMem == false) {
                    eventMembersList.add(userListData[index].id);
                    setState(() {
                      userListData[index].addedMem = true;
                      title = "Remove";
                    });
                  } else if (userListData[index].addedMem == true) {
                    eventMembersList.remove(userListData[index].id);
                    setState(() {
                      userListData[index].addedMem = false;
                      title = "Add";
                    });
                  }
                },
                child: Material(
                  elevation: 1,
                  color: Colors.white,
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.white,
                    ),
                    width: userListData[index].addedMem == false ? 60 : 80,
                    height: 35,
                    padding: EdgeInsets.all(8),
                    alignment: Alignment.centerRight,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          userListData[index].addedMem == false
                              ? "Add"
                              : "Remove",
                          style: TextStyle(fontFamily: kOkraMedium),
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 10,
                          color: Colors.black,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
          separatorBuilder: (context, index) =>
              Divider(
                color: Colors.grey,
              ),
        );
      },
    );
  }

  //api to add members to event
  _addMemberToEvent() async {
    if (eventMembersList.length <= 0) {
      Fluttertoast.showToast(
          msg: "Add at least one member",
          backgroundColor: kAppColor,
          textColor: Colors.white,
          gravity: ToastGravity.CENTER);
    } else {
      /*Navigator.of(context).pop(
        MaterialPageRoute(
          builder: (BuildContext context) => AddEvent(eventMembersList),
        ),
      );*/
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        addingMembers = true;
      });
      const headers = {'Content-Type': 'application/json'};
      String url =
          "https://microecommerce.flicklead.com/api/EventDetails/EditEventMember";

      Map<String, dynamic> data = {
        "Id": prefs.getString("userId"), //userId
        "Members": eventMembersList.toString()
      };
      var response = await http.put(
        url,
        headers: headers,
        body: json.encode(data),
      );
      debugPrint(response.statusCode.toString());
      if (response.statusCode == 200) {
        setState(() {
          addingMembers = false;
        });
        Fluttertoast.showToast(msg: "Members Added Successfully");
        Navigator.of(context).pop(
          MaterialPageRoute(
            builder: (BuildContext context) => AddEvent(),
          ),
        );
      } else {
        setState(() {
          addingMembers = false;
        });
        debugPrint(response.statusCode.toString());
      }
    }
  }
}
