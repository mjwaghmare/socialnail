import 'package:shared_preferences/shared_preferences.dart';

class Utils {
  static String generateUsername(String email) {
    //get the email and split it from the @ symbol into two strings[0][1],
    //eg. manoj.waghmare@aceventura.in it will be splits into
    //[0] manoj.waghmare and this will returned...
    return email.substring(
      0,
      email.indexOf('@'),
    );
  }

  static String generateImagename(String imageName) {
    if (imageName == null) {
      return "";
    } else {
      return imageName.substring(
        0,
        imageName.indexOf('.'),
      );
    }
  }

  static String convertTime(String time) {
    return time.substring(
      0,
      time.indexOf('T'),
    );
  }

  /*static String formatDate(String date) {
    var formatter = new DateFormat('dd-MM-yyyy');
    String formattedDate = formatter.format(date);
    return formattedDate;
  }*/

  //Shared Pref...
  static setVisitingFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("alreadyVisited", true);
  }

  static getVisitingFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool alreadyVisited = prefs.getBool("alreadyVisited") ?? false;
    return alreadyVisited;
  }

  static setIsLoggedInFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("isLoggedIn", true);
  }

  static setIsLoggedOutFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("isLoggedIn", false);
  }

  static getIsLoggedInFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLoggedIn = prefs.getBool("isLoggedIn") ?? false;
    return isLoggedIn;
  }

  static setName(String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("ProfileName", name) ?? null;
  }

  static getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String profileName = prefs.getString('ProfileName');
    return profileName;
  }
}
