class LoggedInUserModel {
  final int id;
  final String name;
  final String mobile;
  final String emailId;

  LoggedInUserModel({this.id, this.name, this.mobile, this.emailId});

  factory LoggedInUserModel.fromJson(Map<String, dynamic> json) {
    return LoggedInUserModel(
      id: json['id'],
      name: json['name'],
      mobile: json['mobileNumber'],
      emailId: json['emailId'],
    );
  }
}
