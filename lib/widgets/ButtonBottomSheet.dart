import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ButtonBottomSheet extends StatelessWidget {
  final String title1, title2;
  final Function onTapped1, onTapped2;

  ButtonBottomSheet({this.title1, this.onTapped1, this.title2, this.onTapped2});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Platform.isIOS ? 70 : 60,
      child: Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              onTap: onTapped1,
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                      Colors.pink[300],
                      Colors.pink[400],
                      Colors.pink[500],
                      Colors.pink[600]
                    ])),
                child: Center(
                  child: Text(
                    title1,
                    style: GoogleFonts.openSans(
                        fontSize: 15.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: onTapped2,
              child: Container(
                color: Colors.white,
                child: Center(
                  child: Text(
                    title2,
                    style: GoogleFonts.openSans(
                        fontSize: 15.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
