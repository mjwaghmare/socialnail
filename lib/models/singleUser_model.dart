class SingleUser {
  int id;
  String name, mob, imgUrl;

  SingleUser(this.id, this.name, this.mob, this.imgUrl);

  SingleUser.map(dynamic obj) {
    this.id = obj["id"];
    this.name = obj["name"];
    this.mob = obj["mobileNumber"];
    this.imgUrl = obj["photoId"];
  }
}
