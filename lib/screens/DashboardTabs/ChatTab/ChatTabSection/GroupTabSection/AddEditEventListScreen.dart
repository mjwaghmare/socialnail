import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/models/AddEditEventsModel.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'AddEvent.dart';
import 'EditEvent.dart';

class AddEditEventListScreen extends StatefulWidget {
  final String grpId;

  AddEditEventListScreen({Key key, this.grpId}) : super();

  @override
  _AddEditEventListScreenState createState() => _AddEditEventListScreenState();
}

class _AddEditEventListScreenState extends State<AddEditEventListScreen> {
  List<AddEditEventsModel> addedEvent = [];
  bool loadingEvents = false;

  @override
  void initState() {
    super.initState();

    _getAllEvents();
  }

  Future<void> _getAllEvents() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      loadingEvents = true;
    });

    //code to get all events
    const headers = {'Content-Type': 'application/json'};
    String url =
        "https://microecommerce.flicklead.com/api/EventDetails/AllEventDetails";

    Map<String, dynamic> data = {
      'CreatedByUserId': prefs.get("userId"),
    };
    debugPrint(prefs.get("userId"));
    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );
    debugPrint(response.statusCode.toString());
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      debugPrint(jsonResponse.toString());
      setState(() {
        loadingEvents = false;
      });
      for (Map i in jsonResponse) {
        addedEvent.add(AddEditEventsModel.map(i));
      }
      debugPrint(addedEvent.toString());
    } else {
      setState(() {
        loadingEvents = false;
      });
      debugPrint(response.statusCode.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 5,
        titleSpacing: 0,
        title: Text(
          "Events",
          style: TextStyle(fontFamily: kOkraMedium, color: kAppTextColor),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: kAppColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add,
              color: kAppColor,
              size: 30,
            ),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (BuildContext context) => AddEvent(
                          grpId: widget.grpId,
                        )),
              );
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            "assets/images/background.png",
            fit: BoxFit.cover,
          ),
          Container(
            margin: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //Product List
                SizedBox(height: 5),
                loadingEvents == false
                    ? Expanded(
                        child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: addedEvent.length,
                          itemBuilder: (BuildContext context, int index) {
                            return eventCard(addedEvent[index]);
                          },
                        ),
                      )
                    : Container(
                        height: MediaQuery.of(context).size.height / 2,
                        width: MediaQuery.of(context).size.width,
                        child: Center(
                          child: CircularProgressIndicator(
                            backgroundColor: kAppColor,
                          ),
                        ),
                      ),
                SizedBox(height: 5),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget eventCard(AddEditEventsModel addedEvent) {
    //list of images of attending members
    List<String> images = [
      addedEvent.eventImage1,
      addedEvent.eventImage2,
      addedEvent.eventImage3,
    ];

    /*List<String> images = new List();
    images.add(
        "https://images.unsplash.com/photo-1458071103673-6a6e4c4a3413?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80");
    images.add(
        "https://images.unsplash.com/photo-1518806118471-f28b20a1d79d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=80");
    images.add(
        "https://images.unsplash.com/photo-1470406852800-b97e5d92e2aa?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80");
    images.add(
        "https://images.unsplash.com/photo-1473700216830-7e08d47f858e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80");
*/
    return Card(
      margin: EdgeInsets.only(top: 10),
      child: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height * 0.20,
        child: Column(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * 0.148,
              padding: EdgeInsets.all(8),
              child: Row(
                children: <Widget>[
                  //event Image and eventType
                  Column(
                    children: <Widget>[
                      CircleAvatar(
                        radius: 19,
                        backgroundColor: kAppColor,
                        backgroundImage: NetworkImage(
                          addedEvent.eventImage1,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(2),
                          color: addedEvent.eventType == "Premium" ||
                                  addedEvent.eventType == "Paid"
                              ? Colors.green
                              : kAppColor,
                        ),
                        padding:
                            EdgeInsets.symmetric(horizontal: 8, vertical: 2.5),
                        child: Text(
                          addedEvent.eventType,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                              fontFamily: kOkraMedium),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(width: 8),
                  //Details
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          addedEvent.title,
                          style: TextStyle(
                              fontFamily: kOkraMedium,
                              fontSize: 17,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          addedEvent.agenda,
                          style: TextStyle(
                              fontFamily: kOkraRegular,
                              fontSize: 15,
                              color: kAppColor,
                              fontWeight: FontWeight.w500),
                        ),
                        /*Text(
                            "Attending members",
                            style: TextStyle(
                                fontFamily: kOkraRegular,
                                fontSize: 16,
                                color: Colors.grey,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(height: 5),
                          //image stack removed
                          Container(
                            margin: EdgeInsets.only(left: 42),
                            height: 30,
                            width: double.infinity,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ImageStack(
                                  imageList: images,
                                  imageRadius: 28,
                                  // Radius of each images
                                  imageCount: 3,
                                  // Maximum number of images to be shown
                                  imageBorderWidth: 0,
                                  // Border width around the images
//                                  totalCount: addedEvent.members.length,
                                  totalCount: 10,
                                ),
                              ],
                            ),
                          )
*/
                      ],
                    ),
                  ),
                  SizedBox(width: 5),
                  //Edit,fees & type of events
                  Container(
                    width: 115,
                    child: Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.topRight,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) => EditEvent(
                                  grpId: widget.grpId,
                                  addedEvent: addedEvent,
                                ),
                              ));
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.85),
                                      offset: Offset(0, 0),
                                      spreadRadius: 0)
                                ],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                color: Colors.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Icon(
                                  Icons.mode_edit,
                                  size: 19,
                                  color: kAppColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                addedEvent.eventType == "Free"
                                    ? addedEvent.eventType + " event"
                                    : addedEvent.eventType + " fees",
                                style: TextStyle(
                                    color: kAppTextColor,
                                    fontFamily: kOkraMedium,
                                    fontSize: 13),
                              ),
                              SizedBox(height: 8),
                              Container(
                                color: Colors.white,
                                width: 100,
                                height: 20,
                                child: OutlineButton(
                                  onPressed: () {},
                                  borderSide: BorderSide(color: Colors.green),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Text(
                                        addedEvent.eventType == "Free"
                                            ? "Join Now"
                                            : "Pay- 5000",
                                        style: TextStyle(
                                            fontFamily: kOkraMedium,
                                            fontSize: 12),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        size: 9,
                                        color: Colors.black,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //bottom container
            Container(
              height: 40,
              width: double.infinity,
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.grey[200],
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(4),
                    bottomRight: Radius.circular(4)),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Time",
                      style: TextStyle(
                          color: Colors.grey[400],
                          fontFamily: kOkraMedium,
                          fontSize: 13),
                    ),
                    Text(
                      "Location",
                      style: TextStyle(
                          color: Colors.grey[400],
                          fontFamily: kOkraMedium,
                          fontSize: 13),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
