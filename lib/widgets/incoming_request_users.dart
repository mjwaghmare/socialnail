import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class IncomingRequestUsers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
          child: Text(
            ("Incoming Request"),
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.grey),
          ),
        ),
        GestureDetector(
          onTap: () {
            Fluttertoast.showToast(
                msg: "Goto request page",
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.CENTER,
                /*timeInSecForIos: 2*/);
          },
          child: Container(
            height: 80,
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.only(left: 10.0),
              scrollDirection: Axis.horizontal,
              itemCount: 10,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: 60.0,
                  width: 60.0,
                  decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 2),
                        blurRadius: 5.0),
                  ]),
                  margin: EdgeInsets.all(10.0),
                  child: ClipOval(
                    child: Image(
                      height: 60.0,
                      width: 60.0,
                      image: AssetImage('assets/images/male_avatar.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
