import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_ecommerce/models/AddEditProductsModel.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/MyCartScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomButton.dart';

class ProductDetailScreen extends StatefulWidget {
  final AddEditProductsModel addEditProductsModel;

  ProductDetailScreen(this.addEditProductsModel);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  List<AddEditProductsModel> myCartProducts = [];
  int selectedColor = 0;
  List<String> selectedSizes = ["M", "S", "L"];

  List<Color> prodColors = [
    Color(0xffffebee),
    Color(0xffffcdd2),
    Color(0xffef9a9a),
    Color(0xffe57373),
    Color(0xfff44336),
    Color(0xfff44338)
  ];

  bool sizeSelected = false;

  @override
  Widget build(BuildContext context) {
    debugPrint(widget.addEditProductsModel.productSize);
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    final _scaffoldKey = GlobalKey<ScaffoldState>();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        brightness: Brightness.dark,
        elevation: 1,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            height: 30,
            width: 30,
            alignment: Alignment.center,
            child: IconButton(
                icon: Icon(Icons.chevron_left, color: kAppColor, size: 35),
                onPressed: () {
                  debugPrint("Pressed");
                  Navigator.of(context).pop();
                }),
          ),
        ),
        title: Row(
          children: <Widget>[
            Text(
              widget.addEditProductsModel.productName,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: TextStyle(
                  fontFamily: kOkraMedium, color: Colors.black, fontSize: 19),
            )
          ],
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(image: kAppBackground, fit: BoxFit.cover),
        ),
        height: screenHeight,
        width: screenWidth,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                height: screenHeight,
                width: screenHeight,
                margin: EdgeInsets.all(12),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    shape: BoxShape.rectangle,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.15),
                          offset: Offset(0, 0),
                          spreadRadius: 0.4)
                    ]),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(2),
                            height: 100,
                            width: 100,
                            child: Hero(
                              tag: widget.addEditProductsModel.id,
                              child: Container(
                                height: 110.0,
                                width: 85.0,
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey.withOpacity(0.55),
                                        offset: Offset(0, 0),
                                        spreadRadius: 0.4)
                                  ],
                                  borderRadius: BorderRadius.circular(7.0),
                                  image: DecorationImage(
                                      image: NetworkImage(widget
                                          .addEditProductsModel.productImage1),
                                      fit: BoxFit.cover),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(top: 20),
                              height: 150,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    widget.addEditProductsModel.productName,
                                    style: TextStyle(
                                        fontFamily: kOkraSemiBold,
                                        fontSize: 17),
                                  ),
                                  Text(
                                    "Rs. " +
                                        widget.addEditProductsModel.price
                                            .toString(),
                                    style: TextStyle(
                                        fontFamily: kOkraRegular, fontSize: 15),
                                  ),
                                  Text(
                                    "Select Color",
                                    style: TextStyle(
                                        fontFamily: kOkraSemiBold,
                                        fontSize: 16),
                                  ),
                                  colorTile(prodColors),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(
                            "Select Size",
                            style: TextStyle(
                                fontFamily: kOkraSemiBold, fontSize: 15),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          sizeTile(selectedSizes),
                        ],
                      ),
                      SizedBox(height: 20),
                      Divider(
                        color: Colors.black,
                      ),
                      SizedBox(height: 10),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Description / Specifications",
                            style: TextStyle(
                                fontFamily: kOkraSemiBold,
                                fontSize: 17,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 15),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  widget
                                      .addEditProductsModel.productDescriptions,
                                  style: TextStyle(color: Colors.grey[500]),
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            ),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                      SizedBox(height: 20),
                      Divider(
                        color: Colors.black,
                      ),
                      SizedBox(height: 15),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Product Reviews",
                            style: TextStyle(
                                fontFamily: kOkraSemiBold,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 2),
                                    decoration: BoxDecoration(
                                        color: Colors.green,
                                        borderRadius: BorderRadius.circular(2),
                                        shape: BoxShape.rectangle,
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.white
                                                  .withOpacity(0.55),
                                              offset: Offset(0, 0),
                                              spreadRadius: 0.4)
                                        ]),
                                    child: Text("4.3"),
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    "Very Good",
                                    style: TextStyle(
                                        fontFamily: kOkraSemiBold,
                                        color: kAppTextColor),
                                  ),
                                ],
                              ),
                              Text(
                                " 23 Reviews >",
                                style: TextStyle(
                                    color: kAppColor,
                                    fontFamily: kOkraSemiBold),
                              ),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
      bottomSheet: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(2.0), topRight: Radius.circular(2.0)),
            shape: BoxShape.rectangle,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.55),
                  offset: Offset(0, 0),
                  spreadRadius: 0.4)
            ]),
        height: Platform.isIOS ? 70 : 60,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Expanded(
                child: Container(
                    margin: EdgeInsets.symmetric(vertical: 1, horizontal: 5),
                    child: CustomButton(
                        title: "Add to Cart",
                        onTapped: () {
                          myCartProducts.add(widget.addEditProductsModel);
                          final snackBar =
                              SnackBar(content: Text("Added to Cart"));
                          _scaffoldKey.currentState.showSnackBar(snackBar);
                        })),
              ),
              VerticalDivider(),
              Expanded(
                child: Container(
                    margin: EdgeInsets.symmetric(vertical: 1, horizontal: 5),
                    child: CustomButton(
                        title: "Go to Cart",
                        onTapped: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  MyCartScreen(myCartProducts: myCartProducts),
                            ),
                          );
                        })),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget sizeTile(List<String> sizes) {
    return Container(
      width: double.infinity,
      height: 25,
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: sizes.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                /*if (sizes[index].selectedSize == false) {
                  setState(() {
                    sizes[index].selectedSize = true;
                  });
                  selectedSizes.add(sizes[index].sizeName);
                } else if (sizes[index].selectedSize == true) {
                  setState(() {
                    sizes[index].selectedSize = false;
                  });
                  selectedSizes.remove(sizes[index].sizeName);
                }*/
              },
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 5),
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 1),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.45),
                        offset: Offset(0, 0),
                        spreadRadius: 0.3)
                  ],
                  borderRadius: BorderRadius.circular(2),
                  color: Colors.white.withOpacity(1),
                  border: Border.all(width: 1, color: kAppColor
                      /*sizes[index].selectedSize == true
                          ? kAppColor
                          :
                          Colors.grey.withOpacity(0.6)*/
                      ),
                ),
                child: Text(
//                    sizes[index].sizeName,
                  sizes[index].toString(),
                  style: TextStyle(
                      fontFamily: kOkraRegular,
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                ),
              ),
            );
          }),
    );
  }

  Widget colorTile(List<Color> prodColorsList) {
    return Container(
      padding: EdgeInsets.all(2),
      width: double.infinity,
      height: 30,
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: prodColorsList.length,
          itemBuilder: (context, index) {
            return colorTileWidget(selectedColor, index, prodColorsList);
          }),
    );
  }

  Widget colorTileWidget(int selectedColor, int index, List<Color> colorsList) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedColor = index;
        });
      },
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 5),
            decoration: BoxDecoration(
              color: colorsList[index],
              borderRadius: BorderRadius.circular(2.0),
            ),
            height: 20,
            width: 20,
          ),
          Positioned(
            right: 5,
            bottom: 5,
            left: 5,
            top: 5,
            child: Icon(Icons.check,
                size: 11,
                color:
                    selectedColor == index ? Colors.black : Colors.transparent),
          )
        ],
      ),
    );
  }
}
