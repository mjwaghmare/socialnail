/// id : 3
/// category : "technology"
/// status : "Active"
/// productName : "Blue Chair2"
/// groupName : "Business D"
/// price : 2000.0
/// stockStatus : "In stock"
/// shippingBy : "Self"
/// productDescriptions : "This is premium blue chair for all"
/// importanatInformation : "Copy right by AR fashion"
/// productLongDescription : "long descriptions here"
/// productImage1 : "C:\\Users\\Admin\\Desktop\\New MicroEcommerce\\MicroEcommerce\\MicroEcommerce\\ProductPhoto\\Blue Chair21.jpg"
/// productImage2 : "C:\\Users\\Admin\\Desktop\\New MicroEcommerce\\MicroEcommerce\\MicroEcommerce\\ProductPhoto\\Blue Chair22.jpg"
/// productImage3 : "C:\\Users\\Admin\\Desktop\\New MicroEcommerce\\MicroEcommerce\\MicroEcommerce\\ProductPhoto\\Blue Chair23.jpg"
/// productImage4 : "C:\\Users\\Admin\\Desktop\\New MicroEcommerce\\MicroEcommerce\\MicroEcommerce\\ProductPhoto\\Blue Chair24.jpg"
/// productImage5 : "C:\\Users\\Admin\\Desktop\\New MicroEcommerce\\MicroEcommerce\\MicroEcommerce\\ProductPhoto\\Blue Chair25.jpg"
/// productImage6 : "C:\\Users\\Admin\\Desktop\\New MicroEcommerce\\MicroEcommerce\\MicroEcommerce\\ProductPhoto\\Blue Chair26.jpg"
/// productSize : "X"
/// productColor : "Black"
/// productBrand : "Culpa"
/// createdByUser : 1
/// entryDate : "2020-05-12T21:59:54-07:00"

class EditProductModel {
  int _id;
  String _category;
  String _status;
  String _productName;
  String _groupName;
  double _price;
  String _stockStatus;
  String _shippingBy;
  String _productDescriptions;
  String _importanatInformation;
  String _productLongDescription;
  String _productImage1;
  String _productImage2;
  String _productImage3;
  String _productImage4;
  String _productImage5;
  String _productImage6;
  String _productSize;
  String _productColor;
  String _productBrand;
  int _createdByUser;
  String _entryDate;

  int get id => _id;
  String get category => _category;
  String get status => _status;
  String get productName => _productName;
  String get groupName => _groupName;
  double get price => _price;
  String get stockStatus => _stockStatus;
  String get shippingBy => _shippingBy;
  String get productDescriptions => _productDescriptions;
  String get importanatInformation => _importanatInformation;
  String get productLongDescription => _productLongDescription;
  String get productImage1 => _productImage1;
  String get productImage2 => _productImage2;
  String get productImage3 => _productImage3;
  String get productImage4 => _productImage4;
  String get productImage5 => _productImage5;
  String get productImage6 => _productImage6;
  String get productSize => _productSize;
  String get productColor => _productColor;
  String get productBrand => _productBrand;
  int get createdByUser => _createdByUser;
  String get entryDate => _entryDate;

  EditProductModel(
      this._id,
      this._category,
      this._status,
      this._productName,
      this._groupName,
      this._price,
      this._stockStatus,
      this._shippingBy,
      this._productDescriptions,
      this._importanatInformation,
      this._productLongDescription,
      this._productImage1,
      this._productImage2,
      this._productImage3,
      this._productImage4,
      this._productImage5,
      this._productImage6,
      this._productSize,
      this._productColor,
      this._productBrand,
      this._createdByUser,
      this._entryDate);

  EditProductModel.map(dynamic obj) {
    this._id = obj["id"];
    this._category = obj["category"];
    this._status = obj["status"];
    this._productName = obj["productName"];
    this._groupName = obj["groupName"];
    this._price = obj["price"];
    this._stockStatus = obj["stockStatus"];
    this._shippingBy = obj["shippingBy"];
    this._productDescriptions = obj["productDescriptions"];
    this._importanatInformation = obj["importanatInformation"];
    this._productLongDescription = obj["productLongDescription"];
    this._productImage1 = obj["productImage1"];
    this._productImage2 = obj["productImage2"];
    this._productImage3 = obj["productImage3"];
    this._productImage4 = obj["productImage4"];
    this._productImage5 = obj["productImage5"];
    this._productImage6 = obj["productImage6"];
    this._productSize = obj["productSize"];
    this._productColor = obj["productColor"];
    this._productBrand = obj["productBrand"];
    this._createdByUser = obj["createdByUser"];
    this._entryDate = obj["entryDate"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = _id;
    map["category"] = _category;
    map["status"] = _status;
    map["productName"] = _productName;
    map["groupName"] = _groupName;
    map["price"] = _price;
    map["stockStatus"] = _stockStatus;
    map["shippingBy"] = _shippingBy;
    map["productDescriptions"] = _productDescriptions;
    map["importanatInformation"] = _importanatInformation;
    map["productLongDescription"] = _productLongDescription;
    map["productImage1"] = _productImage1;
    map["productImage2"] = _productImage2;
    map["productImage3"] = _productImage3;
    map["productImage4"] = _productImage4;
    map["productImage5"] = _productImage5;
    map["productImage6"] = _productImage6;
    map["productSize"] = _productSize;
    map["productColor"] = _productColor;
    map["productBrand"] = _productBrand;
    map["createdByUser"] = _createdByUser;
    map["entryDate"] = _entryDate;
    return map;
  }
}
