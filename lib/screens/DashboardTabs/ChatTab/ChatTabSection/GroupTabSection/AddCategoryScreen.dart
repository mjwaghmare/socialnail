import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/AddProductsScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomSingleButton.dart';

class AddCategoryScreen extends StatefulWidget {
  @override
  _AddCategoryScreenState createState() => _AddCategoryScreenState();
}

class _AddCategoryScreenState extends State<AddCategoryScreen> {
  bool addingCategory = false;
  TextEditingController _categoryController = new TextEditingController();
  final List<String> categories = <String>[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 5,
        titleSpacing: 0,
        title: Text(
          "Add Categories",
          style: TextStyle(fontFamily: kOkraMedium, color: kAppTextColor),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: kAppColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      bottomSheet: addingCategory == false
          ? CustomSingleButton(
              title: "Add Category",
              onTapped: () async {
                if (_categoryController.text.isEmpty) {
                  Fluttertoast.showToast(
                      msg: "Please add at least one "
                          "category",
                      textColor: Colors.white);
                } else {
                  setState(() {
                    addingCategory = true;
                  });
                  const headers = {'Content-Type': 'application/json'};
                  String url =
                      "http://microecommerce.flicklead.com/api/categorydetails/CreateCategoryDetails";

                  Map<String, dynamic> data = {
                    "CategoryName": categories.toString(),
                  };

                  var response = await http.post(
                    url,
                    headers: headers,
                    body: json.encode(data),
                  );

                  if (response.statusCode == 200) {
                    debugPrint(response.body.toString());
                    setState(() {
                      addingCategory = false;
                    });
                    Fluttertoast.showToast(
                        msg: "Categories Added Successfully",
                        textColor: Colors.white,
                        backgroundColor: kAppColor,
                        gravity: ToastGravity.CENTER);

                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AddProductsScreen()),
                        (Route<dynamic> route) => false);
                  } else {
                    setState(() {
                      addingCategory = false;
                    });
                    debugPrint(response.statusCode.toString());
                  }
                }
              },
            )
          : Material(
              elevation: 5,
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                height: Platform.isIOS ? 70 : 60,
                child: Center(
                  child: Container(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(
                      backgroundColor: kAppColor,
                    ),
                  ),
                ),
              ),
            ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            "assets/images/background.png",
            fit: BoxFit.cover,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            margin: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //AddCategorySection
                SizedBox(height: 20),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Expanded(
                      child: Material(
                        shadowColor: Colors.grey,
                        color: Colors.white,
                        elevation: 2,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                        ),
                        child: Container(
                          height: 50,
                          child: TextField(
                            controller: _categoryController,
                            style: TextStyle(
                                fontSize: 18,
                                color: kAppColor,
                                fontFamily: kOkraMedium),
                            cursorColor: kAppColor,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Add category ",
                              hintStyle:
                                  TextStyle(fontSize: 15, color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 0.5),
                    InkWell(
                      splashColor: kAppColor,
                      onTap: () {
                        addCategoryToList();
                      },
                      child: Material(
                        shadowColor: Colors.grey,
                        color: Colors.white,
                        elevation: 2,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                        ),
                        child: Container(
                          height: 50,
                          width: 50,
                          alignment: Alignment.center,
                          child: Icon(
                            Icons.add,
                            size: 30,
                            color: kAppColor,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    color: Colors.white,
                    child: ListView.builder(
                      primary: true,
                      shrinkWrap: true,
                      padding: const EdgeInsets.all(8),
                      itemCount: categories.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          color: Colors.white,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 15),
                                  alignment: Alignment.center,
                                  height: 50,
                                  child: Text(
                                    '${categories[index]}',
                                    style: TextStyle(
                                        fontSize: 18, fontFamily: kOkraMedium),
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.close,
                                  size: 20,
                                  color: kAppColor,
                                ),
                                onPressed: () {
                                  setState(() {
                                    categories.removeAt(index);
                                  });
                                },
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ),
                SizedBox(height: 60),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void addCategoryToList() {
    setState(() {
      categories.insert(0, _categoryController.text);
      _categoryController.clear();
    });
  }
}
