import 'package:flutter/material.dart';

const kBottomContainerHeight = 70.0;
const kAppColor = Color(0xFFF82389);
const kAppTextColor = Color(0xFF35495E);
const kAppBackground = AssetImage("assets/images/background.png");
const mapApi = "AIzaSyBQpdqUKDDfNDxttuOdodyTdKdImVCoF8k";

//font family
const kOkraThin = "OkraThin";
const kOkraLight = "OkraLight";
const kOkraExtraLight = "OkraExtraLight";
const kOkraRegular = "OkraRegular";
const kOkraSemiBold = "OkraSemiBold";
const kOkraMedium = "OkraMedium";
