class UserModel {
  int _id;
  String _name;
  String _mobileNumber;
  String _emailId;
  String _userName;
  dynamic _photoId;
  bool _addedMem = false;

  UserModel(
    this._addedMem,
    this._id,
    this._name,
    this._mobileNumber,
    this._emailId,
    this._userName,
    this._photoId,
  );

  int get id => _id;
  String get name => _name;
  String get mobileNumber => _mobileNumber;
  String get emailId => _emailId;
  String get userName => _userName;
  dynamic get photoId => _photoId;
  bool get addedMem => _addedMem;

  set addedMem(bool value) {
    _addedMem = value;
  }

  UserModel.map(dynamic obj) {
    this._id = obj["id"];
    this._name = obj["name"];
    this._mobileNumber = obj["mobileNumber"];
    this._emailId = obj["emailId"];
    this._userName = obj["userName"];
    this._photoId = obj["photoId"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = _id;
    map["name"] = _name;
    map["mobileNumber"] = _mobileNumber;
    map["emailId"] = _emailId;
    map["userName"] = _userName;
    map["photoId"] = _photoId;
    return map;
  }
}
