class PaymentCardModel {
  String _cardName, _cvv, _cardNumber, _expiredDate;

  PaymentCardModel(
      this._cardName, this._cvv, this._cardNumber, this._expiredDate);

  get expiredDate => _expiredDate;

  set expiredDate(value) {
    _expiredDate = value;
  }

  get cardNumber => _cardNumber;

  set cardNumber(value) {
    _cardNumber = value;
  }

  get cvv => _cvv;

  set cvv(value) {
    _cvv = value;
  }

  String get cardName => _cardName;

  set cardName(String value) {
    _cardName = value;
  }
}
