import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/models/AddEditProductsModel.dart';
import 'package:micro_ecommerce/models/test.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/EditProdctScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';

import 'AddProductsScreen.dart';

class AddEditProductListScreen extends StatefulWidget {
  final String groupName, groupId;

  AddEditProductListScreen({this.groupName, this.groupId});

  @override
  _AddEditProductListScreenState createState() =>
      _AddEditProductListScreenState();
}

class _AddEditProductListScreenState extends State<AddEditProductListScreen> {
  final product = prod;
  List<AddEditProductsModel> addedProducts = [];
  bool loadingProduct = false;

  @override
  void initState() {
    super.initState();
    debugPrint(widget.groupName);
    _getAllProducts(widget.groupName);
  }

  Future<void> _getAllProducts(String groupName) async {
    setState(() {
      loadingProduct = true;
    });

    //code to get all products
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/Productdetails/AllProductdetails";

    Map<String, dynamic> data = {
      'GroupName': groupName,
    };
    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      debugPrint(jsonResponse);
      setState(() {
        loadingProduct = false;
      });
      for (Map i in jsonResponse) {
        addedProducts.add(AddEditProductsModel.map(i));
      }
    } else {
      setState(() {
        loadingProduct = false;
      });
      debugPrint(response.statusCode.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 5,
        titleSpacing: 0,
        title: Text(
          "Products",
          style: TextStyle(fontFamily: kOkraMedium, color: kAppTextColor),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: kAppColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.add,
              color: kAppColor,
              size: 30,
            ),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (BuildContext context) => AddProductsScreen(
                          groupName: widget.groupName,
                        )),
              );
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            "assets/images/background.png",
            fit: BoxFit.cover,
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //Search and Filter Button
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Material(
                        shadowColor: Colors.grey,
                        borderRadius: BorderRadius.circular(2),
                        color: Colors.transparent,
                        elevation: 2,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                          ),
                          height: 40,
                          child: TextField(
                            textAlign: TextAlign.left,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.grey,
                              ),
                              hintText: "Search",
                              hintStyle: TextStyle(
                                  fontSize: 17,
                                  fontFamily: kOkraLight,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 5),
                    Material(
                      shadowColor: Colors.grey,
                      color: Colors.white,
                      elevation: 2,
                      borderRadius: BorderRadius.circular(4),
                      child: Container(
                        height: 40,
                        width: 40,
                        alignment: Alignment.center,
                        child: Icon(
                          Icons.filter_list,
                          size: 25,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 15),
                Text(
                  "Product List",
                  style: TextStyle(
                      fontFamily: kOkraSemiBold,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                //Product List
                SizedBox(height: 10),
                loadingProduct == false
                    ? Expanded(
                        child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: addedProducts.length,
                          itemBuilder: (BuildContext context, int index) {
                            return productCard(addedProducts[index]);
                          },
                        ),
                      )
                    : Center(
                        child: CircularProgressIndicator(
                          backgroundColor: kAppColor,
                        ),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget productCard(AddEditProductsModel product) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 7),
      width: MediaQuery.of(context).size.width,
      height: 80,
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.85),
                offset: Offset(0, 0),
                spreadRadius: 0)
          ],
          borderRadius: BorderRadius.circular(6),
          color: Colors.white),
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(6),
                bottomLeft: Radius.circular(6),
              ),
            ),
            height: 80,
            width: 70,
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(4)),
                child: Image(
                  image: NetworkImage(product.productImage1),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    product.productName,
                    style: TextStyle(
                      fontFamily: kOkraSemiBold,
                      fontSize: 17,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    product.groupName,
                    style: TextStyle(
                        fontFamily: kOkraSemiBold,
                        color: kAppColor,
                        fontSize: 13),
                    overflow: TextOverflow.ellipsis,
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "Price : ",
                          style: TextStyle(
                              fontFamily: kOkraRegular,
                              color: Colors.grey,
                              fontSize: 13),
                        ),
                        TextSpan(
                          text: product.price.toString(),
                          style: TextStyle(
                              fontFamily: kOkraMedium,
                              color: Colors.green,
                              fontSize: 13),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) =>
                            EditProductScreen(product.id.toString()),
                      ));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.85),
                              offset: Offset(0, 0),
                              spreadRadius: 0)
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Icon(
                          Icons.mode_edit,
                          size: 19,
                          color: kAppColor,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "Status : ",
                          style: TextStyle(
                              fontFamily: kOkraRegular,
                              color: Colors.grey,
                              fontSize: 13),
                        ),
                        TextSpan(
                          text: product.stockStatus,
                          style: TextStyle(
                              fontFamily: kOkraRegular,
                              color: kAppColor,
                              fontSize: 13),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
