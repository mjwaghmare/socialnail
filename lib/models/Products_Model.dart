import 'package:flutter/material.dart';

class ProductsModel {
  final String prodImgUrl;
  final String prodName;
  final String prodPrice;

  ProductsModel(
      {@required this.prodName,
      @required this.prodImgUrl,
      @required this.prodPrice});

// factory ProductsModel.fromJson(Map<String, dynamic> json) {
//     return ProductsModel(
//       name: json['groupName'],
//       message: json['groupMember'],
//       time: json['createdDate'],
//       avatarUrl: "",
//     );
//     }

}

var prodData = [
  new ProductsModel(
      prodImgUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRhONq4f30PEF3ekS0d-jPHtSzlb46JKfH4A2SaYTqDLYD22gGuhvifxOZ8-Z3MOkfc06Bup6Vf&usqp=CAc",
      prodName: "One Plus 6T",
      prodPrice: "50,001"),
  new ProductsModel(
      prodImgUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRhONq4f30PEF3ekS0d-jPHtSzlb46JKfH4A2SaYTqDLYD22gGuhvifxOZ8-Z3MOkfc06Bup6Vf&usqp=CAc",
      prodName: "Apple Pro",
      prodPrice: "50,002"),
  new ProductsModel(
      prodImgUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRhONq4f30PEF3ekS0d-jPHtSzlb46JKfH4A2SaYTqDLYD22gGuhvifxOZ8-Z3MOkfc06Bup6Vf&usqp=CAc",
      prodName: "One Plus 6",
      prodPrice: "50,003"),
  new ProductsModel(
      prodImgUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRhONq4f30PEF3ekS0d-jPHtSzlb46JKfH4A2SaYTqDLYD22gGuhvifxOZ8-Z3MOkfc06Bup6Vf&usqp=CAc",
      prodName: "One Plus 5T",
      prodPrice: "50,004"),
  new ProductsModel(
      prodImgUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRhONq4f30PEF3ekS0d-jPHtSzlb46JKfH4A2SaYTqDLYD22gGuhvifxOZ8-Z3MOkfc06Bup6Vf&usqp=CAc",
      prodName: "One Plus 5",
      prodPrice: "50,005"),
  new ProductsModel(
      prodImgUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRhONq4f30PEF3ekS0d-jPHtSzlb46JKfH4A2SaYTqDLYD22gGuhvifxOZ8-Z3MOkfc06Bup6Vf&usqp=CAc",
      prodName: "Realme",
      prodPrice: "50,006"),
  new ProductsModel(
      prodImgUrl:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRhONq4f30PEF3ekS0d-jPHtSzlb46JKfH4A2SaYTqDLYD22gGuhvifxOZ8-Z3MOkfc06Bup6Vf&usqp=CAc",
      prodName: "Realme Pro",
      prodPrice: "50,007"),
];
