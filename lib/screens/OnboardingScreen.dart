import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:micro_ecommerce/models/onboarding_model.dart';
import 'package:micro_ecommerce/screens/login_screen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/utils/utilities.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnboardingScreen extends StatefulWidget {
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  List<SliderModel> slides = new List<SliderModel>();
  int currentIndex = 0;
  PageController pageController = new PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
    slides = getSlides();
  }

  Widget pageIndexIndicator(bool isCurrentPage) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      height: isCurrentPage ? 10.0 : 6.0,
      width: isCurrentPage ? 10.0 : 6.0,
      decoration: BoxDecoration(
        color: isCurrentPage ? Colors.pink : Colors.pinkAccent[100],
        borderRadius: BorderRadius.circular(12),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: PageView.builder(
          controller: pageController,
          onPageChanged: (val) {
            setState(() {
              currentIndex = val;
            });
          },
          itemCount: slides.length,
          itemBuilder: (context, index) {
            return SliderTile(
              title: slides[index].getTitle(),
              imageAssetsPath: slides[index].getImageAssetPath(),
              desc: slides[index].getDesc(),
            );
          }),
      bottomSheet: currentIndex != slides.length - 1
          ? Container(
              height: Platform.isIOS ? 70 : 60,
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      pageController.animateToPage(slides.length - 1,
                          duration: Duration(milliseconds: 300),
                          curve: Curves.linear);
                    },
                    child: Text(
                      'SKIP',
                      style: TextStyle(
                          color: kAppColor, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      for (int i = 0; i < slides.length; i++)
                        currentIndex == i
                            ? pageIndexIndicator(true)
                            : pageIndexIndicator(false)
                    ],
                  ),
                  GestureDetector(
                    onTap: () async {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.setInt("initScreen", 1);
                      pageController.animateToPage(currentIndex + 1,
                          duration: Duration(milliseconds: 300),
                          curve: Curves.easeIn);
                    },
                    child: Text(
                      'NEXT',
                      style: TextStyle(
                          color: kAppColor, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            )
          : GestureDetector(
              onTap: () {
                Utils.setVisitingFlag();
                debugPrint('navigate to login Screen');
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => LoginScreen()));
              },
              child: Container(
                color: kAppColor,
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                height: Platform.isIOS ? 70 : 60,
                child: Text(
                  "GET STARTED",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w600),
                ),
              ),
            ),
    );
  }
}

class SliderTile extends StatefulWidget {
  final String imageAssetsPath, title, desc;

  SliderTile({this.imageAssetsPath, this.title, this.desc});

  @override
  _SliderTileState createState() => _SliderTileState();
}

class _SliderTileState extends State<SliderTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Welcome to",
            textAlign: TextAlign.center,
            style: GoogleFonts.openSans(
              fontSize: 30.0,
              fontWeight: FontWeight.w300,
            ),
          ),
          Text(
            widget.title,
            textAlign: TextAlign.center,
            style: GoogleFonts.openSans(
              fontSize: 30.0,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Container(
            width: double.infinity,
            height: 200,
            child: Image.asset(widget.imageAssetsPath),
          ),
          SizedBox(
            height: 20.0,
          ),
          Text(
            widget.desc,
            textAlign: TextAlign.center,
            style: GoogleFonts.openSans(
                fontSize: 20.0, fontWeight: FontWeight.w500),
          )
        ],
      ),
    );
  }
}

/*setVisitingFlag() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("alreadyVisited", true);
}

getVisitingFlag() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool alreadyVisited = prefs.getBool("alreadyVisited") ?? false;
  return alreadyVisited;
}*/
