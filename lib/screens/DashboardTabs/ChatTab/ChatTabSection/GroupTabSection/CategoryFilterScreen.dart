import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/models/AddEditProductsModel.dart';
import 'package:micro_ecommerce/models/CategoryModel.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomButton.dart';

import 'GroupProductsScreen.dart';

class CategoryFilterScreen extends StatefulWidget {
  @override
  _CategoryFilterScreenState createState() => _CategoryFilterScreenState();
}

class _CategoryFilterScreenState extends State<CategoryFilterScreen> {
  //list of category
  final List<AddEditProductsModel> filteredProductList = [];

  //list of category
  final List<CategoryModel> category = [];

  //list of category
  final List<CategoryModel> size = [];

  //list of category
  final List<CategoryModel> colors = [];

  //list of category
  final List<CategoryModel> brands = [];

  Color _color;
  RangeValues values = RangeValues(1, 100);
  String _minPrice = "1";
  String _maxPrice = "100";
  RangeLabels labels = RangeLabels("1", "100");
  bool _categoriesLoading = true;
  bool _applyingFilters = false;

  bool _sizeLoading = true;
  bool _colorLoading = true;
  bool _brandsLoading = true;

  List<String> categoryList = [];
  List<String> sizeList = [];
  List<String> colorList = [];
  List<String> brandsList = [];
  String minPriceValue, maxPriceValue;

  @override
  void initState() {
    super.initState();
    getCategoryItems();
  }

  //get all categories
  Future<void> getCategoryItems() async {
    category.clear();
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/categorydetails/AllCategoryDetails";

    var response = await http.get(
      url,
      headers: headers,
    );

    if (response.statusCode == 200) {
      setState(() {
        _categoriesLoading = false;
      });
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      for (Map i in jsonResponse) {
        category.add(CategoryModel.map(i));
        size.add(CategoryModel.map(i));
        colors.add(CategoryModel.map(i));
        brands.add(CategoryModel.map(i));
      }
    } else {
      setState(() {
        _categoriesLoading = false;
      });
      debugPrint(response.statusCode.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        brightness: Brightness.dark,
        elevation: 1,
        titleSpacing: 0,
        leading: Container(
          height: 30,
          width: 30,
          child: IconButton(
              icon: Icon(Icons.filter_list, color: kAppColor, size: 30),
              onPressed: null),
        ),
        title: Text(
          "Category Filter",
          style: TextStyle(fontFamily: kOkraMedium, color: kAppTextColor),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 25.0),
            child: Container(
              height: 30,
              width: 30,
              child: IconButton(
                  icon: Icon(Icons.close, color: kAppColor, size: 30),
                  onPressed: () {
                    debugPrint("Pressed");
                    Navigator.of(context).pop();
                  }),
            ),
          ),
        ],
      ),
      bottomSheet: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(2.0), topRight: Radius.circular(2.0)),
            shape: BoxShape.rectangle,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.55),
                  offset: Offset(0, 0),
                  spreadRadius: 0.4)
            ]),
        height: Platform.isIOS ? 70 : 60,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Expanded(
                child: Container(
                    margin: EdgeInsets.symmetric(vertical: 1, horizontal: 5),
                    child: _applyingFilters == true
                        ? Center(
                            child: CircularProgressIndicator(
                                backgroundColor: kAppColor))
                        : CustomButton(
                        title: "Apply Now", onTapped: () => applyFilter())),
              ),
              VerticalDivider(),
              Expanded(
                child: CustomResetButton(
                  title: "Reset",
                  onTapped: () {
                    _applyingFilters == true ? null : resetFilters();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(25),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //Category section...
              SizedBox(height: 10),
              Text(
                "CATEGORY",
                style: TextStyle(
                    fontFamily: kOkraSemiBold, fontSize: 13, color: kAppColor),
              ),
              SizedBox(height: 15),
              _categoriesLoading == true
                  ? Container(
                width: double.infinity,
                height: 45,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: kAppColor,
                  ),
                ),
              )
                  : Container(height: 45, child: categoryTile(category)),
              //Size section...
              SizedBox(height: 45),
              Text(
                "SIZE",
                style: TextStyle(
                    fontFamily: kOkraSemiBold, fontSize: 13, color: kAppColor),
              ),
              SizedBox(height: 15),
              //todo change to brand
              //_sizeLoading == true
              _categoriesLoading == true
                  ? Container(
                width: double.infinity,
                height: 45,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: kAppColor,
                  ),
                ),
              )
                  : Container(height: 45, child: sizeTile(size)),
              //Colors section...
              SizedBox(height: 45),
              Text(
                "COLOR",
                style: TextStyle(
                    fontFamily: kOkraSemiBold, fontSize: 13, color: kAppColor),
              ),
              SizedBox(height: 15),
              //todo change to color
              //_colorLoading == true
              _categoriesLoading == true
                  ? Container(
                width: double.infinity,
                height: 45,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: kAppColor,
                  ),
                ),
              )
                  : Container(height: 45, child: colorTile(colors)),
              //Brand section...
              SizedBox(height: 45),
              Text(
                "BRANDS",
                style: TextStyle(
                    fontFamily: kOkraSemiBold, fontSize: 13, color: kAppColor),
              ),
              SizedBox(height: 15),
              //todo change to brand
              //_brandsLoading == true
              _categoriesLoading == true
                  ? Container(
                width: double.infinity,
                height: 45,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: kAppColor,
                  ),
                ),
              )
                  : Container(height: 45, child: brandTile(brands)),
              SizedBox(height: 45),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "PRICE",
                    style: TextStyle(
                        fontFamily: kOkraSemiBold,
                        fontSize: 13,
                        color: kAppColor),
                  ),
                  Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Rs." + _minPrice,
                          style: TextStyle(
                              fontFamily: kOkraSemiBold,
                              fontSize: 14,
                              color: kAppColor),
                        ),
                        Text(" - "),
                        Text(
                          "Rs." + _maxPrice,
                          style: TextStyle(
                              fontFamily: kOkraSemiBold,
                              fontSize: 14,
                              color: kAppColor),
                        ),
                      ],
                    ),
                  ),
                ],
              ),

              SizedBox(height: 15),
              priceRangeSlider(),
            ]),
      ),
    );
  }

  //category
  Widget categoryTile(List<CategoryModel> category) {
    return Container(
      width: double.infinity,
      height: 40,
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: category.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  if (category[index].selectCategory == false) {
                    category[index].selectCategory = true;
                    categoryList.add(category[index].categoryName);
                  } else {
                    category[index].selectCategory = false;
                    categoryList.remove(category[index].categoryName);
                  }
                });
              },
              child: Material(
                elevation: 1,
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 1),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: category[index].selectCategory == true
                        ? kAppColor
                        : Colors.pinkAccent.withOpacity(0.2),
                    border: Border.all(
                        width: 1,
                        color: category[index].selectCategory == true
                            ? kAppColor
                            : Colors.grey.withOpacity(0.6)),
                  ),
                  child: Text(
                    category[index].categoryName,
                    style: TextStyle(
                        fontFamily: kOkraRegular,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: category[index].selectCategory == true
                            ? Colors.white
                            : kAppColor),
                  ),
                ),
              ),
            );
          }),
    );
  }

  //size
  Widget sizeTile(List<CategoryModel> category) {
    return Container(
      width: double.infinity,
      height: 40,
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: size.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  if (category[index].selectCategory == false) {
                    category[index].selectCategory = true;
                    sizeList.add(category[index].categoryName);
                  } else {
                    category[index].selectCategory = false;
                    sizeList.remove(category[index].categoryName);
                  }
                });
              },
              child: Material(
                elevation: 1,
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 1),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: category[index].selectCategory == true
                        ? kAppColor
                        : Colors.pinkAccent.withOpacity(0.2),
                    border: Border.all(
                        width: 1,
                        color: category[index].selectCategory == true
                            ? kAppColor
                            : Colors.grey.withOpacity(0.6)),
                  ),
                  child: Text(
                    category[index].categoryName,
                    style: TextStyle(
                        fontFamily: kOkraRegular,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: category[index].selectCategory == true
                            ? Colors.white
                            : kAppColor),
                  ),
                ),
              ),
            );
          }),
    );
  }

  //color
  Widget colorTile(List<CategoryModel> category) {
    return Container(
      width: double.infinity,
      height: 40,
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: colors.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  if (category[index].selectCategory == false) {
                    category[index].selectCategory = true;
                    colorList.add(category[index].categoryName);
                  } else {
                    category[index].selectCategory = false;
                    colorList.remove(category[index].categoryName);
                  }
                });
              },
              child: Stack(
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: Colors.pink[index * 100],
                    radius: 32,
                  ),
                  Positioned(
                      right: 5,
                      bottom: 5,
                      left: 5,
                      top: 5,
                      child: Icon(Icons.check,
                          size: 25,
                          color: category[index].selectCategory == true
                              ? Colors.white
                              : Colors.transparent)),
                ],
              ),
            );
          }),
    );
  }

  //size
  Widget brandTile(List<CategoryModel> category) {
    return Container(
      width: double.infinity,
      height: 40,
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: brands.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  if (category[index].selectCategory == false) {
                    category[index].selectCategory = true;
                    brandsList.add(category[index].categoryName);
                  } else {
                    category[index].selectCategory = false;
                    brandsList.remove(category[index].categoryName);
                  }
                });
              },
              child: Material(
                elevation: 1,
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 1),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: category[index].selectCategory == true
                        ? kAppColor
                        : Colors.pinkAccent.withOpacity(0.2),
                    border: Border.all(
                        width: 1,
                        color: category[index].selectCategory == true
                            ? kAppColor
                            : Colors.grey.withOpacity(0.6)),
                  ),
                  child: Text(
                    category[index].categoryName,
                    style: TextStyle(
                        fontFamily: kOkraRegular,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: category[index].selectCategory == true
                            ? Colors.white
                            : kAppColor),
                  ),
                ),
              ),
            );
          }),
    );
  }

  //price
  Widget priceRangeSlider() {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(),
      child: RangeSlider(
          activeColor: kAppColor,
          inactiveColor: Colors.pinkAccent.withOpacity(0.5),
          min: 1,
          max: 100,
          values: values,
          labels: RangeLabels(values.start.toString(), values.end.toString()),
          onChanged: (value) {
            setState(() {
              //change it round figure
              _minPrice = value.start.toStringAsFixed(0);
              _maxPrice = value.end.toStringAsFixed(0);
//              _maxPrice = value.end.toStringAsFixed(2);
              debugPrint(value.toString());
              if (value.end - value.start >= 10) {
                values = value;
              } else {
                if (values.start == value.start) {
                  values = RangeValues(values.start, values.start + 10);
                } else {
                  values = RangeValues(values.end - 10, values.end);
                }
              }
            });
          }),
    );
  }

  //Api call to applyFilter
  Future<void> applyFilter() async {
    setState(() {
      _applyingFilters = true;
    });
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/Productdetails/ProductDetailsbyfilter";

    Map<String, dynamic> data = {
      "Category": categoryList.toString(),
      "Size": sizeList.toString(),
      "Color": colorList.toString(),
      "Brand": brandsList.toString(),
      "Price": _minPrice + "." + _maxPrice,
    };

    debugPrint(data.toString());

    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      setState(() {
        _applyingFilters = false;
      });

      //navigate user to "GroupProductsScreen" with the result from this
      // response
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      for (Map i in jsonResponse) {
        filteredProductList.add(AddEditProductsModel.map(i));
      }

      //passing filtered products lists to product listing page

      Navigator.of(context).pop(MaterialPageRoute(
          builder: (BuildContext context) =>
              GroupProductsScreen(
                filteredProductList: filteredProductList,
              )));

      /*Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (BuildContext context) => GroupProductsScreen(
                    filteredProductList: filteredProductList,
                  )),
          (Route<dynamic> route) => false);*/
    } else {
      setState(() {
        _applyingFilters = false;
      });
      debugPrint(response.statusCode.toString());
    }
  }

  //clear every filter...
  resetFilters() {
    setState(() {
      categoryList.clear();
      sizeList.clear();
      colorList.clear();
      brandsList.clear();
    });
  }
}

class CustomResetButton extends StatelessWidget {
  final String title;
  final Function onTapped;

  CustomResetButton({this.title, this.onTapped});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapped,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                Colors.grey[400],
                Colors.grey[400],
                Colors.grey[400],
                Colors.grey[400],
                Colors.grey[400],
                Colors.grey[500]
              ]),
        ),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
                fontSize: 15.0,
                color: Colors.white,
                letterSpacing: 1,
                fontWeight: FontWeight.w600),
          ),
        ),
      ),
    );
  }
}
