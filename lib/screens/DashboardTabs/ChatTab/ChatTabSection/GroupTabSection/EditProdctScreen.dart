import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:micro_ecommerce/models/CategoryModel.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomSingleButton.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'AddEditProductListScreen.dart';

class EditProductScreen extends StatefulWidget {
  final String productId;

  EditProductScreen(this.productId);

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  TextEditingController productNameController = new TextEditingController();
  TextEditingController productPriceController = new TextEditingController();
  TextEditingController productDescController = new TextEditingController();
  TextEditingController impInfoController = new TextEditingController();
  TextEditingController prodLongDescController = new TextEditingController();
  TextEditingController _addCategory = new TextEditingController();
  TextEditingController productBrandController = new TextEditingController();

  TextEditingController _addSize = new TextEditingController();
  bool addingSize = false;

  bool addingCategory = false;
  bool editingProduct = false;
  Map<String, dynamic> cat;

  String _selectedValue = "";

  //Group Status
  var _groupStatus = ['Select Status', 'Active', 'Inactive'];
  var _currentGroupStatus = 'Select Status';

  //Group Name
//  var _groupName = ['Select Group Name', 'Group Name 1', 'Group Name 2'];
//  var _currentGroupName = 'Select Group Name';

  //Stock Status
  var _stockStatus = ['Select Stock Status', 'In Stock', 'Out of Stock'];
  var _currentStockStatus = 'Select Stock Status';

  //Shipping By
  var _shippingBy = ['Select Shipping By', 'Self', 'Third Party'];
  var _currentShippingBy = 'Select Shipping By';

  List<String> imageUrls = <String>[];

  final List<String> categories = <String>[];
  bool loadingCategory = false;
  List<CategoryModel> category = [];
  String display, value, _error, id;
  String productImage1,
      productImage2,
      productImage3,
      productImage4,
      productImage5,
      productImage6;

  //showing and selection of Categories...
  List<String> programmingList = [];
  List<String> selectedProgrammingList = List();
  String groupName;
  List<File> productImages = List<File>();
  List<String> imageBase64 = <String>[];

  //showing and selection of Size...
  List<String> sizeList = [];
  List<String> selectedSizeList = List();

  //selected colors
  List<Color> colorsList = [];

  _showDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          //Here we will build the content of the dialog
          return AlertDialog(
            title: Text(
              "Select Categories",
              style: TextStyle(fontFamily: kOkraMedium, fontSize: 18),
            ),
            content: MultiSelectChip(
              programmingList,
              onSelectionChanged: (selectedList) {
                setState(() {
                  selectedProgrammingList = selectedList;
                });
              },
            ),
            actions: <Widget>[
              FlatButton(
                splashColor: Colors.pinkAccent,
                color: kAppColor,
                child: Text(
                  "Select",
                  style: TextStyle(
                      fontFamily: kOkraMedium,
                      fontSize: 15,
                      color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  void initState() {
    // getting category list
    super.initState();
    getCategoryItems();
    getProductDetail(widget.productId);
  }

  Future<void> getProductDetail(String productId) async {
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/Productdetails/ProductDetailsbyId";
    Map<String, dynamic> data = {
      'Id': productId,
    };

    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      debugPrint(jsonResponse);
      id = jsonResponse['id'].toString();
      String category = jsonResponse['category'].toString();
      String status = jsonResponse['status'].toString();
      String productName = jsonResponse['productName'].toString();
      groupName = jsonResponse['groupName'].toString();
      String price = jsonResponse['price'].toString();
      String stockStatus = jsonResponse['stockStatus'].toString();
      String shippingBy = jsonResponse['shippingBy'].toString();
      String productDescriptions =
          jsonResponse['productDescriptions'].toString();
      String importantInformation =
          jsonResponse['importanatInformation'].toString();
      String productLongDescription =
          jsonResponse['productLongDescription'].toString();
      String prodImage1 = jsonResponse['productImage1'].toString();
      String prodImage2 = jsonResponse['productImage2'].toString();
      String prodImage3 = jsonResponse['productImage3'].toString();
      String prodImage4 = jsonResponse['productImage4'].toString();
      String prodImage5 = jsonResponse['productImage5'].toString();
      String prodImage6 = jsonResponse['productImage6'].toString();
      String prodtSize = jsonResponse['productSize'].toString();
      String prodColor = jsonResponse['productColor'].toString();
      String prodBrand = jsonResponse['productBrand'].toString();
      String createdByUser = jsonResponse['createdByUser'].toString();

      setState(() {
        //setting initial values...
        _currentGroupStatus = status;
        productNameController = new TextEditingController(text: productName);
        productPriceController = new TextEditingController(text: price);
        _currentStockStatus = stockStatus;
        _currentShippingBy = shippingBy;
        productDescController =
            new TextEditingController(text: productDescriptions);
        impInfoController =
            new TextEditingController(text: importantInformation);
        prodLongDescController =
            new TextEditingController(text: productDescriptions);
        selectedProgrammingList.add(category);
        selectedSizeList.add(prodtSize == "" ? "Select/Add Size" : prodtSize);
      });
    } else {
      debugPrint(response.statusCode.toString());
    }
  }

  Future<void> getCategoryItems() async {
    category.clear();
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/categorydetails/AllCategoryDetails";

    var response = await http.get(
      url,
      headers: headers,
    );

    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      for (Map i in jsonResponse) {
        category.add(CategoryModel.map(i));
      }
      getCategoryName();
    } else {
      debugPrint(response.statusCode.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      bottomSheet: editingProduct == true
          ? Material(
              elevation: 5,
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                height: Platform.isIOS ? 70 : 60,
                child: Center(
                  child: Container(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(
                      backgroundColor: kAppColor,
                    ),
                  ),
                ),
              ),
            )
          : CustomSingleButton(
              title: "Edit Product",
              onTapped: () {
                _editProduct();
              },
            ),
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 5,
        titleSpacing: 0,
        title: Text(
          "Edit Product",
          style: TextStyle(fontFamily: kOkraMedium, color: kAppTextColor),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: kAppColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        actions: <Widget>[
          PopupMenuButton(
            icon: Icon(
              Icons.delete,
              size: 25,
              color: kAppColor,
            ),
            itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: 'Delete Product',
                child: Text(
                  'Delete Product',
                  style: TextStyle(fontSize: 15, fontFamily: kOkraMedium),
                ),
              ),
            ],
            onSelected: (value) {
              if (value == "Delete Product") {
                deleteProduct();
              }
            },
          ),
          SizedBox(width: 5),
        ],
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            "assets/images/background.png",
            fit: BoxFit.cover,
          ),
          SingleChildScrollView(
            primary: true,
            child: Container(
              margin: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //AddCategorySection
                  Text(
                    "Category",
                    style: TextStyle(
                        fontFamily: kOkraMedium, color: kAppTextColor),
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          child: Material(
                            shadowColor: Colors.grey,
                            color: Colors.white,
                            elevation: 2,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                            ),
                            child: Material(
                              shadowColor: Colors.grey,
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8),
                                bottomLeft: Radius.circular(8),
                              ),
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                alignment: Alignment.centerLeft,
                                height: 50,
                                child: Text(
                                  selectedProgrammingList.length == 0
                                      ? "Select Category"
                                      : selectedProgrammingList.join(","),
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontFamily: kOkraMedium, fontSize: 16),
                                ),
                              ),
                            ),
                          ),
                          onTap: _showDialog, //select category dialog
                        ),
                      ),
                      SizedBox(
                        width: 0.5,
                      ),
                      InkWell(
                        onTap: () {
                          _showCategoryDialog(); //Add category dialog
                        },
                        child: Material(
                          shadowColor: Colors.grey,
                          color: Colors.white,
                          elevation: 2,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(8),
                            bottomRight: Radius.circular(8),
                          ),
                          child: Container(
                            height: 50,
                            width: 50,
                            alignment: Alignment.center,
                            child: Icon(
                              Icons.add,
                              size: 30,
                              color: kAppColor,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),

                  //Add/SelectSizeSection
                  SizedBox(height: 15),
                  Text(
                    "Size",
                    style: TextStyle(
                        fontFamily: kOkraMedium, color: kAppTextColor),
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          child: Material(
                            shadowColor: Colors.grey,
                            color: Colors.white,
                            elevation: 2,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                            ),
                            child: Material(
                              shadowColor: Colors.grey,
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8),
                                bottomLeft: Radius.circular(8),
                              ),
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                alignment: Alignment.centerLeft,
                                height: 50,
                                child: Text(
                                  selectedSizeList.length == 0
                                      ? "Select/Add Size"
                                      : selectedSizeList.join(","),
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontFamily: kOkraMedium, fontSize: 16),
                                ),
                              ),
                            ),
                          ),
                          onTap: _showSelectSizeDialog, //select Size dialog
                        ),
                      ),
                      SizedBox(
                        width: 0.5,
                      ),
                      InkWell(
                        onTap: () {
                          _showAddSizeDialog(); //Add Size dialog
                        },
                        child: Material(
                          shadowColor: Colors.grey,
                          color: Colors.white,
                          elevation: 2,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(8),
                            bottomRight: Radius.circular(8),
                          ),
                          child: Container(
                            height: 50,
                            width: 50,
                            alignment: Alignment.center,
                            child: Icon(
                              Icons.add,
                              size: 30,
                              color: kAppColor,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),

                  //Group Status Dropdown
                  SizedBox(height: 20),
                  Text(
                    "Group Status",
                    style: TextStyle(
                        color: kAppTextColor, fontFamily: kOkraMedium),
                  ),
                  groupStatusDropDown(),
                  //Product Name
                  SizedBox(height: 10),
                  TextField(
                    controller: productNameController,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: kOkraMedium,
                        color: kAppTextColor),
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.grey.withOpacity(0.30)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: kAppTextColor),
                      ),
                      labelText: "Product Name",
                      labelStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                  //Group Name Dropdown
//                  SizedBox(height: 20),
//                  Text(
//                    "Group Name",
//                    style: TextStyle(
//                        color: kAppTextColor, fontFamily: kOkraMedium),
//                  ),
//                  groupNameDropDown(),
                  //Product Price
                  SizedBox(height: 10),
                  TextField(
                    keyboardType: TextInputType.number,
                    controller: productPriceController,
                    style: TextStyle(
                        fontSize: 18,
                        fontFamily: kOkraMedium,
                        color: kAppTextColor),
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.grey.withOpacity(0.30)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: kAppTextColor),
                      ),
                      labelText: "Product Price",
                      labelStyle: TextStyle(color: Colors.grey),
                    ),
                  ),

                  //Product Brand Name
                  SizedBox(height: 10),
                  TextField(
                    keyboardType: TextInputType.text,
                    controller: productBrandController,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: kOkraMedium,
                        color: kAppTextColor),
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                        BorderSide(color: Colors.grey.withOpacity(0.30)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: kAppTextColor),
                      ),
                      labelText: "Brand Name",
                      labelStyle: TextStyle(color: Colors.grey),
                    ),
                  ),

                  //Product Color Variants
                  SizedBox(height: 10),
                  Text(
                    "Color Variants",
                    style: TextStyle(
                        color: kAppTextColor, fontFamily: kOkraMedium),
                  ),
                  SizedBox(height: 10),
                  InkWell(
                    splashColor: kAppColor,
                    onTap: () {
                      _colorPicker();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: RichText(
                        textAlign: TextAlign.justify,
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(
                                Icons.color_lens,
                                size: 19,
                                color: kAppColor,
                              ),
                            ),
                            TextSpan(
                              text: " Click to select colors of product"
                                  "(Max 6)",
                              style: TextStyle(
                                  fontFamily: kOkraSemiBold,
                                  color: Colors.grey,
                                  fontSize: 14),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  colorsList == null || colorsList.length == 0
                      ? SizedBox(height: 0)
                      : SizedBox(height: 10),
                  colorsList == null || colorsList.length == 0
                      ? Container()
                      : colorTile(colorsList),

                  //Product Stock Status Dropdown
                  SizedBox(height: 20),
                  Text(
                    "Stock Status",
                    style: TextStyle(
                        color: kAppTextColor, fontFamily: kOkraMedium),
                  ),
                  stockStatusDropDown(),
                  //Shipping By Dropdown
                  SizedBox(height: 20),
                  Text(
                    "Shipping By",
                    style: TextStyle(
                        color: kAppTextColor, fontFamily: kOkraMedium),
                  ),
                  shippingByDropDown(),
                  //Product Description
                  SizedBox(height: 10),
                  TextField(
                    maxLength: 500,
                    controller: productDescController,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: kOkraMedium,
                        color: kAppTextColor),
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.grey.withOpacity(0.30)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: kAppTextColor),
                      ),
                      labelText: "Product Description / Specifications",
                      labelStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                  //Important Information
                  SizedBox(height: 10),
                  TextField(
                    controller: impInfoController,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: kOkraMedium,
                        color: kAppTextColor),
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.grey.withOpacity(0.30)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: kAppTextColor),
                      ),
                      labelText: "Important Information",
                      labelStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                  //Product Long Information
                  SizedBox(height: 10),
                  TextField(
                    controller: prodLongDescController,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: kOkraMedium,
                        color: kAppTextColor),
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.grey.withOpacity(0.30)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: kAppTextColor),
                      ),
                      labelText: "Product Long Information",
                      labelStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                  //Upload Product Images Section
                  SizedBox(height: 20),
                  Text(
                    "Product images",
                    style: TextStyle(
                        color: kAppTextColor, fontFamily: kOkraMedium),
                  ),
                  SizedBox(height: 10),
                  InkWell(
                    splashColor: kAppColor,
                    onTap: () {
                      debugPrint("select images here");
//                      loadAssets();
                      _galleryPhotoIdImagePicker();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: RichText(
                        textAlign: TextAlign.justify,
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(
                                Icons.add_photo_alternate,
                                size: 20,
                                color: Colors.grey,
                              ),
                            ),
                            TextSpan(
                              text: " Click here to select (Max 6)",
                              style: TextStyle(
                                  fontFamily: kOkraSemiBold,
                                  color: Colors.grey,
                                  fontSize: 14),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      height: productImages.length > 0 ? size.height * .3 : 5.0,
                      child: buildGridView()),
                  SizedBox(height: 60),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  //Group Status
  Widget groupStatusDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupStatus.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropDownGroupOptionItemSelected(value);
        },
        value: _currentGroupStatus,
      ),
    );
  }

  void _dropDownGroupOptionItemSelected(String value) {
    setState(() {
      _currentGroupStatus = value;
    });
  }

  /*//Group Name
  Widget groupNameDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupName.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropDownGroupNameItemSelected(value);
        },
        value: _currentGroupName,
        hint: Text(
          "Select Group Name",
          style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
        ),
      ),
    );
  }

  void _dropDownGroupNameItemSelected(String value) {
    setState(() {
      this._currentGroupName = value;
    });
  }
*/
  //Stock Status
  Widget stockStatusDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _stockStatus.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropDownStockStatusItemSelected(value);
        },
        value: _currentStockStatus,
        hint: Text(
          "Stock Status",
          style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
        ),
      ),
    );
  }

  void _dropDownStockStatusItemSelected(String value) {
    setState(() {
      this._currentStockStatus = value;
    });
  }

  //Shipping By
  Widget shippingByDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _shippingBy.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropDownShippingByItemSelected(value);
        },
        hint: Text(
          "Shipping By",
          style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
        ),
        value: _currentShippingBy,
      ),
    );
  }

  void _dropDownShippingByItemSelected(String value) {
    setState(() {
      this._currentShippingBy = value;
    });
  }

  Widget buildGridView() {
    return GridView.count(
      primary: false,
      crossAxisCount: 3,
      children: List.generate(productImages.length, (index) {
        File file = productImages[index];
        return Stack(
          children: <Widget>[
            Card(
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(4)),
                child: Container(
                  width: 100,
                  height: 100,
                  child: Image.file(
                    file,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 6,
              right: 6,
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    productImages.removeAt(index);
                  });
                },
                child: Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.85),
                          offset: Offset(0, 0),
                          spreadRadius: 0)
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Icon(
                      Icons.close,
                      size: 15,
                      color: kAppColor,
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      }),
    );
  }

  void _showCategoryDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text(
              'Category Name',
              style: TextStyle(fontFamily: kOkraMedium),
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 6.0),
                  child: Center(
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: "type here...",
                        hintStyle: TextStyle(fontFamily: kOkraRegular),
                      ),
                      controller: _addCategory,
                    ),
                  ),
                ),
              ],
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: addingCategory == false
                    ? RaisedButton(
                        color: kAppColor,
                        onPressed: () async {
                          String category = _addCategory.text;
                          if (category.isNotEmpty) {
                            setState(() {
                              addingCategory = true;
                            });
                            const headers = {
                              'Content-Type': 'application/json'
                            };
                            String url =
                                "http://microecommerce.flicklead.com/api/categorydetails/CreateCategoryDetails";

                            Map<String, dynamic> data = {
                              "CategoryName": category,
                            };

                            var response = await http.post(
                              url,
                              headers: headers,
                              body: json.encode(data),
                            );

                            if (response.statusCode == 200) {
                              setState(() {
                                addingCategory = false;
                              });
                              _addCategory.clear(); //clearing controller
                              Fluttertoast.showToast(
                                  msg: "Categories Added Successfully",
                                  textColor: Colors.white,
                                  backgroundColor: kAppColor,
                                  gravity: ToastGravity.CENTER);
                              setState(() {
                                getCategoryItems();
                              });
                              Navigator.of(context).pop();
                            } else {
                              setState(() {
                                addingCategory = false;
                              });
                              debugPrint(response.statusCode.toString());
                            }
                          }
                        },
                        child: Center(
                          child: Text(
                            'Add',
                            style: TextStyle(
                                color: Colors.white, fontFamily: kOkraMedium),
                          ),
                        ),
                      )
                    : Material(
                        elevation: 5,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.white,
                          child: Center(
                            child: Container(
                              height: 30,
                              width: 30,
                              child: CircularProgressIndicator(
                                backgroundColor: kAppColor,
                              ),
                            ),
                          ),
                        ),
                      ),
              ),
            ],
          );
        });
  }

  void getCategoryName() {
    programmingList.clear();
    for (var value in category) {
      display = value.categoryName.toString();
      programmingList.add(display);
      debugPrint("CategoryName" + display);
    }
  }

  Future<void> _editProduct() async {
    setState(() {
      editingProduct = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String prodImg;
    int len = imageBase64.length;
    int i = 0;
    for (var v = 0; v < len; v++) {
      try {
        prodImg = imageBase64[i];
      } on Exception catch (e) {
        debugPrint(e.toString());
      }
      debugPrint("mj -" + prodImg);
      if (i == 0) {
        productImage1 = prodImg;
      } else if (i == 1) {
        productImage2 = prodImg;
      } else if (i == 2) {
        productImage3 = prodImg;
      } else if (i == 3) {
        productImage4 = prodImg;
      } else if (i == 4) {
        productImage5 = prodImg;
      } else if (i == 5) {
        productImage6 = prodImg;
      }
      i++;
    }
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/Productdetails/EditProduct";

    Map<String, dynamic> data = {
      "Id": id,
      "Category": selectedProgrammingList
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", ""),
      "Status": _currentGroupStatus,
      "ProductName": productNameController.text,
      "GroupName": groupName,
      "Price": productPriceController.text,
      "StockStatus": _currentStockStatus,
      "ShippingBy": _currentShippingBy,
      "ProductDescriptions": productDescController.text,
      "ImportanatInformation": impInfoController.text,
      "ProductLongDescription": prodLongDescController.text,
      "ProductSize": "X", //Size hardcoded for now
      "ProductColor": "Black", //Color hardcoded for now
      "ProductBrand": "Nike", //Product hardcoded for now
      "CreatedByUser": prefs.getString("userId"), //userId
      "ProductImage1": productImage1,
      "ProductImage2": productImage2,
      "ProductImage3": productImage3,
      "ProductImage4": productImage4,
      "ProductImage5": productImage5,
      "ProductImage6": productImage6,
    };

    debugPrint(data.toString());
    var response = await http.put(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      debugPrint(response.body.toString());
      /*Fluttertoast.showToast(
          msg: "Product Edited Successfully",
          textColor: Colors.white,
          backgroundColor: kAppColor,
          gravity: ToastGravity.CENTER);

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => Dashboard()),
          (Route<dynamic> route) => false);*/

      String urlImg =
          "https://microecommercephoto.flicklead.com/api/ProductPhoto/savephoto";
      Map<String, dynamic> data = {
        "imgname": productNameController.text,
        "img1": productImage1 == null ? "" : productImage1,
        "img2": productImage2 == null ? "" : productImage2,
        "img3": productImage3 == null ? "" : productImage3,
        "img4": productImage4 == null ? "" : productImage4,
        "img5": productImage5 == null ? "" : productImage5,
        "img6": productImage6 == null ? "" : productImage6,
      };

      var responseImg = await http.post(
        urlImg,
        headers: headers,
        body: json.encode(data),
      );

      //2nd api response
      if (responseImg.statusCode == 200) {
        setState(() {
          editingProduct = false;
        });
        Fluttertoast.showToast(
            msg: "Product edited Successfully",
            textColor: Colors.white,
            backgroundColor: kAppColor,
            gravity: ToastGravity.CENTER);

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    AddEditProductListScreen(
                      groupName: groupName,
                    )),
                (Route<dynamic> route) => false);
      } else {
        setState(() {
          editingProduct = false;
        });
      }
    } else {
      setState(() {
        editingProduct = false;
      });
      debugPrint(response.statusCode.toString());
    }
  }

  Future _galleryPhotoIdImagePicker() async {
    File galleryFile;
    debugPrint('gallery');
    galleryFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (galleryFile == null) return;
    String base64Image = base64Encode(galleryFile.readAsBytesSync());
//    String base64Image = "mj";
    productImages.add(galleryFile);
    imageBase64.add(base64Image);
    setState(() {
      buildGridView();
    });
  }

  Future<void> deleteProduct() async {
    const headers = {'Content-Type': 'application/json'};
    String deleteProduct =
        "http://microecommerce.flicklead.com/api/Productdetails/DeleteProduct";
    Map<String, dynamic> data = {
      "Id": widget.productId,
    };

    var response = await http.post(
      deleteProduct,
      headers: headers,
      body: json.encode(data),
    );

    debugPrint("delete product api response" + response.statusCode.toString());
    if (response.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "Product deleted Successfully.",
          textColor: Colors.white,
          backgroundColor: kAppColor,
          gravity: ToastGravity.CENTER);

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  AddEditProductListScreen(
                    groupName: groupName,
                  )),
              (Route<dynamic> route) => false);
    } else {
      Fluttertoast.showToast(
          msg: "Unable to delete the delete.",
          textColor: Colors.white,
          backgroundColor: kAppColor,
          gravity: ToastGravity.CENTER);
      return;
    }
  }

  //Select size dialog
  _showSelectSizeDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          //Here we will build the content of the dialog
          return AlertDialog(
            title: Text(
              "Select Sizes",
              style: TextStyle(fontFamily: kOkraMedium, fontSize: 18),
            ),
            content: MultiSelectChip(
              sizeList,
              onSelectionChanged: (selectedList) {
                setState(() {
                  selectedSizeList = selectedList;
                });
              },
            ),
            actions: <Widget>[
              FlatButton(
                splashColor: Colors.pinkAccent,
                color: kAppColor,
                child: Text(
                  "Select",
                  style: TextStyle(
                      fontFamily: kOkraMedium,
                      fontSize: 15,
                      color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  //Show Add size dialog
  void _showAddSizeDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text(
              'Sizes',
              style: TextStyle(fontFamily: kOkraMedium),
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 6.0),
                  child: Center(
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: "type here...",
                        hintStyle: TextStyle(fontFamily: kOkraRegular),
                      ),
                      controller: _addSize,
                    ),
                  ),
                ),
              ],
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: addingSize == false
                    ? RaisedButton(
                  color: kAppColor,
                  onPressed: () async {
                    String size = _addSize.text;
                    if (size.isNotEmpty) {
                      setState(() {
                        addingSize = true;
                      });
                      const headers = {
                        'Content-Type': 'application/json'
                      };
                      String url =
                          "https://microecommerce.flicklead.com/api/sizedetails/CreateSizeDetails";

                      Map<String, dynamic> data = {
                        "size": size,
                      };

                      var response = await http.post(
                        url,
                        headers: headers,
                        body: json.encode(data),
                      );

                      if (response.statusCode == 200) {
                        setState(() {
                          addingSize = false;
                        });
                        _addSize.clear(); //clearing controller
                        Fluttertoast.showToast(
                            msg: "Size Added Successfully",
                            textColor: Colors.white,
                            backgroundColor: kAppColor,
                            gravity: ToastGravity.CENTER);
                        setState(() {
                          getCategoryItems();
                        });
                        Navigator.of(context).pop();
                      } else {
                        setState(() {
                          addingSize = false;
                        });
                        debugPrint(response.statusCode.toString());
                      }
                    }
                  },
                  child: Center(
                    child: Text(
                      'Add',
                      style: TextStyle(
                          color: Colors.white, fontFamily: kOkraMedium),
                    ),
                  ),
                )
                    : Material(
                  elevation: 5,
                  child: Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    color: Colors.white,
                    child: Center(
                      child: Container(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator(
                          backgroundColor: kAppColor,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  void _colorPicker() {
    // create some values
    Color pickerColor = Color(0xff443a49);
    Color currentColor = Color(0xff443a49);

// ValueChanged<Color> callback
    void changeColor(Color color) {
      setState(() => pickerColor = color);
      debugPrint("________________");
      debugPrint(pickerColor.toString());
      Fluttertoast.showToast(msg: pickerColor.toString() + "selected");
      colorsList.add(pickerColor);
      debugPrint("________________");
    }

// raise the [showDialog] widget
    showDialog(
      context: context,
      child: AlertDialog(
        title: const Text('Pick product color!'),
        content: SingleChildScrollView(
//          child: ColorPicker(
//            pickerColor: pickerColor,
//            onColorChanged: changeColor,
//            showLabel: true,
//            pickerAreaHeightPercent: 0.8,
//          ),

          // Use Material color picker:
          child: MaterialPicker(
            pickerColor: pickerColor,
            onColorChanged: changeColor,
            enableLabel: true, // only on portrait mode
          ),
          //
          // Use Block color picker:
          //
          // child: BlockPicker(
          //   pickerColor: currentColor,
          //   onColorChanged: changeColor,
          // ),
        ),
        actions: <Widget>[
          FlatButton(
            child: const Text('Select'),
            onPressed: () {
              setState(() => currentColor = pickerColor);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  //color
  Widget colorTile(List<Color> colorsList) {
    return Material(
      borderRadius: BorderRadius.circular(30),
      elevation: 2,
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        padding: EdgeInsets.all(2),
        width: double.infinity,
        height: 40,
        child: ListView.builder(
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: colorsList.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    colorsList.remove(colorsList[index]);
                  });
                },
                child: Stack(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: colorsList[index],
                      radius: 25,
                    ),
                    Positioned(
                      right: 5,
                      bottom: 5,
                      left: 5,
                      top: 5,
                      child: Icon(Icons.close, size: 18, color: Colors.white),
                    ),
                  ],
                ),
              );
            }),
      ),
    );
  }
}

class MultiSelectChip extends StatefulWidget {
  final List<String> reportList;
  final Function(List<String>) onSelectionChanged;

  MultiSelectChip(this.reportList, {this.onSelectionChanged});

  @override
  _MultiSelectChipState createState() => _MultiSelectChipState();
}

class _MultiSelectChipState extends State<MultiSelectChip> {
  List<String> selectedChoices = List();

  _buildChoiceList() {
    List<Widget> choices = List();
    widget.reportList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.all(2.0),
        child: ChoiceChip(
          backgroundColor: kAppColor,
          label: Text(
            item,
            style: TextStyle(fontFamily: kOkraRegular, color: Colors.white),
          ),
          selected: selectedChoices.contains(item),
          onSelected: (selected) {
            setState(() {
              selectedChoices.contains(item)
                  ? selectedChoices.remove(item)
                  : selectedChoices.add(item);
              widget.onSelectionChanged(selectedChoices);
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: _buildChoiceList(),
    );
  }
}
