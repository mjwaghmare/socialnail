import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupsTab.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/MyGroupTab.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/PersonalTab.dart';
import 'package:micro_ecommerce/utils/constants.dart';

class ChatsTab extends StatefulWidget {
  @override
  _ChatsTabState createState() => _ChatsTabState();
}

class _ChatsTabState extends State<ChatsTab> {
  ScrollController _scrollController;
  Color _theme;
  bool _pic;

  @override
  void initState() {
    super.initState();
    _theme = Colors.white;
    _pic = false;

    _scrollController = ScrollController()
      ..addListener(
        () => _isAppBarExpanded
            ? _theme != kAppColor
                ? setState(
                    () {
                      _pic = true;
                      _theme = kAppColor;
                      debugPrint('setState is called');
                    },
                  )
                : {}
            : _theme != Colors.white
                ? setState(() {
                    _pic = false;
                    debugPrint('setState is called');
                    _theme = Colors.white;
                  })
                : {},
      );
  }

  bool get _isAppBarExpanded {
    return _scrollController.hasClients &&
        _scrollController.offset > (200 - kToolbarHeight);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 3,
        child: NestedScrollView(
          controller: _scrollController,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor:
                    _pic == false ? Colors.transparent : Colors.white,
                leading: _pic == false
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipOval(
                          child: Image.network(
                            "https://images.pexels.com/photos/807598/pexels-photo-807598.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                centerTitle: _pic == false ? true : false,
                title: Text(
                  "Jennifer Anniston",
                  style: TextStyle(
                    fontFamily: kOkraMedium,
                    color: _pic == false ? Colors.white : kAppTextColor,
                    fontSize: 20.0,
                  ),
                ),
                bottom: TabBar(
                    indicatorColor: kAppColor,
                    indicatorWeight: 4.0,
                    unselectedLabelColor: Colors.grey,
                    labelColor: kAppColor,
                    labelStyle: TextStyle(
                        fontSize: 15,
                        color: kAppColor,
                        fontFamily: kOkraRegular,
                        fontWeight: FontWeight.w600),
                    tabs: [
                      Tab(text: "Groups"),
                      Tab(text: "Personal"),
                      Tab(text: "My Groups"),
                    ]),
                actions: <Widget>[
                  Icon(
                    Icons.search,
                    color: _theme,
                  ),
                  SizedBox(width: 10),
                  Icon(
                    Icons.more_vert,
                    color: _theme,
                  ),
                  SizedBox(width: 10),
                ],
                expandedHeight: MediaQuery.of(context).size.height * 0.40,
                pinned: true,
                floating: false,
                flexibleSpace: FlexibleSpaceBar(
                  background: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Image.network(
                        "https://images.pexels.com/photos/807598/pexels-photo-807598.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
                        fit: BoxFit.cover,
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        left: 0,
                        child: Container(
                          width: double.infinity,
                          height: 50,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ];
          },
          body: TabBarView(
            children: [
              GroupsTab(),
              PersonalTabScreen(),
              MyGroupTabScreen(),
            ],
          ),
        ),
      ),
    );
  }
}
