import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/models/LoggedInUserModel.dart';
import 'package:micro_ecommerce/screens/VerifyMobileScreen.dart';
import 'package:micro_ecommerce/screens/dashboard.dart';
import 'package:micro_ecommerce/screens/signup_screen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/utils/utilities.dart';
import 'package:micro_ecommerce/widgets/ButtonBottomSheet.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setInt("initScreen", 1);
}

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var email, userEmailId, userMobile, userId, userName;
  bool loading = false;
  final GlobalKey<FormState> _globalKey = new GlobalKey<FormState>();
  ProgressDialog pr;
  var screenHeight, screenWidth;
  bool passwordVisible = false;

  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, isDismissible: false);
    pr.style(
      message: 'Please wait...',
      progressWidget: CircularProgressIndicator(),
      borderRadius: 8.0,
      backgroundColor: Colors.white,
      messageTextStyle: TextStyle(
          color: kAppColor, fontSize: 18.0, fontWeight: FontWeight.w500),
    );
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
          body: SingleChildScrollView(
            child: Container(
              height: screenHeight,
              width: screenWidth,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: Image.asset(
                  "assets/images/background.png",
                  fit: BoxFit.cover,
                ).image),
              ),
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 25,
                    left: 20,
                    right: 20,
                    child: Container(
                      width: screenWidth,
                      height: screenHeight * .83,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0, 1),
                            blurRadius: 1.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(25),
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          SizedBox(height: 30),
                          Image(
                              image: AssetImage(
                                'assets/images/logonew2.png',
                              ),
                              height: 100,
                              width: 210,
                              fit: BoxFit.cover),
                          SizedBox(height: 25),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Enter login details for ",
                                style: GoogleFonts.openSans(
                                  fontSize: 15.0,
                                ),
                              ),
                              Text(
                                "SocialNail ",
                                style: GoogleFonts.openSans(
                                  fontSize: 15.0,
                                  color: kAppColor,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Text(
                                "with us.",
                                style: GoogleFonts.openSans(
                                  fontSize: 15.0,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 30),
                          Container(
                            width: 150.0,
                            height: 120,
                            child: Image.asset(
                              "assets/images/login_icon.png",
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          SizedBox(height: 40),
                          Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15),
                              child: sectionEmailPwd()),
                          SizedBox(height: 30),
                          Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: forgotPwd(),
                              )),
                        ]),
                  )
                ],
              ),
            ),
          ),
          bottomSheet: ButtonBottomSheet(
            title1: "Login",
            onTapped1: () {
              if (!_globalKey.currentState.validate()) {
                return;
              } else {
                login(emailController.text.trim(),
                    passwordController.text.trim(), context);
              }
            },
            title2: "Register",
            onTapped2: () {
              emailController.clear();
              passwordController.clear();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SignUpScreen()));
            },
          ),
        ),
      ),
    );
  }

  //remaining on Click and screen
  forgotPwd() {
    return GestureDetector(
      onTap: () {
        debugPrint('forgot password clicked');
        emailController.clear();
        passwordController.clear();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
              builder: (BuildContext context) => VerifyMobileScreen(),
            ),
            (Route<dynamic> route) => false);
      },
      child: Text(
        "Forgot Password?",
        style: GoogleFonts.openSans(
            fontSize: 16.0, color: Colors.blue, fontWeight: FontWeight.w600),
      ),
    );
  }

  Widget signUpSection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "Don't have an account ? ",
          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0),
        ),
        GestureDetector(
          onTap: () {
            debugPrint('navigating to signUp screen');
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => SignUpScreen()));
          },
          child: Text(
            "Sign up here ",
            style: TextStyle(
                color: kAppColor, fontWeight: FontWeight.w500, fontSize: 16.0),
          ),
        ),
      ],
    );
  }

  Widget loginButton(BuildContext context) {
    return Container(
      width: 200,
      height: 50,
      child: RaisedButton(
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(60.0),
          ),
          color: kAppColor,
          textColor: Colors.white,
          child: Text(
            "Login",
            style: TextStyle(fontSize: 16),
          ),
          onPressed: () {
            if (!_globalKey.currentState.validate()) {
              return;
            } else {
              login(emailController.text.trim(), passwordController.text.trim(),
                  context);
            }
          }),
    );
  }

  login(String email, String password, BuildContext context) async {
    pr.show();
    const headers = {'Content-Type': 'application/json'};
    String url = "http://microecommerce.flicklead.com/api/UserDetail/UserLogin";

    Map<String, dynamic> data = {'UserName': email, 'Password': password};

    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    debugPrint(response.statusCode.toString());

    if (response.statusCode == 200) {
      pr.hide();
      //after the login REST api call && response code ==200
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      debugPrint(jsonResponse.toString());
      debugPrint(response.statusCode.toString());
      String data = jsonResponse.toString();
      if (data == "[]") {
        Fluttertoast.showToast(
            backgroundColor: kAppColor,
            textColor: Colors.white,
            msg: "Please check the credentials..Something went wrong.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 2);
        return;
      } else {
        Utils.setIsLoggedInFlag();
        SharedPreferences prefs = await SharedPreferences.getInstance();
        for (Map i in jsonResponse) {
          List<LoggedInUserModel> userDetails = [];
          userDetails.add(LoggedInUserModel.fromJson(i));

          userDetails.forEach((LoggedInUserModel loggedInUserModel) {
            userId = loggedInUserModel.id;
            userName = loggedInUserModel.name;
            userMobile = loggedInUserModel.mobile;
            userEmailId = loggedInUserModel.emailId;
          });
        }

        //saving userDetails in SPrefs...
        prefs.setString(
            'userId', userId.toString()); //converted Id(int) to string
        prefs.setString('userName', userName);
        prefs.setString('userMobile', userMobile);
        prefs.setString('userEmailId', userEmailId);

        //----------------------------//

        emailController.clear();
        passwordController.clear();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => Dashboard()),
            (Route<dynamic> route) => false);
      }
    } else {
      pr.hide();
      Fluttertoast.showToast(
          backgroundColor: kAppColor,
          textColor: Colors.white,
          msg: "Error while Login.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2);
      debugPrint(response.statusCode.toString());
      return;
    }
  }

  Widget sectionEmailPwd() {
    return Form(
      key: _globalKey,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        margin: EdgeInsets.only(top: 10),
        child: Column(
          children: <Widget>[
            emailSection("Email", Icons.email),
            SizedBox(height: 20.0),
            passwordSection("Password", Icons.lock),
          ],
        ),
      ),
    );
  }

  Widget emailSection(String title, IconData iconData) {
    return TextFormField(
      validator: (String value) {
        if (value.isEmpty) {
          return "Email is required";
        }
        return null;
      },
      onSaved: (String value) {},
      keyboardType: TextInputType.emailAddress,
      controller: emailController,
      style: TextStyle(color: Colors.black, fontSize: 18),
      decoration: InputDecoration(
        labelText: title,
        labelStyle: TextStyle(color: Colors.black),
        prefixIcon: Icon(iconData),
      ),
    );
  }

  Widget passwordSection(String title, IconData iconData) {
    return TextFormField(
      validator: (String value) {
        if (value.isEmpty) {
          return "Password is required";
        }
        return null;
      },
      onSaved: (String value) {},
      controller: passwordController,
      style: TextStyle(color: Colors.black, fontSize: 18),
      obscureText: !passwordVisible,
      decoration: InputDecoration(
        labelText: title,
        labelStyle: TextStyle(color: Colors.black),
        prefixIcon: Icon(iconData),
        suffixIcon: GestureDetector(
          onTap: () {
            setState(() {
              passwordVisible = !passwordVisible;
            });
          },
          child: new Icon(
              passwordVisible ? Icons.visibility_off : Icons.visibility),
        ),
      ),
    );
  }
}
