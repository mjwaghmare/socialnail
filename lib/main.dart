import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:micro_ecommerce/screens/OnboardingScreen.dart';
import 'package:micro_ecommerce/screens/dashboard.dart';
import 'package:micro_ecommerce/screens/login_screen.dart';
import 'package:micro_ecommerce/utils/utilities.dart';

int initScreen;
String email;
var newInitialRoute;

Future<void> main() async {
  //this line insures that all widgets are initialized or build.
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  final MyApp myApp = MyApp();
  bool visitingFlagCheck = await Utils.getVisitingFlag();
  bool isLoggedFlagCheck = await Utils.getIsLoggedInFlag();

  //checking conditions to open a particular screen
  if (visitingFlagCheck == false && isLoggedFlagCheck == false) {
    newInitialRoute = '/OnboardingScreen';
  } else if (visitingFlagCheck == true && isLoggedFlagCheck == false) {
    newInitialRoute = '/LoginScreen';
  } else if (visitingFlagCheck == true && isLoggedFlagCheck == true) {
    newInitialRoute = '/Dashboard';
  } else if (visitingFlagCheck == true && isLoggedFlagCheck == true) {
    newInitialRoute = '/LoginScreen';
  }
  runApp(myApp);
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          fontFamily: 'OkraRegular', //font for complete app...
          visualDensity: VisualDensity.adaptivePlatformDensity),
      title: 'SocialNail',
      initialRoute: newInitialRoute,
      routes: {
        '/OnboardingScreen': (context) => OnboardingScreen(),
        '/LoginScreen': (context) => LoginScreen(),
        '/Dashboard': (context) => Dashboard(),
      },
      debugShowCheckedModeBanner: false,
      supportedLocales: const <Locale>[
        Locale('en', ''),
        Locale('hi', ''),
        Locale('mr', ''),
      ],
    );
  }
}
