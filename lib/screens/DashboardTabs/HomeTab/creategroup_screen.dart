import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomSingleButton.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../dashboard.dart';

class CreateGroup extends StatefulWidget {
  @override
  _CreateGroupState createState() => _CreateGroupState();
}

bool createGrploading = false;
ProgressDialog pr;
TextEditingController groupNameController = new TextEditingController();
TextEditingController groupDescriptionController = new TextEditingController();
String grpOption,
    grpType,
    grpSubscription,
    grpFreeMembers,
    grpTypeOf,
    grpUserInfo,
    reqProfile,
    grpValidation;

class _CreateGroupState extends State<CreateGroup> {
  bool checkboxRequestValue = false;
  bool checkboxVerifyValue = false;

  var option = '';

  //Group Option
  var _groupOption = ['Group Option', 'Public', 'Private'];
  var _currentGroupOption = 'Group Option';

  //Group Type
  var _groupType = ['Group Type', 'Free', 'Premium'];
  var _currentGroupType = 'Group Type';

  //Group Subscription
  var _groupSubscription = ['Choose Group Subscription', 'Monthly', 'Yearly'];
  var _currentGroupSubscription = 'Choose Group Subscription';

  //Free Members
  var _groupFreeMembers = [
    'Select Free Members',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10'
  ];
  var _currentGroupFreeMembers = 'Select Free Members';

  //Request user info
  var _groupRequestUserInfo = ['Request User Info', 'Dob', 'email'];
  var _currentRequestUserInfo = 'Request User Info';

  //Type of group
  var _groupTypeOfGroup = ['Select type of group', 'Technology', 'Commerce'];
  var _currentTypeOfGroup = 'Select type of group';

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, isDismissible: false);
    pr.style(
      message: 'Loading contacts...',
      progressWidget: CircularProgressIndicator(),
      borderRadius: 8.0,
      backgroundColor: Colors.white,
      messageTextStyle: TextStyle(
          color: kAppColor, fontSize: 18.0, fontWeight: FontWeight.w500),
    );
    return Scaffold(
      bottomSheet: createGrploading == false
          ? CustomSingleButton(
              title: "Create Group",
              onTapped: createGroup,
            )
          : Material(
              elevation: 5,
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                height: Platform.isIOS ? 70 : 60,
                child: Center(
                  child: Container(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(
                      backgroundColor: kAppColor,
                    ),
                  ),
                ),
              ),
            ),
      appBar: AppBar(
        title: Text(
          "Group Creation",
          style: TextStyle(
              fontFamily: kOkraRegular, fontSize: 18, color: kAppTextColor),
        ),
        titleSpacing: 0.0,
        elevation: 5.0,
        leading: GestureDetector(
          onTap: () {
            //navigate user to create Home screen
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: kAppColor,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              child: Image.asset(
                "assets/images/background.png",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              margin: EdgeInsets.all(25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //Group Name
                  TextField(
                    controller: groupNameController,
                    style: TextStyle(
                        fontSize: 18,
                        fontFamily: kOkraMedium,
                        color: kAppTextColor),
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: kAppTextColor),
                      ),
                      labelText: "Group Name",
                      labelStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                  //Group Description
                  TextField(
                    controller: groupDescriptionController,
                    style: TextStyle(
                        fontSize: 18,
                        fontFamily: kOkraMedium,
                        color: kAppTextColor),
                    decoration: InputDecoration(
                      labelText: "Group Description",
                      labelStyle: TextStyle(color: Colors.grey),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: kAppTextColor),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  //Group Option Dropdown
                  Text(
                    "Group Option",
                    style:
                        TextStyle(color: Colors.grey, fontFamily: kOkraMedium),
                  ),
                  groupOptionDropDown(),
                  SizedBox(height: 20),
                  //Group Type Dropdown
                  Text(
                    "Group Type",
                    style:
                        TextStyle(color: Colors.grey, fontFamily: kOkraMedium),
                  ),
                  groupTypeDropDown(),
                  SizedBox(height: 20),
                  //Monthly Subscription Dropdown
                  Text(
                    "Group Subscription",
                    style:
                        TextStyle(color: Colors.grey, fontFamily: kOkraMedium),
                  ),
                  groupSubscriptionDropDown(),
                  //No. of member (Free) Dropdown
                  _currentGroupType == "Premium"
                      ? SizedBox(height: 20)
                      : Container(),
                  _currentGroupType == "Premium"
                      ? Text(
                          "Number of member join(Free)",
                          style: TextStyle(
                              color: Colors.grey, fontFamily: kOkraMedium),
                        )
                      : Container(),
                  _currentGroupType == "Premium"
                      ? groupFreeMemberDropDown()
                      : Container(),
                  SizedBox(height: 20),
                  //Type of Group Dropdown
                  Text(
                    "Group Type of",
                    style:
                        TextStyle(color: Colors.grey, fontFamily: kOkraMedium),
                  ),
                  groupTypeOfGroupDropDown(),
                  SizedBox(height: 20),
                  //Request User Info Dropdown
                  Text(
                    "Request user information",
                    style:
                        TextStyle(color: Colors.grey, fontFamily: kOkraMedium),
                  ),
                  groupRequestInfoDropDown(),
                  SizedBox(height: 20),
                  //Checkbox request user profile
                  checkboxRequest(),
                  //Checkbox verify group
                  checkboxVerify(),
                  SizedBox(height: 60),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> createGroup() async {
    setState(() {
      createGrploading = true;
    });
    String grpName = groupNameController.text;
    setState(() {
      grpOption = _currentGroupOption;
      grpType = _currentGroupType;
      grpSubscription = _currentGroupSubscription;
      grpFreeMembers = _currentGroupFreeMembers;
      grpTypeOf = _currentTypeOfGroup;
      grpUserInfo = _currentRequestUserInfo;
      reqProfile = checkboxRequestValue.toString();
      grpValidation = checkboxVerifyValue.toString();
    });

    if (grpName.isEmpty ||
        grpOption == "Group Option" ||
        grpType == "Group Type" ||
        grpSubscription == "Choose Group Subscription" ||
        grpFreeMembers == "Select Free Members" ||
        grpTypeOf == "Select type of group" ||
        grpUserInfo == "Request User Info") {
      setState(() {
        createGrploading = false;
      });
      Fluttertoast.showToast(
          backgroundColor: kAppColor,
          textColor: Colors.white,
          msg: "Please complete all fields",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2);
      return;
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      const headers = {'Content-Type': 'application/json'};
      String url =
          "http://microecommerce.flicklead.com/api/Groupdetails/CreateGroup";

      Map<String, dynamic> data = {
        "GroupName": groupNameController.text.toString(),
        "GroupDescription": groupDescriptionController.text.toString(),
        "GroupOption": grpOption,
        "GroupType": grpType,
        "GroupSubscription": grpSubscription,
        "NumberOfMembersJoinFree": grpFreeMembers,
        "GroupTypeOf": grpTypeOf,
        "RequestUserInformations": grpUserInfo,
        "RequestUserProfile": reqProfile,
        "VerifyMyGroupStatusFiled": grpValidation,
        "GroupStatus": "Active",
        "CreatedBy": prefs.getString("userId"),
      };

      debugPrint(data.toString());

      var response = await http.post(
        url,
        headers: headers,
        body: json.encode(data),
      );

      debugPrint(response.body.toString());

      if (response.statusCode == 200) {
        setState(() {
          createGrploading = false;
        });
        Fluttertoast.showToast(msg: "Group Created Successfully");
        groupNameController.clear();
        groupDescriptionController.clear();
        Navigator.of(context).pop(
            MaterialPageRoute(builder: (BuildContext context) => Dashboard()));
      } else {
        setState(() {
          createGrploading = false;
        });
        debugPrint(response.statusCode.toString());
      }
    }
  }

  //Request
  Widget checkboxRequest() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            checkColor: Colors.white,
            activeColor: kAppColor,
            value: checkboxRequestValue,
            onChanged: (bool value) {
              setState(() {
                checkboxRequestValue = value;
              });
            }),
        Text(
          "Request user profile",
          style: TextStyle(fontFamily: kOkraMedium, fontSize: 15),
        ),
      ],
    );
  }

  Widget checkboxVerify() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Checkbox(
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            checkColor: Colors.white,
            activeColor: kAppColor,
            value: checkboxVerifyValue,
            onChanged: (bool value) {
              setState(() {
                checkboxVerifyValue = value;
              });
            }),
        Text(
          "Verify my group status filed",
          style: TextStyle(fontFamily: kOkraMedium, fontSize: 15),
        ),
      ],
    );
  }

  //Group Option
  Widget groupOptionDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupOption.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropGroupOptionDownItemSelected(value);
        },
        value: _currentGroupOption,
      ),
    );
  }

  void _dropGroupOptionDownItemSelected(String value) {
    setState(() {
      this._currentGroupOption = value;
    });
  }

  //Group Type
  Widget groupTypeDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupType.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropGroupTypeDownItemSelected(value);
        },
        value: _currentGroupType,
      ),
    );
  }

  void _dropGroupTypeDownItemSelected(String value) {
    setState(() {
      this._currentGroupType = value;
    });
  }

  //Group Subscription
  Widget groupSubscriptionDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupSubscription.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropGroupSubscriptionDownItemSelected(value);
        },
        value: _currentGroupSubscription,
      ),
    );
  }

  void _dropGroupSubscriptionDownItemSelected(String value) {
    setState(() {
      this._currentGroupSubscription = value;
    });
  }

  //Group Free Member
  Widget groupFreeMemberDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupFreeMembers.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropGroupFreeMemberDownItemSelected(value);
        },
        value: _currentGroupFreeMembers,
      ),
    );
  }

  void _dropGroupFreeMemberDownItemSelected(String value) {
    setState(() {
      this._currentGroupFreeMembers = value;
    });
  }

  //Type of Group
  Widget groupTypeOfGroupDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupTypeOfGroup.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropDownTypeOfGroupItemSelected(value);
        },
        value: _currentTypeOfGroup,
      ),
    );
  }

  void _dropDownTypeOfGroupItemSelected(String value) {
    setState(() {
      this._currentTypeOfGroup = value;
    });
  }

  //Request userInfo
  Widget groupRequestInfoDropDown() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _groupRequestUserInfo.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery.of(context).size.width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _dropDownRequestInfoItemSelected(value);
        },
        value: _currentRequestUserInfo,
      ),
    );
  }

  void _dropDownRequestInfoItemSelected(String value) {
    setState(() {
      this._currentRequestUserInfo = value;
    });
  }
}
