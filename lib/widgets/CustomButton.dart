import 'package:flutter/material.dart';
import 'package:micro_ecommerce/utils/constants.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final Function onTapped;

  CustomButton({this.title, this.onTapped});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: onTapped,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [
                    Colors.pink[300],
                    Colors.pink[400],
                    Colors.pink[400],
                    Colors.pink[500],
                    Colors.pink[600]
                  ])),
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                  fontFamily: kOkraRegular,
                  fontSize: 14.0,
                  color: Colors.white,
                  letterSpacing: 1.0,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
      ),
    );
  }
}
