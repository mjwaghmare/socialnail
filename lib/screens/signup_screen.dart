import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:micro_ecommerce/screens/dashboard.dart';
import 'package:micro_ecommerce/screens/login_screen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/utils/utilities.dart';
import 'package:micro_ecommerce/widgets/ButtonBottomSheet.dart';
import 'package:micro_ecommerce/widgets/UploadImageWidget.dart';
import 'package:package_info/package_info.dart';
import 'package:progress_dialog/progress_dialog.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController mobileController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();
  TextEditingController photoIdController = new TextEditingController();
  TextEditingController paController = new TextEditingController();
  final getEmailOtpController = new TextEditingController();
  final getMobileOtpController = new TextEditingController();
  bool checkBoxTCValue = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  ProgressDialog pr;
  String selectedFileName;
  String _name,
      _mobile,
      _mobileOtp,
      _email,
      _emailOtp,
      _username,
      _password,
      _confirmPassword,
      _newUsername,
      appName,
      packageName,
      version,
      buildNumber,
      base64Image,
      fileName;

  var androidInfo,
      release,
      sdkInt,
      manufacturer,
      model,
      iosInfo,
      name,
      systemName;
  bool passwordVisible = true;
  bool confirmPasswordVisible = true;
  String errorMessage = '';
  String _newVerId;
  File _image;
  String _imageName = '';
  FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseAuthException authException;
  AuthCredential credential;

  @override
  Future<void> initState() {
    super.initState();
    initPlatformState();
    initAppInfo();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, isDismissible: false);
    pr.style(
      message: 'Please wait...',
      progressWidget: CircularProgressIndicator(),
      borderRadius: 8.0,
      backgroundColor: Colors.white,
      messageTextStyle: TextStyle(
          color: kAppColor, fontSize: 18.0, fontWeight: FontWeight.w500),
    );

    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          resizeToAvoidBottomPadding: true,
          bottomSheet: ButtonBottomSheet(
            title1: "Next",
            onTapped1: () {
              debugPrint("next btn");
              if (!_formKey.currentState.validate()) {
                return;
              } else if (checkBoxTCValue.toString() == "false") {
                Fluttertoast.showToast(
                    msg: "Please accept the terms and conditions.",
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIosWeb: 2);
                return;
              } else {
                if (_newVerId != null) {
                  verifyOtp(getMobileOtpController.text.trim(),
                      getEmailOtpController.text.trim(), _newVerId);
                  debugPrint(getEmailOtpController.text.trim());
                } else {
                  Fluttertoast.showToast(msg: "Try Again..");
                }
              }
            },
            title2: "Login",
            onTapped2: () {
              nameController.clear();
              mobileController.clear();
              emailController.clear();
              usernameController.clear();
              passwordController.clear();
              confirmPasswordController.clear();
              checkBoxTCValue = false;
              Navigator.pop(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            },
          ),
          body: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Container(
                margin: EdgeInsets.only(bottom: 80),
                height: MediaQuery.of(context).size.height * 1.4,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: Image.asset(
                    "assets/images/background.png",
                    fit: BoxFit.cover,
                  ).image),
                ),
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 30,
                      left: 20,
                      right: 20,
                      bottom: 1,
                      child: Container(
                        padding: EdgeInsets.only(bottom: 20),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.grey,
                              offset: Offset(0, 1),
                              blurRadius: 1.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      top: 40,
                      left: 0,
                      right: 0,
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 45),
                        alignment: Alignment.center,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Image(
                                image: AssetImage(
                                  'assets/images/logonew2.png',
                                ),
                                height: 150,
                                width: 210,
                                fit: BoxFit.cover),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text(
                                  "Enter register details for ",
                                  style: GoogleFonts.openSans(
                                    fontSize: 14.0,
                                  ),
                                ),
                                Text(
                                  "SocialNail ",
                                  style: GoogleFonts.openSans(
                                    fontSize: 14.0,
                                    color: kAppColor,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                Text(
                                  "with us.",
                                  style: GoogleFonts.openSans(
                                    fontSize: 14.0,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 15),
                            nameTextField("Name", Icons.person_outline),
                            SizedBox(height: 8),
                            mobileTextField(
                                "Mobile Number", Icons.phone_android),
                            SizedBox(height: 8),
                            mobileOtp("Enter Mobile Otp", Icons.lock_outline),
                            SizedBox(height: 8),
                            emailTextField("Email", Icons.email),
                            SizedBox(height: 8),
                            emailOtp("Enter Email Otp", Icons.lock_outline),
                            SizedBox(height: 8),
                            usernameTextField("Username", Icons.person),
                            SizedBox(height: 8),
                            passwordSection("Password", Icons.lock),
                            SizedBox(height: 10),
                            confirmPasswordSection(
                                "Confirm Password", Icons.lock),
                            SizedBox(height: 10),
                            UploadDocumentWidget(
                              title: _imageName == ""
                                  ? "Upload Profile"
                                  : _imageName,
                              onTap: getImage,
                              nameOfController: photoIdController,
                              fileName: selectedFileName,
                              sourceName: "photoId",
                            ),
                            SizedBox(height: 10),
                            checkBoxTC(),
                            SizedBox(height: 10),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget nameTextField(String name, IconData nameIconData) {
    return TextFormField(
      validator: (String value) {
        if (value.isEmpty) {
          return "Name is required";
        }
        return null;
      },
      onSaved: (String value) {
        _name = value;
      },
      controller: nameController,
      style: TextStyle(
          fontFamily: kOkraRegular,
          color: Colors.black,
          fontWeight: FontWeight.w400,
          fontSize: 16),
      decoration: InputDecoration(
        labelText: name,
        prefixIcon: Icon(nameIconData),
      ),
    );
  }

  Widget mobileTextField(String mobile, IconData mobileIconData) {
    return TextFormField(
      validator: (String value) {
        if (value.isEmpty) {
          return "Mobile number is required";
        }
        return null;
      },
      onSaved: (String value) {
        _mobile = value;
      },
      keyboardType: TextInputType.phone,
      maxLength: 10,
      controller: mobileController,
      style: TextStyle(
          fontFamily: kOkraRegular,
          color: Colors.black,
          fontWeight: FontWeight.w400,
          fontSize: 16),
      decoration: InputDecoration(
        labelText: mobile,
        prefixIcon: Icon(mobileIconData),
      ),
    );
  }

  Widget mobileOtp(String mobile, IconData mobileIconData) {
    return TextFormField(
      onTap: () {
        var mob = mobileController.text;
        if (mob != "") {
          checkExistingUser(context);
        } else {
          Fluttertoast.showToast(
              msg: "Enter Mobile Number", gravity: ToastGravity.CENTER);
        }
      },
      validator: (String value) {
        if (value.isEmpty) {
          return "Mobile Otp is required";
        }
        return null;
      },
      onSaved: (String value) {
        _mobileOtp = value;
      },
      keyboardType: TextInputType.number,
      maxLength: 6,
      controller: getMobileOtpController,
      style: TextStyle(
          fontFamily: kOkraRegular,
          color: Colors.black,
          fontWeight: FontWeight.w400,
          fontSize: 16),
      decoration: InputDecoration(
        labelText: mobile,
        prefixIcon: Icon(mobileIconData),
      ),
    );
  }

  Widget emailTextField(String email, IconData emailIconData) {
    return TextFormField(
      validator: (String value) {
        if (!RegExp(
                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
            .hasMatch(value)) {
          return "Please enter a valid email address";
        }
        return null;
      },
      onSaved: (String value) {
        _name = value;
      },
      keyboardType: TextInputType.emailAddress,
      controller: emailController,
      style: TextStyle(
          fontFamily: kOkraRegular,
          color: Colors.black,
          fontWeight: FontWeight.w400,
          fontSize: 16),
      decoration: InputDecoration(
        labelText: email,
        prefixIcon: Icon(emailIconData),
      ),
    );
  }

  Widget emailOtp(String mobile, IconData mobileIconData) {
    return TextFormField(
      onTap: () {
        if (emailController.text.trim().length != 0 &&
            RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                .hasMatch(emailController.text.trim())) {
          pr.show();
          emailOtpMethod();
        } else {
          Fluttertoast.showToast(
              msg: "Enter email Id", backgroundColor: kAppColor);
        }
      },
      validator: (String value) {
        if (value.isEmpty) {
          return "Email Otp is required";
        }
        return null;
      },
      onSaved: (String value) {
        _emailOtp = value;
      },
      keyboardType: TextInputType.emailAddress,
//      maxLength: 6,
      controller: getEmailOtpController,
      style: TextStyle(
          fontFamily: kOkraRegular,
          color: Colors.black,
          fontWeight: FontWeight.w400,
          fontSize: 16),
      decoration: InputDecoration(
        labelText: mobile,
        prefixIcon: Icon(mobileIconData),
      ),
    );
  }

  Widget usernameTextField(String username, IconData usernameIconData) {
    return TextFormField(
      onTap: () {
        //setting the userName got from emailField..
        if (emailController.text.trim().length != 0 &&
            RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                .hasMatch(emailController.text.trim())) {
          _newUsername = Utils.generateUsername(emailController.text.trim());
          usernameController.text = _newUsername;
        } else {
          usernameController.text = "";
        }
      },
      validator: (String value) {
        if (value.isEmpty) {
          return "Username is required";
        }
        return null;
      },
      onSaved: (String value) {
        _username = value;
      },
      controller: usernameController,
      style: TextStyle(
          fontFamily: kOkraRegular,
          color: Colors.black,
          fontWeight: FontWeight.w400,
          fontSize: 16),
      decoration: InputDecoration(
        labelText: username,
        prefixIcon: Icon(usernameIconData),
      ),
    );
  }

  Widget passwordSection(String title, IconData iconData) {
    return TextFormField(
      validator: (String value) {
        if (value.isEmpty) {
          return "Password is required";
        }
        return null;
      },
      onSaved: (String value) {
        _password = value;
      },
      controller: passwordController,
      style: TextStyle(
          fontFamily: kOkraRegular,
          color: Colors.black,
          fontWeight: FontWeight.w400,
          fontSize: 16),
      obscureText: passwordVisible,
      decoration: InputDecoration(
        labelText: title,
        prefixIcon: Icon(iconData),
        suffixIcon: GestureDetector(
          onTap: () {
            setState(() {
              passwordVisible = !passwordVisible;
            });
          },
          child: new Icon(
              passwordVisible ? Icons.visibility : Icons.visibility_off),
        ),
      ),
    );
  }

  Widget confirmPasswordSection(String title, IconData iconData) {
    return TextFormField(
      validator: (String val) {
        if (val.isEmpty) return 'Confirm Password required';
        if (val != passwordController.text.trim()) return 'Not Match';
        return null;
      },
      onSaved: (String value) {
        _confirmPassword = value;
      },
      controller: confirmPasswordController,
      style: TextStyle(
          fontFamily: kOkraRegular,
          color: Colors.black,
          fontWeight: FontWeight.w400,
          fontSize: 16),
      obscureText: confirmPasswordVisible,
      decoration: InputDecoration(
        labelText: title,
        prefixIcon: Icon(iconData),
        suffixIcon: GestureDetector(
          onTap: () {
            setState(() {
              confirmPasswordVisible = !confirmPasswordVisible;
            });
          },
          child: new Icon(
              confirmPasswordVisible ? Icons.visibility : Icons.visibility_off),
        ),
      ),
    );
  }

  Widget checkBoxTC() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Checkbox(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                checkColor: Colors.white,
                activeColor: kAppColor,
                value: checkBoxTCValue,
                onChanged: (bool value) {
                  setState(() {
                    checkBoxTCValue = value;
                  });
                }),
            Text(
              "I agree with the ",
              style: TextStyle(
                  fontFamily: kOkraRegular,
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0),
            ),
            GestureDetector(
              child: Text(
                "Terms and Conditions",
                style: TextStyle(
                    color: kAppColor,
                    fontFamily: kOkraRegular,
                    fontWeight: FontWeight.w500,
                    fontSize: 14.0),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 47.0),
              child: Text(
                "and the ",
                style: TextStyle(
                    fontFamily: kOkraRegular,
                    fontWeight: FontWeight.w500,
                    fontSize: 14.0),
              ),
            ),
            GestureDetector(
              child: Text(
                "Privacy Policy",
                style: TextStyle(
                    color: kAppColor,
                    fontFamily: kOkraRegular,
                    fontWeight: FontWeight.w500,
                    fontSize: 14.0),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Future<void> checkExistingUser(BuildContext context) async {
    pr.show();
    String checkUser =
        "https://microecommerce.flicklead.com/api/UserDetail/CheckForduplicateUser";
    const headers = {'Content-Type': 'application/json'};

    Map<String, dynamic> data = {'MobileNumber': mobileController.text.trim()};

    var response = await http.post(
      checkUser,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      debugPrint(jsonResponse.toString());
      String data = jsonResponse.toString();

      //user is not registered so registered the user and  send the otp to mail and mobile...
      if (data == "[]") {
        pr.hide();
        _formKey.currentState.save();
        mobileOtpMethod();
      }
      //user is already registered...
      else {
        pr.hide();
        Fluttertoast.showToast(
            msg: "Mobile number already registered.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 2);
        return;
      }
    }
  }

  Future<void> mobileOtpMethod() async {
    //Param for phoneOtp...
    final number = mobileController.text.trim();

    //mobile Otp

    String numberWithCountryCode = "+91" + number;

    await _auth.verifyPhoneNumber(
        phoneNumber: numberWithCountryCode,
        timeout: Duration(seconds: 120), //two minutes
        verificationCompleted: null,
        verificationFailed: (exception) {
          Fluttertoast.showToast(
              msg: "Something went wrong...Please try again",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 2);
          Text('please check the number');
          debugPrint(exception.toString());
        },
        codeSent: (String verId, [int forceResendingToken]) {
          _newVerId = verId;
          pr.hide();
          Fluttertoast.showToast(
              msg: "Otp sent to Registered mobile",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 2);
        },
        codeAutoRetrievalTimeout: null);
  }

  Future<void> emailOtpMethod() async {
    final email = emailController.text.trim();

    String sendOtp =
        "http://microecommerce.flicklead.com/api/EmailOTP/SaveEmailOTP";
    const headers = {'Content-Type': 'application/json'};

    Map<String, dynamic> data = {'EmailId': email};

    var response = await http.post(
      sendOtp,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      pr.hide();
      var jsonResponse;
      jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      debugPrint(jsonResponse.toString());
      Fluttertoast.showToast(
          msg: "Otp sent to Email Id...",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2);
    } else {
      pr.hide();
      Fluttertoast.showToast(
          msg: "Something went wrong...",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2);
    }
  }

  handleError(error) {
    getEmailOtpController.clear;
    getMobileOtpController.clear();
    Fluttertoast.showToast(
        msg: "Something went wrong while verification.",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 2);

    debugPrint(error);
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    String base64Image = base64Encode(
      image.readAsBytesSync(),
    );
    String fileName = image.path.split("/").last;
    debugPrint(fileName);
    debugPrint(base64Image);

    setState(
      () {
        _imageName = fileName;
      },
    );
  }

  Future<void> initPlatformState() async {
    if (Platform.isAndroid) {
      DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
      androidInfo = await deviceInfoPlugin.androidInfo;
      release = androidInfo.version.release;
      sdkInt = androidInfo.version.sdkInt;
      manufacturer = androidInfo.manufacturer;
      model = androidInfo.model;
      debugPrint("---------------Android-------------------");
      debugPrint('Android $release (SDK $sdkInt), $manufacturer $model');
      // Android 9 (SDK 28), Xiaomi Redmi Note 7
    }

    if (Platform.isIOS) {
      iosInfo = await DeviceInfoPlugin().iosInfo;
      systemName = iosInfo.systemName;
      version = iosInfo.systemVersion;
      name = iosInfo.name;
      model = iosInfo.model;
      debugPrint('$systemName $version, $name $model');
      // iOS 13.1, iPhone 11 Pro Max iPhone
    }
  }

  Future<void> initAppInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    version = packageInfo.version;
    debugPrint("AppVersion - " + version);
  }

  Future<void> verifyOtp(
      String mobileOtp, String emailOtp, String verId) async {
    try {
      if (mobileOtp.isNotEmpty && emailOtp.isNotEmpty) {
        credential = PhoneAuthProvider.credential(
            verificationId: verId, smsCode: mobileOtp);
        UserCredential result =
            await FirebaseAuth.instance.signInWithCredential(credential);
        //getting user from firebase result
        User user =
            result.user; // if user not null means entered otp is correct

        //getting response to check the email otp if correct or not
        String checkUser =
            "http://microecommerce.flicklead.com/api/EmailOTP/EmailOTPDetails";
        const headers = {'Content-Type': 'application/json'};

        Map<String, dynamic> data = {'EmailId': emailController.text.trim()};

        var response = await http.post(
          checkUser,
          headers: headers,
          body: json.encode(data),
        );
        var jsonResponse;
        debugPrint("body" + response.statusCode.toString());
        if (response.statusCode == 200) {
          pr.hide();
          jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
          debugPrint(jsonResponse);

          String otp = jsonResponse["otp"];
          debugPrint("OTP is - " + otp.toString());

          //then check if both otp are correct or not
          if (user != null && emailOtp == otp) {
            signUpUser(); // call this method only when both otp are correct
          } else {
            pr.hide();
            Fluttertoast.showToast(
                msg: "Please check the Otp...",
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 2);
          }
        }
      } else {
        Fluttertoast.showToast(
            msg: "Enter both Otp",
            gravity: ToastGravity.BOTTOM,
            backgroundColor: kAppColor,
            textColor: Colors.white);
      }
    } catch (e) {
      pr.hide();
      handleError(e);
    }
  }

  Future<void> signUpUser() async {
    pr.show();
    //New params ...
    var baseCode = base64Image;
    var profilePicName = fileName;
    var appVersion = version; //1.0.0
    var deviceName = '$manufacturer $model'; //Realme - RMX1831
    var deviceOs = 'Android $release'; //Android 9

    const headers = {'Content-Type': 'application/json'};

    String url =
        "https://microecommerce.flicklead.com/api/UserDetail/UserRegistration";

    Map<String, dynamic> data = {
      'Name': nameController.text,
      'MobileNumber': mobileController.text,
      'EmailId': emailController.text,
      'UserName': usernameController.text,
      'Password': passwordController.text,
      'TermAndCondition': checkBoxTCValue,
      "ApkVersionNo": appVersion,
      "DeviceName": deviceName,
      "DeviceOs": deviceOs,
      "PhotoIdfileName": profilePicName,
      "PhotoId": baseCode
    };

    var jsonResponse;

    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    debugPrint(response.statusCode.toString());

    if (response.statusCode == 200) {
      Utils.setName(nameController.text);
      pr.hide();
      jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      debugPrint(response.statusCode.toString());
      debugPrint(jsonResponse.toString());

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => Dashboard()),
          (Route<dynamic> route) => false);
    } else {
      pr.hide();
      debugPrint(response.body);
      Fluttertoast.showToast(
          backgroundColor: kAppColor,
          textColor: Colors.white,
          msg: "Unable to sign in...",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2);
      return;
    }
  }
}
