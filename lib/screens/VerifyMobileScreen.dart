import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/screens/dashboard.dart';
import 'package:micro_ecommerce/screens/login_screen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/ButtonBottomSheet.dart';

class VerifyMobileScreen extends StatefulWidget {
  @override
  _VerifyMobileScreenState createState() => _VerifyMobileScreenState();
}

class _VerifyMobileScreenState extends State<VerifyMobileScreen> {
  final otpController = new TextEditingController();
  final getOtpController = new TextEditingController();
  FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseAuthException authException;
  AuthCredential credential;

  //method to send Otp to user
  Future<bool> sendOtp(String phone, BuildContext context) async {
    FirebaseAuth _auth = FirebaseAuth.instance;
    String numberWithCountryCode = "+91" + phone;

    await _auth.verifyPhoneNumber(
        phoneNumber: numberWithCountryCode,
        timeout: Duration(seconds: 60),
        verificationCompleted: null,
        verificationFailed: (exception) {
          Fluttertoast.showToast(
              msg: "Something went wrong...",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 2);
          Text('please check the number');
          debugPrint(exception.toString());
        },
        codeSent: (String verId, [int forceResendingToken]) {
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) {
                return AlertDialog(
                  title: Text('Enter the Otp'),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Center(
                          child: TextField(
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            controller: getOtpController,
                          ),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    RaisedButton(
                      color: kAppColor,
                      onPressed: () async {
                        final otp = getOtpController.text.trim().toString();
                        credential = PhoneAuthProvider.credential(
                            verificationId: verId, smsCode: otp);
                        UserCredential result =
                            await _auth.signInWithCredential(credential);

                        User user = result.user;

                        if (user != null) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Dashboard(),
                            ),
                          );
                        } else {
                          Fluttertoast.showToast(
                              msg: "Something went wrong..Please check the OTP",
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 2);
                          debugPrint('Somenting went wrong');
                        }
                      },
                      child: Center(
                        child: Text(
                          'Confirm',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                );
              });
        },
        codeAutoRetrievalTimeout: null);
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        bottomSheet: ButtonBottomSheet(
            title1: "Get Otp",
            onTapped1: () {
              getOtpMethod();
            },
            title2: "Login",
            onTapped2: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (BuildContext context) => LoginScreen(),
                  ),
                  (Route<dynamic> route) => false);
            }),
        body: Container(
          height: screenHeight,
          width: screenWidth,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: Image.asset(
              "assets/images/background.png",
              fit: BoxFit.cover,
            ).image),
          ),
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 20,
                    left: 20,
                    right: 20,
                    child: Container(
                      width: screenWidth,
                      height: screenHeight * .80,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0, 1),
                            blurRadius: 1.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 45.0,
                    left: 50.0,
                    right: 50.0,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image(
                          image: AssetImage(
                            'assets/images/logonew2.png',
                          ),
                          height: 100,
                          width: 210,
                          fit: BoxFit.cover),
                    ),
                  ),
                  Positioned(
                    top: 150.0,
                    left: 50.0,
                    right: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Text(
                          "Enter login details for ",
                          style: GoogleFonts.openSans(
                            fontSize: 15.0,
                          ),
                        ),
                        Text(
                          "SocialNail ",
                          style: GoogleFonts.openSans(
                            fontSize: 15.0,
                            color: kAppColor,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          "with us.",
                          style: GoogleFonts.openSans(
                            fontSize: 15.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 200.0,
                    left: 80.0,
                    right: 50.0,
                    child: Container(
                      child: Image.asset(
                        "assets/images/otp_icon.png",
                      ),
                    ),
                  ),
                  Positioned(
                    top: 400.0,
                    left: 50.0,
                    right: 50.0,
                    child: sectionEnterOtp(
                        "Registered Number", Icons.phonelink_lock),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Row signInSection() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "Already have an account ? ",
          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0),
        ),
        GestureDetector(
          onTap: () {
            debugPrint('navigating to signUp screen');
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => LoginScreen()));
          },
          child: Text(
            "Sign in here ",
            style: TextStyle(
                color: Colors.deepOrangeAccent,
                fontWeight: FontWeight.w500,
                fontSize: 16.0),
          ),
        ),
      ],
    );
  }

  sectionEnterOtp(String title, IconData iconData) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 40.0),
      child: TextFormField(
        maxLength: 10,
        keyboardType: TextInputType.phone,
        controller: otpController,
        style: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w400, fontSize: 18),
        decoration: InputDecoration(
          labelText: title,
          prefixText: "+91",
          prefixIcon: Icon(iconData),
        ),
      ),
    );
  }

  Future<void> getOtpMethod() async {
    //first check if the user is registered or not using Api here,
    //if already registered then send otp else not
    String chechUser =
        "https://microecommerce.flicklead.com/api/UserDetail/CheckForduplicateUser";
    const headers = {'Content-Type': 'application/json'};
    Map<String, dynamic> data = {'MobileNumber': otpController.text.trim()};

    var response = await http.post(
      chechUser,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      debugPrint(jsonResponse.toString());
      String data = jsonResponse.toString();

      //not the registered User/Mobile
      if (data == "[]") {
        Fluttertoast.showToast(
            textColor: Colors.white,
            backgroundColor: kAppColor,
            msg: "Please enter registered mobile number.",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 2);
        return;
      }
      //Registered User/Mobile
      else {
        debugPrint(response.statusCode.toString());
        debugPrint(jsonResponse.toString());
        //after the login REST api call && response code ==200
        final number = otpController.text.trim();
        sendOtp(number, context);
      }
    }
  }
}
