import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/models/UserModel.dart';
import 'package:micro_ecommerce/models/group_model.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/EditGroupScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';

import '../../../../dashboard.dart';

class GroupDetailsScreen extends StatefulWidget {
  final String grpId;

  GroupDetailsScreen({Key key, this.grpId}) : super();

  @override
  _GroupDetailsScreenState createState() => _GroupDetailsScreenState();
}

class _GroupDetailsScreenState extends State<GroupDetailsScreen> {
  List<GroupModel> groupListData = [];

  //Group Details
  String groupId,
      grpImage,
      grpName,
      grpDesc,
      grpRegNo,
      grpOption,
      grpType,
      grpTypeOf,
      grpFreeMembers,
      grpReqUserInfo,
//      grpMembers,
      grpSubsDetails;
  bool loading = false;
  var newIds;
  List<UserModel> newMembersData = [];

  @override
  void initState() {
    _getGroupDetails(widget.grpId);
    super.initState();
  }

  Future<void> _getGroupDetails(String grpId) async {
    setState(() {
      loading = true;
    });
    debugPrint(grpId);
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/Groupdetails/GroupDetailsbyId";

    //send the userId of admin
    Map<String, dynamic> data = {
      "Id": grpId,
    };

    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    debugPrint("GROUPDETAILS" + response.statusCode.toString());

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body.replaceAll("ï»¿", ""));
      debugPrint(data.toString());
      setState(() {
        for (Map i in data) {
          groupListData.add(GroupModel.fromJson(i));
        }
        _getDetailsFromApi();
        loading = false;
      });
    } else {
      Fluttertoast.showToast(msg: "Error while fetching details");
      debugPrint(response.statusCode.toString());
    }
  }

  void _getDetailsFromApi() {
    String grpMembers = "";
    for (var value in groupListData) {
      groupId = value.id.toString();
      grpName = value.groupName;
      grpImage = value.avatarUrl;
      grpDesc = value.groupDescription;
      grpRegNo = value.groupRegistrationNumber;
      grpOption = value.groupOption;
      grpType = value.groupType;
      grpTypeOf = value.groupTypeOf;
      grpFreeMembers = value.numberOfMembersJoinFree;
      grpReqUserInfo = value.requestUserInformations;
      grpSubsDetails = value.subscriptionsDetails;
      grpMembers = value.groupMember;
    }

    if (grpMembers == null || grpMembers == "") {
      return;
    } else {
      newIds = json.decode(grpMembers);
      _listOfMembers(newIds);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        elevation: 5,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.chevron_left,
            size: 35,
            color: kAppColor,
          ),
        ),
        title: Text(
          "Group Details",
          style: TextStyle(
              fontSize: 18, color: kAppTextColor, fontFamily: kOkraSemiBold),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => EditGroupScreen(
                    grpId: widget.grpId,
                  ),
                ),
              );
            },
            icon: Icon(
              Icons.edit,
              size: 20,
              color: kAppColor,
            ),
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            "assets/images/background.png",
            fit: BoxFit.fill,
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: loading
                  ? Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: Center(
                        child: CircularProgressIndicator(
                          backgroundColor: kAppColor,
                        ),
                      ),
                    )
                  : SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Card(
                              elevation: 5,
                              color: Colors.white,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(4.0),
                                child: grpImage == null
                                    ? Image.network(
                                        "https://images.pexels.com/photos/807598/pexels-photo-807598.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
                                        fit: BoxFit.cover,
                                        height: 150.0,
                                        width:
                                            MediaQuery.of(context).size.width,
                                      )
                                    : Image.network(
                                        grpImage,
                                        fit: BoxFit.cover,
                                        height: 150.0,
                                        width:
                                            MediaQuery.of(context).size.width,
                                      ),
                              ),
                            ),
                            //Group Name
                            SizedBox(height: 20),
                            Text(
                              "Group Name",
                              style: TextStyle(
                                  fontFamily: kOkraMedium, fontSize: 17),
                            ),
                            Text(
                              grpName,
                              style: TextStyle(
                                  fontFamily: kOkraMedium,
                                  fontSize: 15,
                                  color: Colors.grey),
                            ),
                            //Group Description
                            SizedBox(height: 15),
                            Text(
                              "Group Description",
                              style: TextStyle(
                                  fontFamily: kOkraMedium, fontSize: 17),
                            ),
                            Text(
                              grpDesc,
                              style: TextStyle(
                                  fontFamily: kOkraMedium,
                                  fontSize: 15,
                                  color: Colors.grey),
                            ),
                            //Group Description
                            SizedBox(height: 15),
                            Text(
                              "Group Registration Number",
                              style: TextStyle(
                                  fontFamily: kOkraMedium, fontSize: 17),
                            ),
                            Text(
                              grpRegNo,
                              style: TextStyle(
                                  fontFamily: kOkraMedium,
                                  fontSize: 15,
                                  color: Colors.grey),
                            ),
                            //Group Media Section
                            SizedBox(height: 15),
                            Card(
                              color: Colors.white,
                              child: Container(
                                color: Colors.white,
                                padding: EdgeInsets.all(8),
                                height: 40,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "Media, Links, Docs & "
                                      "Folder",
                                      style: TextStyle(
                                          fontFamily: kOkraSemiBold,
                                          fontSize: 15),
                                    ),
                                    Icon(
                                      Icons.chevron_right,
                                      color: kAppColor,
                                      size: 30,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            //Approval Request Section
                            SizedBox(height: 15),
                            Card(
                              color: Colors.white,
                              child: Container(
                                color: Colors.white,
                                padding: EdgeInsets.all(8),
                                height: 40,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "Media, Links, Docs & "
                                      "Folder",
                                      style: TextStyle(
                                          fontFamily: kOkraSemiBold,
                                          fontSize: 15),
                                    ),
                                    Icon(
                                      Icons.chevron_right,
                                      color: kAppColor,
                                      size: 30,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            //Orders Section
                            SizedBox(height: 15),
                            Card(
                              color: Colors.white,
                              child: Container(
                                color: Colors.white,
                                padding: EdgeInsets.all(8),
                                height: 40,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "Orders",
                                      style: TextStyle(
                                          fontFamily: kOkraSemiBold,
                                          fontSize: 15),
                                    ),
                                    Icon(
                                      Icons.chevron_right,
                                      color: kAppColor,
                                      size: 30,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            //Group Option
                            SizedBox(height: 15),
                            Text(
                              "Group Option",
                              style: TextStyle(
                                  fontFamily: kOkraMedium, fontSize: 17),
                            ),
                            Text(
                              grpOption,
                              style: TextStyle(
                                  fontFamily: kOkraMedium,
                                  fontSize: 15,
                                  color: Colors.grey),
                            ),
                            //Group Type
                            SizedBox(height: 15),
                            Text(
                              "Group Type",
                              style: TextStyle(
                                  fontFamily: kOkraMedium, fontSize: 17),
                            ),
                            Text(
                              grpTypeOf,
                              style: TextStyle(
                                  fontFamily: kOkraMedium,
                                  fontSize: 15,
                                  color: Colors.grey),
                            ),
                            //Free Group Members
                            SizedBox(height: 15),
                            Text(
                              "Number of members join(Free)",
                              style: TextStyle(
                                  fontFamily: kOkraMedium, fontSize: 17),
                            ),
                            Text(
                              grpFreeMembers,
                              style: TextStyle(
                                  fontFamily: kOkraMedium,
                                  fontSize: 15,
                                  color: Colors.grey),
                            ),
                            //Request User Info
                            SizedBox(height: 15),
                            Text(
                              "Request user informations",
                              style: TextStyle(
                                  fontFamily: kOkraMedium, fontSize: 17),
                            ),
                            Text(
                              grpReqUserInfo,
                              style: TextStyle(
                                  fontFamily: kOkraMedium,
                                  fontSize: 15,
                                  color: Colors.grey),
                            ),
                            //Group Subscription Details
                            SizedBox(height: 15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Subscriptions Details",
                                      style: TextStyle(
                                          fontFamily: kOkraMedium,
                                          fontSize: 17),
                                    ),
                                    Text(
                                      grpSubsDetails == null
                                          ? "NA"
                                          : grpSubsDetails,
                                      style: TextStyle(
                                          fontFamily: kOkraMedium,
                                          fontSize: 15,
                                          color: Colors.grey),
                                    ),
                                  ],
                                ),
                                Container(
                                  width: 85,
                                  height: 25,
                                  color: Colors.white,
                                  child: FlatButton(
                                    onPressed: () {},
                                    child: Text('Renew',
                                        style: TextStyle(
                                            color: Colors.lightGreen,
                                            fontFamily: kOkraRegular,
                                            fontWeight: FontWeight.w600)),
                                    textColor: Colors.lightGreen,
                                    shape: RoundedRectangleBorder(
                                        side: BorderSide(
                                            color: Colors.lightGreen,
                                            width: 1,
                                            style: BorderStyle.solid),
                                        borderRadius: BorderRadius.circular(5)),
                                  ),
                                )
                              ],
                            ),
                            //Participants
                            SizedBox(height: 15),
                            listOfUsers(newMembersData),
                            //Group Actions
                            SizedBox(height: 15),
                            Text(
                              "Group Actions",
                              style: TextStyle(
                                  fontFamily: kOkraMedium, fontSize: 17),
                            ),
                            SizedBox(height: 10),
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      deleteGroup(widget.grpId);
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.exit_to_app,
                                            color: kAppColor,
                                          ),
                                          SizedBox(width: 5),
                                          Text(
                                            "Delete Group",
                                            style: TextStyle(
                                                fontFamily: kOkraSemiBold),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Container(
                                    width: 10,
                                    color: Colors.red,
                                    child: VerticalDivider(
                                      color: Colors.black,
                                      thickness: 5,
                                      width: 5,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  GestureDetector(
                                    onTap: () {
                                      Fluttertoast.showToast(
                                          msg: "Report Group",
                                          gravity: ToastGravity.CENTER);
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.thumb_down,
                                            color: kAppColor,
                                          ),
                                          SizedBox(width: 5),
                                          Text(
                                            "Report Group",
                                            style: TextStyle(
                                                fontFamily: kOkraSemiBold),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height: 50),
                          ],
                        ),
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> deleteGroup(String id) async {
    const headers = {'Content-Type': 'application/json'};
    String url =
        "https://microecommerce.flicklead.com/api/Groupdetails/DeleteGroup";

    Map<String, dynamic> data = {"Id": id};

    final response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "Group deleted successfully...",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2);

      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => Dashboard(),
        ),
      );
    } else {
      debugPrint(response.toString());
    }
  }

  //getting members using Ids and adding in list
  _listOfMembers(newIds) async {
    if (newIds.length == 0) {
      return Container();
    } else {
      for (int i = 0; i < newIds.length; i++) {
        debugPrint(newIds[i]);

        const headers = {'Content-Type': 'application/json'};
        String url =
            "https://microecommerce.flicklead.com/api/UserDetail/GetUserDetails";

        Map<String, dynamic> data = {
          "Id": newIds[i].toString(),
        };

        debugPrint("PARAMS" + data.toString());
        var response = await http.post(
          url,
          headers: headers,
          body: json.encode(data),
        );

        if (response.statusCode == 200) {
          final data = jsonDecode(response.body.replaceAll("ï»¿", ""));
          debugPrint("RESPONSE" + data.toString());
          newMembersData.add(
            UserModel.map(data),
          );
        } else {
          Fluttertoast.showToast(
              msg: "Unable to Group Members",
              backgroundColor: kAppColor,
              textColor: Colors.white);
          debugPrint(response.statusCode.toString());
        }
      }
    }
  }

  listOfUsers(List<UserModel> newMembersData) {
    debugPrint(newMembersData.toString());
    if (newMembersData.length == 0) {
      return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.all(6),
        color: Colors.grey.withOpacity(0.10),
        width: MediaQuery.of(context).size.width,
        height: 40,
        child: Text(
          "No group members added...",
          style: TextStyle(fontFamily: kOkraMedium, fontSize: 16),
        ),
      );
    } else {
      if (newMembersData.length == newIds.length) {
        return Builder(
          builder: (context) {
            return Container(
              child: ListView.separated(
                primary: false,
                shrinkWrap: true,
                itemCount: newMembersData.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      Fluttertoast.showToast(
                          msg: "Go to details page",
                          timeInSecForIosWeb: 2,
                          backgroundColor: kAppColor,
                          gravity: ToastGravity.CENTER,
                          textColor: Colors.white);
                    },
                    child: ListTile(
                      leading: CircleAvatar(
                        radius: 20,
                        backgroundColor: Colors.white,
                        child: newMembersData[index].photoId == null ||
                                newMembersData[index].photoId == ""
                            ? Image.asset(
                                "assets/images/male_avatar.png",
                                fit: BoxFit.cover,
                              )
                            : Image.network(newMembersData[index].photoId),
                        //todo  check img url here
                      ),
                      title: Text(
                        newMembersData[index].name,
                        style: TextStyle(fontFamily: kOkraSemiBold),
                      ),
                      subtitle: Text(
                        newMembersData[index].mobileNumber,
                        style: TextStyle(fontFamily: kOkraMedium),
                      ),
                      trailing: Icon(Icons.keyboard_arrow_down),
                    ),
                  );
                },
                separatorBuilder: (context, index) => Divider(
                  color: Colors.grey,
                ),
              ),
            );
          },
        );
      } else if (newMembersData.length < newIds.length) {
        return Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.all(6),
          color: Colors.grey.withOpacity(0.10),
          width: MediaQuery.of(context).size.width,
          height: 40,
          child: Text(
            "Loading members ...",
            style: TextStyle(fontFamily: kOkraMedium, fontSize: 16),
          ),
        );
      }
    }
  }
}
