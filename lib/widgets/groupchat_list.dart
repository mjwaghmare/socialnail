import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_ecommerce/models/group_model.dart';

class GroupsChatList extends StatefulWidget {
  @override
  _GroupsChatListState createState() => _GroupsChatListState();
}

class _GroupsChatListState extends State<GroupsChatList> {
  List<GroupModel> dummyData;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 10.0),
          child: Text(
            ("My Groups"),
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.grey),
          ),
        ),
        _buildGroupList(),
      ],
    );
  }

  _buildGroupList() {
    List<Widget> groupList = [];
    dummyData.forEach((GroupModel groupChatModel) {
      groupList.add(Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 15.0),
              child: ClipOval(
                child: Image(
                  height: 50.0,
                  width: 50.0,
                  image: groupChatModel.avatarUrl != ""
                      ? NetworkImage(groupChatModel.avatarUrl)
                      : AssetImage("male_avatar.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      groupChatModel.groupName,
                      style: TextStyle(
                          color: Colors.pink,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          groupChatModel.groupDescription,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.black, fontSize: 15),
                        ),
                        Text(
                          groupChatModel.createdDate,
                          style: TextStyle(color: Colors.blueGrey),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ));
    });
    return Column(children: groupList);
  }
}
