/// id : 2
/// categoryName : "technology"
///

class CategoryModel {
  int _id;
  String _categoryName;
  bool selectCategory = false;

  int get id => _id;
  String get categoryName => _categoryName;

  CategoryModel(this._id, this._categoryName, this.selectCategory);

  CategoryModel.map(dynamic obj) {
    this._id = obj["id"];
    this._categoryName = obj["categoryName"];
  }

  void setId(int id) {
    id = id;
  }

  int getId() {
    return id;
  }

  void setCategoryName(String categoryName) {
    categoryName = categoryName;
  }

  String getCategoryName() {
    return categoryName;
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = _id;
    map["categoryName"] = _categoryName;
    return map;
  }
}
