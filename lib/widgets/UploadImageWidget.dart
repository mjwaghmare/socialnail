import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_ecommerce/utils/constants.dart';

class UploadDocumentWidget extends StatelessWidget {
  final String title;
  final Function onTap;
  final TextEditingController nameOfController;
  final String fileName;
  final String sourceName;

  UploadDocumentWidget(
      {@required this.title,
      @required this.onTap,
      @required this.sourceName,
      @required this.nameOfController,
      this.fileName});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        CircleAvatar(
          radius: 25,
          backgroundColor: kAppColor,
          backgroundImage: AssetImage(
            'assets/images/upload_file.png',
          ),
        ),
        SizedBox(width: 10),
        Container(
          padding: EdgeInsets.symmetric(vertical: 5),
          width: 100,
          child: TextFormField(
            enabled: false,
            maxLength: 15,
            controller: nameOfController,
            style: TextStyle(
                color: Colors.black, fontFamily: kOkraMedium, fontSize: 15),
            decoration: InputDecoration(
              border: InputBorder.none,
              labelText: title,
              labelStyle: TextStyle(color: Colors.grey),
            ),
          ),
        ),
        SizedBox(width: 30),
        InkWell(
          onTap: onTap,
          child: Container(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            decoration: BoxDecoration(
                color: kAppColor, borderRadius: BorderRadius.circular(2)),
            child: Text(
              'Upload',
              style: TextStyle(
                  color: Colors.white, fontSize: 15, fontFamily: kOkraRegular),
            ),
          ),
        ),
      ],
    );
  }
}
