import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatsTab.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ExploreTab/ExploreTab.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/HomeTab/HomeTab.dart';
import 'package:micro_ecommerce/utils/constants.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _currentIndex = 0;
  final tabs = [HomeTab(), ChatsTab(), ExploreTab()];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: tabs[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          elevation: 0,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
          currentIndex: _currentIndex,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Image.asset(
                "assets/images/icon_navigation_home.png",
                height: 30,
                width: 30,
                color: Colors.grey,
              ),
              title: Text(
                "Home",
                style: TextStyle(color: kAppColor),
              ),
              activeIcon: Image.asset(
                  "assets/images/icon_navigation_home_active.png",
                  height: 30,
                  width: 30,
                  color: kAppColor),
            ),
            BottomNavigationBarItem(
              icon: Image.asset("assets/images/icon_navigation_chat.png",
                  height: 30, width: 30, color: Colors.grey),
              title: Text(
                "Chat",
                style: TextStyle(color: kAppColor),
              ),
              activeIcon: Image.asset(
                  "assets/images/icon_navigation_chat_active.png",
                  height: 30,
                  width: 30,
                  color: kAppColor),
            ),
            BottomNavigationBarItem(
              icon: Image.asset("assets/images/icon_navigation_explore.png",
                  height: 30, width: 30, color: Colors.grey),
              title: Text(
                "Explore",
                style: TextStyle(color: kAppColor),
              ),
              activeIcon: Image.asset(
                  "assets/images/icon_navigation_explore_active.png",
                  height: 30,
                  width: 30,
                  color: kAppColor),
            ),
          ],
        ),
      ),
      length: 3,
      initialIndex: 0,
    );
  }
}
