/// id : 3
/// title : "Demo1"
/// datOfEvent : "2020-06-15T00:00:00-07:00"
/// timeOfEvent : "2020-06-15T03:46:37-07:00"
/// googleMapLink : "https://www.google.com/maps/place/Deorashtre,+Maharashtra/@17.1613335,74.3705747,14z/data=!3m1!4b1!4m5!3m4!1s0x3bc171ee78a1329b:0xa9d22e27ffc7c0fb!8m2!3d17.1645901!4d74.3880699"
/// locationEvent : "Deorashtre"
/// agenda : "Test"
/// agendaDescription : "Test"
/// eventType : "Demo"
/// members : "1,80"
/// eventImage : "https://microecommercephoto.flicklead.com/EventPhoto/Demo1.jpg"
/// groupId : 17
/// createdByUserId : 1
/// createdDate : "2020-06-12T06:05:46-07:00"

class AddEditEventsModel {
  int _id;
  String _title;
  String _datOfEvent;
  String _timeOfEvent;
  String _googleMapLink;
  String _locationEvent;
  String _agenda;
  String _agendaDescription;
  String _eventType;
  String _members;
  String _eventImage1;
  String _eventImage2;
  String _eventImage3;
  String _eventImage4;
  String _eventImage5;
  String _eventImage6;
  int _groupId;
  int _createdByUserId;
  String _createdDate;

  int get id => _id;
  String get title => _title;
  String get datOfEvent => _datOfEvent;
  String get timeOfEvent => _timeOfEvent;
  String get googleMapLink => _googleMapLink;
  String get locationEvent => _locationEvent;
  String get agenda => _agenda;
  String get agendaDescription => _agendaDescription;
  String get eventType => _eventType;
  String get members => _members;
  String get eventImage1 => _eventImage1;
  String get eventImage2 => _eventImage2;
  String get eventImage3 => _eventImage3;
  String get eventImage4 => _eventImage4;
  String get eventImage5 => _eventImage5;
  String get eventImage6 => _eventImage6;
  int get groupId => _groupId;
  int get createdByUserId => _createdByUserId;
  String get createdDate => _createdDate;

  AddEditEventsModel(
      {int id,
      String title,
      String datOfEvent,
      String timeOfEvent,
      String googleMapLink,
      String locationEvent,
      String agenda,
      String agendaDescription,
      String eventType,
      String members,
      String eventImage1,
      String eventImage2,
      String eventImage3,
      String eventImage4,
      String eventImage5,
      String eventImage6,
      int groupId,
      int createdByUserId,
      String createdDate}) {
    _id = id;
    _title = title;
    _datOfEvent = datOfEvent;
    _timeOfEvent = timeOfEvent;
    _googleMapLink = googleMapLink;
    _locationEvent = locationEvent;
    _agenda = agenda;
    _agendaDescription = agendaDescription;
    _eventType = eventType;
    _members = members;
    _eventImage1 = eventImage1;
    _eventImage2 = eventImage2;
    _eventImage3 = eventImage3;
    _eventImage4 = eventImage4;
    _eventImage5 = eventImage5;
    _eventImage6 = eventImage6;
    _groupId = groupId;
    _createdByUserId = createdByUserId;
    _createdDate = createdDate;
  }

  AddEditEventsModel.map(dynamic obj) {
    _id = obj["id"];
    _title = obj["title"];
    _datOfEvent = obj["datOfEvent"];
    _timeOfEvent = obj["timeOfEvent"];
    _googleMapLink = obj["googleMapLink"];
    _locationEvent = obj["locationEvent"];
    _agenda = obj["agenda"];
    _agendaDescription = obj["agendaDescription"];
    _eventType = obj["eventType"];
    _members = obj["members"];
    _eventImage1 = obj["eventImage1"];
    _eventImage2 = obj["eventImage2"];
    _eventImage3 = obj["eventImage3"];
    _eventImage4 = obj["eventImage4"];
    _eventImage5 = obj["eventImage5"];
    _eventImage6 = obj["eventImage6"];
    _groupId = obj["groupId"];
    _createdByUserId = obj["createdByUserId"];
    _createdDate = obj["createdDate"];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["title"] = _title;
    map["datOfEvent"] = _datOfEvent;
    map["timeOfEvent"] = _timeOfEvent;
    map["googleMapLink"] = _googleMapLink;
    map["locationEvent"] = _locationEvent;
    map["agenda"] = _agenda;
    map["agendaDescription"] = _agendaDescription;
    map["eventType"] = _eventType;
    map["members"] = _members;
    map["eventImage1"] = _eventImage1;
    map["eventImage2"] = _eventImage2;
    map["eventImage3"] = _eventImage3;
    map["eventImage4"] = _eventImage4;
    map["eventImage5"] = _eventImage5;
    map["eventImage6"] = _eventImage6;
    map["groupId"] = _groupId;
    map["createdByUserId"] = _createdByUserId;
    map["createdDate"] = _createdDate;
    return map;
  }
}
