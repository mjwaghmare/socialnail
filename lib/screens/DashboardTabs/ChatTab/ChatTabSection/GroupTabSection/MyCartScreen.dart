import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:micro_ecommerce/models/AddEditProductsModel.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/GroupActivityScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomButton.dart';
import 'package:micro_ecommerce/widgets/CustomSingleButton.dart';

class MyCartScreen extends StatefulWidget {
  final String groupName, groupDp, groupId;
  final List<AddEditProductsModel> myCartProducts;

//  final String groupName;
  MyCartScreen(
      {this.myCartProducts, this.groupName, this.groupDp, this.groupId});

  @override
  _MyCartScreenState createState() => _MyCartScreenState();
}

class _MyCartScreenState extends State<MyCartScreen> {
  String productName, _subTotal, _radioValue, choice, appBarTitle = "Products";
  PageController _pageController = PageController();
  int _pageNumber = 0; //0 - product page,1 - address page,2 - Payment page,
  // 3 - Completed page

  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 5,
        titleSpacing: 0,
        title: Text(
          appBarTitle,
          style: TextStyle(fontFamily: kOkraMedium, color: kAppTextColor),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: kAppColor,
          ),
          onPressed: () {
            if (_pageNumber == 3) {
              setState(() {
                appBarTitle = "Payment";
                _pageNumber -= 1;
              });
              _pageController.jumpToPage(2);
            } else if (_pageNumber == 2) {
              setState(() {
                appBarTitle = "Address";
                _pageNumber -= 1;
              });
              _pageController.jumpToPage(1);
            } else if (_pageNumber == 1) {
              setState(() {
                appBarTitle = "Products";
                _pageNumber -= 1;
              });
              _pageController.jumpToPage(0);
            } else {
              widget.myCartProducts.clear();
              Navigator.of(context).pop();
            }
          },
        ),
      ),
      bottomSheet: _pageNumber == 3
          ? Container(
              height: Platform.isIOS ? 70 : 60,
              width: MediaQuery.of(context).size.width,
              child: CustomSingleButton(
                  title: "Back To Home",
                  onTapped: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => GroupActivityScreen(
                            widget.groupName, widget.groupDp, widget.groupId),
                      ),
                    );
                  }),
            )
          : Material(
              color: Colors.white,
              elevation: 5,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(11),
                    height: Platform.isIOS ? 70 : 60,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "  Sub Total: ",
                                style: TextStyle(
                                    fontFamily: kOkraSemiBold, fontSize: 15),
                              ),
                              Text(
                                "Rs. 5500",
                                style: TextStyle(
                                    fontFamily: kOkraSemiBold,
                                    fontSize: 15,
                                    color: kAppColor),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "  Tax: ",
                                style: TextStyle(
                                    fontFamily: kOkraSemiBold, fontSize: 15),
                              ),
                              Text(
                                "Rs. 50",
                                style: TextStyle(
                                    fontFamily: kOkraSemiBold,
                                    fontSize: 15,
                                    color: kAppColor),
                              ),
                            ],
                          ),
                        ]),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(2.0),
                                topRight: Radius.circular(2.0),
                              ),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.65),
                                    offset: Offset(0, 0),
                                    spreadRadius: 0.1)
                              ]),
                          height: Platform.isIOS ? 70 : 60,
                          width: MediaQuery.of(context).size.width,
                          child: Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Text(
                                      "Item(s)",
                                      style: TextStyle(
                                        fontFamily: kOkraLight,
                                        color: kAppTextColor,
                                      ),
                                    ),
                                    Text(
                                      "02",
                                      style: TextStyle(
                                          fontFamily: kOkraSemiBold,
                                          color: kAppTextColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Text(
                                      "Total :",
                                      style: TextStyle(
                                          fontFamily: kOkraLight,
                                          color: kAppTextColor),
                                    ),
                                    Text(
                                      "2500.00",
                                      style: TextStyle(
                                          fontFamily: kOkraSemiBold,
                                          color: Colors.green,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                                VerticalDivider(),
                                Container(
                                  height: 40,
                                  width: MediaQuery.of(context).size.width / 3,
                                  child: CustomButton(
                                    title: "Continue",
                                    onTapped: () {
                                      if (_pageNumber == 0) {
                                        _pageNumber += 1;
                                        _pageController.jumpToPage(_pageNumber);
                                        setState(() {
                                          appBarTitle = "Address";
                                        });
                                      } else if (_pageNumber == 1) {
                                        _pageNumber += 1;
                                        _pageController.jumpToPage(_pageNumber);
                                        setState(() {
                                          appBarTitle = "Payment";
                                        });
                                      } else if (_pageNumber == 2) {
                                        _pageNumber += 1;
                                        _pageController.jumpToPage(_pageNumber);
                                        setState(() {
                                          appBarTitle = "Order Successful";
                                        });
                                      } else if (_pageNumber == 3) {
                                        _pageNumber = 3;
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
      body: PageView(
        controller: _pageController,
        physics: new NeverScrollableScrollPhysics(), // to disable the scroll
        children: <Widget>[
          productPage(),
          shippingPage(),
          checkoutPage(),
          completedPage(),
        ],
      ),
    );
  }

  //Product Page
  Widget productPage() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image: kAppBackground, fit: BoxFit.cover),
            ),
          ),
          Column(
            children: <Widget>[
              SizedBox(height: 15),
              ProductsPageStep(),
              widget.myCartProducts.length == 0
                  ? Container(
                      height: MediaQuery.of(context).size.height * 0.6,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            "assets/images/empty_cart.png",
                            width: 250,
                            height: 250,
                          ),
                          Text(
                            "Oopps!!  Empty Cart...",
                            style: TextStyle(
                                fontFamily: kOkraMedium,
                                fontSize: 16,
                                color: kAppColor),
                          )
                        ],
                      ),
                    )
                  : Expanded(
                      child: ListView.builder(
                        primary: true,
                        shrinkWrap: true,
                        itemCount: widget.myCartProducts.length,
                        itemBuilder: (BuildContext context, int index) {
                          return cartProduct(index);
                        },
                      ),
                    ),
              SizedBox(height: 130),
            ],
          ),
        ],
      ),
    );
  }

  //Shipping Page
  Widget shippingPage() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image: kAppBackground, fit: BoxFit.cover),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 15),
              ShippingPageStep(),
              SizedBox(height: 15),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  "Select Address",
                  style: TextStyle(
                      fontFamily: kOkraSemiBold,
                      fontSize: 16,
                      fontWeight: FontWeight.w700),
                ),
              ),
              SizedBox(height: 15),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12),
                child: Card(
                  elevation: 4,
                  child: Container(
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Radio(
                              activeColor: kAppColor,
                              focusColor: kAppColor,
                              value: 'one',
                              groupValue: _radioValue,
                              onChanged: radioButtonChanges),
                          SizedBox(height: 5),
                          Expanded(
                            child: Container(
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Text(
                                      "Shewrapara,Mirpur,Dhaka - 1216",
                                      style: TextStyle(
                                          fontFamily: kOkraMedium,
                                          fontSize: 16),
                                    ),
                                    Text(
                                      "House no:938",
                                      style: TextStyle(
                                          fontFamily: kOkraMedium,
                                          fontSize: 15),
                                    ),
                                    Text(
                                      "Road no: 9",
                                      style: TextStyle(
                                          fontFamily: kOkraMedium,
                                          fontSize: 15),
                                    ),
                                  ]),
                            ),
                          ),
                        ]),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 12),
                child: Card(
                  elevation: 4,
                  child: Container(
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Radio(
                              activeColor: kAppColor,
                              focusColor: kAppColor,
                              value: 'two',
                              groupValue: _radioValue,
                              onChanged: radioButtonChanges),
                          SizedBox(height: 5),
                          Expanded(
                            child: Container(
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Text(
                                      "Chatkhil,Noakhali",
                                      style: TextStyle(
                                          fontFamily: kOkraMedium,
                                          fontSize: 16),
                                    ),
                                    Text(
                                      "House no:22",
                                      style: TextStyle(
                                          fontFamily: kOkraMedium,
                                          fontSize: 15),
                                    ),
                                    Text(
                                      "Road no: 7",
                                      style: TextStyle(
                                          fontFamily: kOkraMedium,
                                          fontSize: 15),
                                    ),
                                  ]),
                            ),
                          ),
                        ]),
                  ),
                ),
              ),
              SizedBox(height: 15),
              InkWell(
                splashColor: kAppColor,
                onTap: () {
                  Fluttertoast.showToast(
                      msg: "Add New Address",
                      backgroundColor: kAppColor,
                      textColor: Colors.white,
                      gravity: ToastGravity.CENTER);
                },
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 15),
                  child: Container(
                    child: DottedBorder(
                      dashPattern: [2.5, 5],
                      color: kAppColor,
                      strokeWidth: 2,
                      child: Container(
                        height: 50,
                        width: double.infinity,
                        child: Center(
                          child: Text(
                            "Add Address",
                            style: TextStyle(
                                fontFamily: kOkraMedium,
                                color: kAppColor,
                                fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  //Checkout Page
  Widget checkoutPage() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image: kAppBackground, fit: BoxFit.cover),
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 15),
                CheckoutPageStep(),
                SizedBox(height: 15),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Select Payment",
                        style: TextStyle(
                            fontFamily: kOkraMedium,
                            fontWeight: FontWeight.bold,
                            fontSize: 15),
                      ),
                      SizedBox(height: 10),
                      Container(
                        height: 75,
                        child: Expanded(
                          child: ListView.builder(
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              itemCount: 8,
                              itemBuilder: (context, index) {
                                return Card(
                                  elevation: 3,
                                  child: Image.asset(
                                      "assets/images/paymentupi.png",
                                      width: 70,
                                      height: 75,
                                      fit: BoxFit.fill),
                                );
                              }),
                        ),
                      ),
                      SizedBox(height: 20),
                      Text(
                        "Select Debit Card",
                        style: TextStyle(
                            fontFamily: kOkraMedium,
                            fontWeight: FontWeight.bold,
                            fontSize: 15),
                      ),
                      SizedBox(height: 10),
                      Container(
                        height: 85,
                        child: Expanded(
                          child: ListView.builder(
                              physics: BouncingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              itemCount: 4,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: EdgeInsets.only(right: 9),
                                  child: Image.asset(
                                    "assets/images/creditcard.png",
                                    width: 150,
                                    height: 75,
                                    fit: BoxFit.fill,
                                  ),
                                );
                              }),
                        ),
                      ),
                      SizedBox(height: 20),
                      Text(
                        "Add Card",
                        style: TextStyle(
                            fontFamily: kOkraMedium,
                            fontWeight: FontWeight.bold,
                            fontSize: 15),
                      ),
                      SizedBox(height: 10),
                      Card(
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6),
                        ),
                        elevation: 2,
                        child: Container(
                          padding: EdgeInsets.all(12),
                          color: Colors.white,
                          height: 150,
                          width: double.infinity,
                          child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text("Card Name"),
                                        Text(
                                          "Vipul Shah",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text("CVV #"),
                                        Text(
                                          "# # # #",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(height: 5),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text("Card Number"),
                                        Text(
                                          "XXXX - XXXX - XXXX - XXXX",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text("Expired Date"),
                                        Text(
                                          "MM/YY",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ]),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //Completed Page
  Widget completedPage() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(image: kAppBackground, fit: BoxFit.cover),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 15),
              CompletedPageStep(),
              SizedBox(height: 20),
              Container(
                  margin: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            height: 200,
                            width: 200,
                            child: FlareActor(
                              "assets/animation/paymentsuccessful.flr",
                              alignment: Alignment.center,
                              fit: BoxFit.contain,
                              animation: "Wait",
                            ),
                          )
                        /*Image.asset(
                          "assets/images/successfulcheck.png",
                          height: 150,
                          width: 150,
                        ),*/
                      ),
                      SizedBox(height: 20),
                      Text(
                        "Order Successful",
                        style: TextStyle(
                            fontFamily: kOkraMedium,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      SizedBox(height: 15),
                      Container(width: 100, height: 2, color: kAppTextColor),
                      SizedBox(height: 15),
                      Text(
                        "We know the world is full of choices.\n THANK YOU "
                        "for selecting S-Nail app",
                        style: TextStyle(
                            fontFamily: kOkraMedium,
                            fontSize: 16,
                            color: kAppTextColor),
                      ),
                      SizedBox(height: 20),
                      Container(width: 100, height: 2, color: kAppTextColor),
                      SizedBox(height: 40),
                      Text(
                        "Your order Ref is",
                        style: TextStyle(
                            fontFamily: kOkraMedium,
                            fontSize: 17,
                            color: Colors.grey),
                      ),
                      SizedBox(height: 15),
                      Text(
                        "1234567890",
                        style: TextStyle(
                            fontFamily: kOkraMedium,
                            fontSize: 16,
                            color: kAppTextColor,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  )),
            ],
          ),
        ],
      ),
    );
  }

  //Product Card Widget
  Widget cartProduct(int index) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          height: 130,
          width: double.infinity,
          child: (Row(
            children: <Widget>[
              //Product Image
              Container(
                height: 120,
                width: 120,
                padding: EdgeInsets.all(10),
                child: Image.asset(
                  'assets/images/chair.png',
                  fit: BoxFit.cover,
                ),
              ),
              //Productdetails
              Expanded(
                child: Container(
                  height: 130,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 2, vertical: 6),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                widget.myCartProducts[index].productName,
                                style: TextStyle(
                                    fontFamily: kOkraSemiBold, fontSize: 18),
                                overflow: TextOverflow.ellipsis,
                              ),
                              Text("Rs. " +
                                  widget.myCartProducts[index].price
                                      .toString()),
                              Text("Color: "),
                              Row(
                                children: <Widget>[
                                  Text("Size: "),
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(2),
                                        shape: BoxShape.rectangle,
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.55),
                                              offset: Offset(0, 0),
                                              spreadRadius: 0.4)
                                        ]),
                                    child: Text(
                                      "Medium",
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.all(1),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              GestureDetector(
                                child: Container(
                                  width: 45,
                                  height: 35,
                                  child: Image(
                                    image: AssetImage("assets/images/add.png"),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                onTap: () {
//                                            increaseQuantity(product);
                                },
                              ),
                              /*quantity > 0
                                        ? */
                              SizedBox(
                                height: 25,
                                child: Text(
//                                            quantity.toString(),
                                  "2",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontFamily: "OkraMedium"),
                                ),
                              )
                              /* : Container()*/,
                              /*quantity > 0
                                        ?*/
                              GestureDetector(
                                  child: Container(
                                    height: 35,
                                    width: 45,
                                    child: Image(
                                      image:
                                          AssetImage("assets/images/sub.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  onTap: () {
//                                              _reduceQuantity(product);
                                  })
//                                            : Container()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          )),
        ),
      ),
    );
  }

  void radioButtonChanges(String value) {
    setState(() {
      _radioValue = value;
      switch (value) {
        case 'one':
          choice = value;
          break;
        case 'two':
          choice = value;
          break;
        default:
          choice = null;
      }
      debugPrint(choice); //Debug the choice in console
    });
  }
}

//Product Step Indicator
class ProductsPageStep extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: kAppColor, //                   <---
                              // border color
                              width: 1.5,
                            ),
                            color: Colors.white,
                            shape: BoxShape.circle),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppTextColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: kAppColor, //                   <---
                              // border color
                              width: 1.5,
                            ),
                            color: Colors.white,
                            shape: BoxShape.circle),
                        child: Text(""),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppTextColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: kAppColor, //                   <---
                              // border color
                              width: 1.5,
                            ),
                            color: Colors.white,
                            shape: BoxShape.circle),
                        child: Text(""),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Products",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Shipping",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "    Checkout",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Completed",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

//Shipping Step Indicator
class ShippingPageStep extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppTextColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: kAppColor, //                   <---
                              // border color
                              width: 1.5,
                            ),
                            color: Colors.white,
                            shape: BoxShape.circle),
                        child: Text(""),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppTextColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: kAppColor, //                   <---
                              // border color
                              width: 1.5,
                            ),
                            color: Colors.white,
                            shape: BoxShape.circle),
                        child: Text(""),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Products",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Shipping",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "    Checkout",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Completed",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

//Checkout Step Indicator
class CheckoutPageStep extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                        child: Text(""),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppTextColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: kAppColor, //                   <---
                              // border color
                              width: 1.5,
                            ),
                            color: Colors.white,
                            shape: BoxShape.circle),
                        child: Text(""),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Products",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Shipping",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "    Checkout",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Completed",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

//Completed Step Indicator
class CompletedPageStep extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                        child: Text(""),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        height: 2.5,
                        color: kAppColor,
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            color: kAppColor, shape: BoxShape.circle),
                        child: Text(""),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Products",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Shipping",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "    Checkout",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Completed",
                    style: TextStyle(
                        fontSize: 13,
                        fontFamily: kOkraMedium,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
