import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:micro_ecommerce/models/PaymentCardModel.dart';
import 'package:micro_ecommerce/models/ProfileModel.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/utils/utilities.dart';
import 'package:micro_ecommerce/widgets/CustomSingleButton.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../dashboard.dart';

class MyProfileScreen extends StatefulWidget {
  @override
  _MyProfileScreenState createState() => _MyProfileScreenState();
}

class _MyProfileScreenState extends State<MyProfileScreen> {
  var _imageName, _imageString;
  TextEditingController userName = new TextEditingController();
  TextEditingController userMobileNumber = new TextEditingController();
  TextEditingController userEmail = new TextEditingController();
  TextEditingController userUsername = new TextEditingController();

  TextEditingController userCardName1 = new TextEditingController();
  TextEditingController userCardName2 = new TextEditingController();
  TextEditingController userCardName3 = new TextEditingController();
  TextEditingController userCardName4 = new TextEditingController();

  TextEditingController userCardCVV1 = new TextEditingController();
  TextEditingController userCardCVV2 = new TextEditingController();
  TextEditingController userCardCVV3 = new TextEditingController();
  TextEditingController userCardCVV4 = new TextEditingController();

  TextEditingController userCardNumber1 = new TextEditingController();
  TextEditingController userCardNumber2 = new TextEditingController();
  TextEditingController userCardNumber3 = new TextEditingController();
  TextEditingController userCardNumber4 = new TextEditingController();

  TextEditingController userCardDate1 = new TextEditingController();
  TextEditingController userCardDate2 = new TextEditingController();
  TextEditingController userCardDate3 = new TextEditingController();
  TextEditingController userCardDate4 = new TextEditingController();

  TextEditingController userAddress = new TextEditingController();
  TextEditingController userAddress1 = new TextEditingController();
  TextEditingController userAddress2 = new TextEditingController();
  TextEditingController userPinCode = new TextEditingController();
  String profileName, fileName;
  bool loading = false;
  bool editingProfile = false;
  List<ProfileModel> profileData = [];
  List<PaymentCardModel> cardList = [];
  List<DebitCard> cards = [];

  @override
  void initState() {
    _getProfileDetails();
    super.initState();
  }

  Future<void> _getProfileDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
    });
    const headers = {'Content-Type': 'application/json'};
    String url =
        "https://microecommerce.flicklead.com/api/UserDetail/GetUserDetails";

    //send the userId of admin
    Map<String, dynamic> data = {
      "Id": prefs.getString("userId"),
    };

    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body.replaceAll("ï»¿", ""));
      debugPrint(data.toString()); //response
      //response is json object
      profileData.add(ProfileModel.map(data));
      getProfileData(profileData);

      setState(() {
        loading = false;
      });
    } else {
      setState(() {
        loading = false;
      });
      Fluttertoast.showToast(msg: "Error while fetching details");
      debugPrint(response.statusCode.toString());
    }
  }

  //binding the data
  void getProfileData(List<ProfileModel> profileData) {
    for (var values in profileData) {
      profileName = values.name ?? "";
      userName = TextEditingController(text: values.name ?? "");
      userMobileNumber = TextEditingController(text: values.mobileNumber ?? "");
      userEmail = TextEditingController(text: values.emailId ?? "");
      userUsername = TextEditingController(text: values.userName ?? "");

      userCardName1 = TextEditingController(text: values.cardName1 ?? "");
      userCardCVV1 = TextEditingController(text: values.cvv1.toString() ?? "");
      userCardNumber1 = TextEditingController(text: values.cardNumber1 ?? "");
      userCardDate1 = TextEditingController(text: values.expiredDate1 ?? "");

      userCardName2 = TextEditingController(text: values.cardName2 ?? "");
      userCardCVV2 = TextEditingController(text: values.cvv2.toString() ?? "");
      userCardNumber2 = TextEditingController(text: values.cardNumber2 ?? "");
      userCardDate2 = TextEditingController(text: values.expiredDate2 ?? "");

      userCardName3 = TextEditingController(text: values.cardName3 ?? "");
      userCardCVV3 = TextEditingController(text: values.cvv3.toString() ?? "");
      userCardNumber3 = TextEditingController(text: values.cardNumber3 ?? "");
      userCardDate3 = TextEditingController(text: values.expiredDate3 ?? "");

      userCardName4 = TextEditingController(text: values.cardName4 ?? "");
      userCardCVV4 = TextEditingController(text: values.cvv4.toString() ?? "");
      userCardNumber4 = TextEditingController(text: values.cardNumber4 ?? "");
      userCardDate4 = TextEditingController(text: values.expiredDate4 ?? "");

      userAddress = TextEditingController(text: values.address ?? "");
      userAddress1 = TextEditingController(text: values.addressLine1 ?? "");
      userAddress2 = TextEditingController(text: values.addressLine2 ?? "");
      userPinCode = TextEditingController(text: values.pincode ?? "");

      _imageName = values.photoIdfileName;
    }
    debugPrint("PPUrl" + _imageName);
  }

  @override
  Widget build(BuildContext context) {
    cards.clear();
    cards.add(
      DebitCard(
          userCardName: userCardName1,
          userCardNumber: userCardNumber1,
          userCardCVV: userCardCVV1,
          userCardDate: userCardDate1),
    );
    cards.add(
      DebitCard(
          userCardName: userCardName2,
          userCardNumber: userCardNumber2,
          userCardCVV: userCardCVV2,
          userCardDate: userCardDate2),
    );
    cards.add(
      DebitCard(
          userCardName: userCardName3,
          userCardNumber: userCardNumber3,
          userCardCVV: userCardCVV3,
          userCardDate: userCardDate3),
    );
    cards.add(
      DebitCard(
          userCardName: userCardName4,
          userCardNumber: userCardNumber4,
          userCardCVV: userCardCVV4,
          userCardDate: userCardDate4),
    );

    var size = MediaQuery.of(context).size.height;
    return Scaffold(
      bottomSheet: editingProfile == false
          ? Container(
              height: Platform.isIOS ? 70 : 60,
              width: MediaQuery.of(context).size.width,
              child: CustomSingleButton(
                  title: "Save And Update",
                  onTapped: () {
                    updateProfileData();
                  }),
            )
          : Material(
              elevation: 5,
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                height: Platform.isIOS ? 70 : 60,
                child: Center(
                  child: Container(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(
                      backgroundColor: kAppColor,
                    ),
                  ),
                ),
              ),
            ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          //backgroundImage #1
          Image.asset('assets/images/background.png', fit: BoxFit.cover),
          //Container #2
          if (loading == true)
            Container(
              child: Center(
                  child: CircularProgressIndicator(backgroundColor: kAppColor)),
            )
          else
            SingleChildScrollView(
              primary: true,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: size * 0.30,
                        child: _imageName == null
                            ? Image.asset(
                                'assets/images/profilepicbanner.png',
                                fit: BoxFit.cover,
                              )
                            : FadeInImage.memoryNetwork(
                                image: _imageName,
                                placeholder: kTransparentImage,
                              ),
                      ),
                      Positioned(
                        bottom: 0,
                        top: 0,
                        right: 0,
                        left: 0,
                        child: Container(
                          width: double.infinity,
                          height: size * 0.30,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Colors.transparent,
                                  Colors.transparent,
                                  Colors.transparent,
                                  Colors.pink[300]
                                ]),
                          ),
                        ),
                      ),
                      Positioned(
                          bottom: 14,
                          right: 0,
                          left: 0,
                          child: profileName != null ||
                                  profileName != "" ||
                                  profileName != "null"
                              ? Container(
                                  alignment: Alignment.bottomCenter,
                                  width: double.infinity,
                                  height: size * 0.30,
                                  child: Text(
                                    profileName == null ? "" : profileName,
                                    style: TextStyle(
                                        fontFamily: kOkraMedium,
                                        color: Colors.white,
                                        fontSize: 20),
                                  ),
                                )
                              : Container()),
                      Positioned(
                        bottom: 12,
                        right: 12,
                        child: Row(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                _getImage();
                                Fluttertoast.showToast(
                                    msg: "Upload profile pic");
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RichText(
                                  text: TextSpan(
                                    children: [
                                      WidgetSpan(
                                        child: Icon(
                                          Icons.file_upload,
                                          size: 18,
                                          color: Colors.white,
                                        ),
                                      ),
                                      TextSpan(
                                        text: " Upload",
                                        style: TextStyle(
                                            fontFamily: kOkraRegular,
                                            color: Colors.white,
                                            fontSize: 13),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Profile Details",
                          style: TextStyle(
                              fontFamily: kOkraRegular,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                        //Name
                        SizedBox(height: 15),
                        TextField(
                          controller: userName,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontFamily: kOkraMedium),
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: kAppTextColor),
                            ),
                            labelText: "Name",
                            labelStyle: TextStyle(
                              fontFamily: kOkraRegular,
                              fontSize: 16,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        //Mobile Number
                        SizedBox(height: 15),
                        TextField(
                          keyboardType: TextInputType.number,
                          controller: userMobileNumber,
                          maxLength: 10,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontFamily: kOkraMedium),
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: kAppTextColor),
                            ),
                            labelText: "Mobile number",
                            labelStyle: TextStyle(
                              fontFamily: kOkraRegular,
                              fontSize: 16,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        //Email
                        SizedBox(height: 15),
                        TextField(
                          keyboardType: TextInputType.emailAddress,
                          controller: userEmail,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontFamily: kOkraMedium),
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: kAppTextColor),
                            ),
                            labelText: "Email",
                            labelStyle: TextStyle(
                              fontFamily: kOkraRegular,
                              fontSize: 16,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        //UserName
                        SizedBox(height: 15),
                        TextField(
                          onTap: () {
                            if (userEmail.text.trim().length != 0 &&
                                RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(userEmail.text.trim())) {
                              String _newUsername =
                                  Utils.generateUsername(userEmail.text.trim());
                              userUsername.text = _newUsername;
                            } else {
                              userUsername.text = "";
                              Fluttertoast.showToast(
                                  msg: "Please enter valid "
                                      "emailId",
                                  backgroundColor: kAppColor,
                                  textColor: Colors.white,
                                  gravity: ToastGravity.CENTER);
                            }
                          },
                          controller: userUsername,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontFamily: kOkraMedium),
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: kAppTextColor),
                            ),
                            labelText: "UserName",
                            labelStyle: TextStyle(
                              fontFamily: kOkraRegular,
                              fontSize: 16,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        //My Payment Option
                        SizedBox(height: 15),
                        Text(
                          "My Payment Options",
                          style: TextStyle(
                              fontFamily: kOkraRegular,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10),
                        //Payment cards list
                        buildPaymentCardContainer(),
                        SizedBox(height: 15),
                        Text(
                          "Address details",
                          style: TextStyle(
                              fontFamily: kOkraMedium,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                        //Address
                        SizedBox(height: 10),
                        TextField(
                          controller: userAddress,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontFamily: kOkraMedium),
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: kAppTextColor),
                            ),
                            labelText: "Address",
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 16),
                          ),
                        ),
                        //Address Line 1
                        SizedBox(height: 10),
                        TextField(
                          controller: userAddress1,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontFamily: kOkraMedium),
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kAppTextColor),
                              ),
                              labelText: "Address Line 1",
                              labelStyle:
                                  TextStyle(color: Colors.grey, fontSize: 16)),
                        ),
                        //Address Line 2
                        SizedBox(height: 10),
                        TextField(
                          controller: userAddress2,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontFamily: kOkraMedium),
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: kAppTextColor),
                            ),
                            labelText: "Address Line 2",
                            labelStyle:
                                TextStyle(color: Colors.grey, fontSize: 16),
                          ),
                        ),
                        //Pin code
                        SizedBox(height: 10),
                        TextField(
                          controller: userPinCode,
                          maxLength: 6,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontFamily: kOkraMedium),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kAppTextColor),
                              ),
                              labelText: "Pin code",
                              labelStyle:
                                  TextStyle(color: Colors.grey, fontSize: 16)),
                        ),
                      ],
                    ),
                  ),
                  //==================
                  SizedBox(height: 70),
                ],
              ),
            )
        ],
      ),
    );
  }

  Container buildPaymentCardContainer() {
    return Container(
      padding: EdgeInsets.only(top: 2.0, bottom: 2, right: 2),
      height: 220,
      width: double.infinity,
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: cards.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return DebitCard(
                userCardName: cards[index].userCardName,
                userCardCVV: cards[index].userCardCVV,
                userCardNumber: cards[index].userCardNumber,
                userCardDate: cards[index].userCardDate);
          }),
    );
  }

  //Image Picker
  Future _getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      File croppedImg = await ImageCropper.cropImage(
          sourcePath: image.path,
          compressQuality: 100,
          maxWidth: MediaQuery.of(context).size.width.toInt(),
          compressFormat: ImageCompressFormat.jpg,
          androidUiSettings: AndroidUiSettings(
              toolbarColor: kAppColor,
              toolbarTitle: "SocialNail Image Cropper",
              backgroundColor: Colors.white,
              lockAspectRatio: false,
              showCropGrid: true),
          iosUiSettings: IOSUiSettings(
            minimumAspectRatio: 1.0,
          ));

      String base64Image = base64Encode(
        image.readAsBytesSync(),
      );
      fileName = image.path.split("/").last;
      _imageString = base64Image;
      /*this.setState(
        () {
          _imageName = croppedImg;
        },
      );*/
    }
  }

  Future<void> updateProfileData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String imgName;
    if (fileName == null) {
      imgName = "";
    } else {
      imgName = Utils.generateImagename(fileName);
      debugPrint(imgName);
      debugPrint("===========");
    }
    setState(() {
      editingProfile = true;
    });
    const headers = {'Content-Type': 'application/json'};
    String url =
        "https://microecommerce.flicklead.com/api/UserDetail/DetailProfileCreation";

    Map<String, dynamic> data = {
      "Id": prefs.getString("userId"),
      "Name": userName.text,
      "MobileNumber": userMobileNumber.text,
      "EmailId": userEmail.text,
      "UserName": userName.text,

      //Card1
      "CardName1": userCardName1.text,
      "CardNumber1": userCardNumber1.text,
      "Cvv1": userCardCVV1.text,
      "ExpiredDate1": userCardDate1.text,

      //Card2
      "CardName2": userCardName2.text,
      "CardNumber2": userCardNumber2.text,
      "Cvv2": userCardCVV2.text,
      "ExpiredDate2": userCardDate2.text,

      //Card3
      "CardName3": userCardName3.text,
      "CardNumber3": userCardNumber3.text,
      "Cvv3": userCardCVV3.text,
      "ExpiredDate3": userCardDate3.text,

      //Card4
      "CardName4": userCardName4.text,
      "CardNumber4": userCardNumber4.text,
      "Cvv4": userCardCVV4.text,
      "ExpiredDate4": userCardDate4.text,

      "Address": userAddress.text,
      "addressLine1": userAddress1.text,
      "addressLine2": userAddress2.text,
      "Pincode": userPinCode.text,
      "PhotoIdfileName": imgName,
      "PhotoId": _imageString == null ? "" : _imageString.toString(),
    };

    var response = await http.put(
      url,
      headers: headers,
      body: json.encode(data),
    );

    debugPrint("Profile_Param" + data.toString());
    //1st api response
    if (response.statusCode == 200) {
      Utils.setName(userName.text);

      String urlImg =
          "https://microecommercephoto.flicklead.com/api/UserPhoto/savephoto";
      Map<String, dynamic> data = {
        "imgname": fileName == null ? "" : Utils.generateImagename(fileName),
        "img1": _imageString == null ? "" : _imageString,
      };

      var responseImg = await http.post(
        urlImg,
        headers: headers,
        body: json.encode(data),
      );

      //2nd api response
      if (responseImg.statusCode == 200) {
        setState(() {
          editingProfile = false;
        });

        Fluttertoast.showToast(msg: "Profile Edited Successfully...");
        Navigator.of(context).pop(
          MaterialPageRoute(
            builder: (BuildContext context) => Dashboard(),
          ),
        );
      } else {
        setState(() {
          editingProfile = false;
        });
      }
    } else {
      setState(() {
        editingProfile = false;
      });
    }
  }
}

class DebitCard extends StatelessWidget {
  const DebitCard({
    Key key,
    @required this.userCardName,
    @required this.userCardCVV,
    @required this.userCardNumber,
    @required this.userCardDate,
  }) : super(key: key);

  final TextEditingController userCardName;
  final TextEditingController userCardCVV;
  final TextEditingController userCardNumber;
  final TextEditingController userCardDate;

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
      ),
      elevation: 2,
      child: Container(
        height: 200,
        width: 280,
        padding: EdgeInsets.all(14),
        color: Colors.white,
        child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          "Card Name",
                          style: TextStyle(color: Colors.grey),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 20),
                          height: 55,
                          child: TextField(
                            controller: userCardName,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 17,
                                fontFamily: kOkraMedium),
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kAppTextColor),
                              ),
                              hintText: "Card Name holder",
                              hintStyle: TextStyle(
                                fontFamily: kOkraRegular,
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                            maxLines: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          "CVV #",
                          style: TextStyle(color: Colors.grey),
                        ),
                        Container(
                          height: 55,
                          child: TextField(
                            controller: userCardCVV,
                            maxLength: 3,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 17,
                                fontFamily: kOkraMedium),
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kAppTextColor),
                              ),
                              hintText: "###",
                              hintStyle: TextStyle(
                                fontFamily: kOkraRegular,
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text("Card Number"),
                        Container(
                          height: 55,
                          child: TextField(
                            controller: userCardNumber,
                            maxLength: 12,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 17,
                                fontFamily: kOkraMedium),
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kAppTextColor),
                              ),
                              hintText: "XXXX - XXXX - XXXX",
                              hintStyle: TextStyle(
                                fontFamily: kOkraRegular,
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                            maxLines: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text("Expired Date"),
                        Container(
                          height: 55,
                          child: TextField(
                            controller: userCardDate,
                            maxLength: 5,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 17,
                                fontFamily: kOkraMedium),
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kAppTextColor),
                              ),
                              hintText: "MM/YY",
                              hintStyle: TextStyle(
                                fontFamily: kOkraRegular,
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                            maxLines: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ]),
      ),
    );
  }
}
