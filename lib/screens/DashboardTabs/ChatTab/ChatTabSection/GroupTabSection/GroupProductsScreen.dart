import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/models/AddEditProductsModel.dart';
import 'package:micro_ecommerce/models/CategoryModel.dart';
import 'package:micro_ecommerce/models/Products_Model.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/ProductDetailsScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomButton.dart';

import 'CategoryFilterScreen.dart';
import 'MyCartScreen.dart';

List<AddEditProductsModel> myCartProducts = [];

class GroupProductsScreen extends StatefulWidget {
  final String groupName, groupDp, groupId;
  final List<AddEditProductsModel> filteredProductList;

  GroupProductsScreen(
      {this.groupName, this.groupDp, this.groupId, this.filteredProductList});

  @override
  _GroupProductsScreenState createState() => _GroupProductsScreenState();
}

class _GroupProductsScreenState extends State<GroupProductsScreen> {
//  int quantity = 0;
  final productData = prodData;
  int _currentQty = 0;
  List<AddEditProductsModel> addedProducts = [];
  bool loadingProduct = false;
  bool loadingCategories = false;
  List<CategoryModel> category = [];
  List<int> filterCategory = [];
  List<String> categoriesList = [];
  bool borderColor = false;
  bool categorySelected = false;
  bool selectCategory = false;
  String _selectedCategoryValue = "All";

  @override
  void initState() {
    super.initState();
    //checking if the filteredProductList from categoryFilter is null or not,
    //if null show all product
    //else show the products from the filtered list from CategoryFilter
    // screen...

    /*widget.filteredProductList == null
        ? _getAllProducts(widget.groupName)
        : addedProducts = widget.filteredProductList;*/

    addedProducts.clear();
    getCategoryItems();
    _getAllProducts(widget.groupName);
  }

  Future<void> _getAllProducts(String groupName) async {
    addedProducts.clear();
    setState(() {
      loadingProduct = true;
    });

    //code to get all products
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/Productdetails/AllProductdetails";

    Map<String, dynamic> data = {
      'GroupName': groupName,
    };
    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      debugPrint("RESPONSE" + jsonResponse.toString());

      setState(() {
        loadingProduct = false;
      });
      for (Map i in jsonResponse) {
        addedProducts.add(AddEditProductsModel.map(i));
      }
    } else {
      setState(() {
        loadingProduct = false;
      });
      debugPrint(response.statusCode.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: kAppBackground, fit: BoxFit.cover),
          ),
          height: screenHeight,
          width: screenWidth,
          child: Container(
            margin: EdgeInsets.all(7.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //Search Box
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Material(
                        shadowColor: Colors.grey,
                        borderRadius: BorderRadius.circular(2),
                        color: Colors.transparent,
                        elevation: 2,
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                          ),
                          height: 40,
                          child: TextField(
                            textAlign: TextAlign.left,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.grey,
                              ),
                              hintText: "Search",
                              hintStyle: TextStyle(
                                  fontSize: 17,
                                  fontFamily: kOkraLight,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 5),
                    InkWell(
                      splashColor: kAppColor,
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CategoryFilterScreen(),
                          ),
                        );
                      },
                      child: Material(
                        shadowColor: Colors.grey,
                        color: Colors.white,
                        elevation: 2,
                        borderRadius: BorderRadius.circular(4),
                        child: Container(
                          height: 40,
                          width: 40,
                          alignment: Alignment.center,
                          child: Icon(
                            Icons.filter_list,
                            size: 25,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                //CategoryHeading
                SizedBox(height: 20.0),
                category.length == 0
                    ? Container()
                    : Text("Categories",
                        style: TextStyle(
                            fontFamily: kOkraMedium,
                            fontSize: 15,
                            fontWeight: FontWeight.w600)),
                //Category list
                SizedBox(height: 10.0),
                loadingCategories == true
                    ? Container(
                        height: 35,
                        child: Center(
                          child: CircularProgressIndicator(
                              backgroundColor: kAppColor),
                        ),
                      )
                    : Container(height: 35, child: categoryTile(category)),
                //Products Grid
                SizedBox(height: 30.0),
                loadingProduct == false
                    ? Expanded(
                        child: Container(
                          child: GridView.count(
                            primary: false,
                            childAspectRatio: 0.8,
                            crossAxisCount: 2,
                            mainAxisSpacing: 20,
                            crossAxisSpacing: 4,
                            children: <Widget>[
                              ...addedProducts.map((f) {
                                return productCard(f);
                              }).toList()
                            ],
                          ),
                        ),
                      )
                    : Center(
                        child: CircularProgressIndicator(
                          backgroundColor: kAppColor,
                        ),
                      ),
                SizedBox(height: 50.0),
              ],
            ),
          ),
        ),
      ),
      bottomSheet: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(2.0),
                    topRight: Radius.circular(2.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.65),
                        offset: Offset(0, 0),
                        spreadRadius: 0.1)
                  ]),
              height: Platform.isIOS ? 70 : 60,
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Text(
                                  "Item(s)",
                                  style: TextStyle(
                                    fontFamily: kOkraLight,
                                    color: kAppTextColor,
                                  ),
                                ),
                                Text(
                                  "$_currentQty",
                                  style: TextStyle(
                                      fontFamily: kOkraSemiBold,
                                      color: kAppTextColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Text(
                                  "Total :",
                                  style: TextStyle(
                                      fontFamily: kOkraLight,
                                      color: kAppTextColor),
                                ),
                                Text(
                                  "2500.00",
                                  style: TextStyle(
                                      fontFamily: kOkraSemiBold,
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ],
                            ),
                          ),
                          VerticalDivider(),
                          Container(
                            height: 40,
                            width: MediaQuery.of(context).size.width / 3,
                            child: CustomButton(
                              title: "Go To Cart",
                              onTapped: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => MyCartScreen(
                                      myCartProducts: myCartProducts,
                                      groupName: widget.groupName,
                                      groupDp: widget.groupDp,
                                      groupId: widget.groupId,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  productCard(AddEditProductsModel product) {
    return Card(
      elevation: 2,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      child: Padding(
        padding: const EdgeInsets.all(1.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center, //today
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ProductDetailScreen(product),
                  ),
                );
              },
              child: Hero(
                tag: product.id,
                child: Container(
                  margin: const EdgeInsets.all(4.0),
                  height: MediaQuery.of(context).size.height * .16,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(8),
                        topLeft: Radius.circular(8)),
                  ),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/spinner.gif",
                    image: product.productImage1,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0, vertical: 4.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Text(
                      product.productName,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: "OkraMedium",
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Rs." + product.price.toString(),
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontFamily: "OkraMedium",
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                        CounterRow(product: product),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Row counter(AddEditProductsModel product) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        GestureDetector(
          child: Container(
            width: 35,
            height: 25,
            child: Image(
              image: AssetImage("assets/images/add.png"),
              fit: BoxFit.cover,
            ),
          ),
          onTap: () {
            debugPrint("increase");
            setState(() {
              myCartProducts.add(product);
              _currentQty += 1;
            });
          },
        ),
        _currentQty > 0
            ? SizedBox(
                height: 25,
                child: Text(
                  "$_currentQty",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontFamily: "OkraMedium"),
                ),
              )
            : Container(),
        _currentQty > 0
            ? GestureDetector(
                child: Container(
                  height: 25,
                  width: 35,
                  child: Image(
                    image: AssetImage("assets/images/sub.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                onTap: () {
                  if (_currentQty == 0) {
                    return;
                  } else {
                    setState(() {
                      myCartProducts.remove(product);
                      _currentQty -= 1;
                    });
                  }
                })
            : Container()
      ],
    );
  }

  Widget categoryTile(List<CategoryModel> category) {
    return Container(
      color: Colors.transparent,
      width: double.infinity,
      height: 30,
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: category.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                if (_selectedCategoryValue != category[index].categoryName) {
                  _selectedCategoryValue = category[index].categoryName;
                  addedProducts.clear();
                  filteredProduct(_selectedCategoryValue);
                } else if (_selectedCategoryValue ==
                    category[index].categoryName) {
                  _selectedCategoryValue = category[index].categoryName;
                  addedProducts.clear();
                  filteredProduct(_selectedCategoryValue);
                } else if (_selectedCategoryValue == "All") {
                  addedProducts.clear();
                  _getAllProducts(widget.groupName);
                }
              },
              child: Material(
                elevation: 1,
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  padding: EdgeInsets.symmetric(horizontal: 12, vertical: 1),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color:
                        _selectedCategoryValue != category[index].categoryName
                            ? Colors.white.withOpacity(1)
                            : Colors.white,
                    border: Border.all(
                      width:
                          _selectedCategoryValue != category[index].categoryName
                              ? 1
                              : 1.5,
                      color:
                          _selectedCategoryValue != category[index].categoryName
                              ? Colors.grey.withOpacity(0.6)
                              : kAppColor,
                    ),
                  ),
                  child: Text(
                    category[index].categoryName,
                    style: TextStyle(
                        fontFamily: kOkraRegular,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: _selectedCategoryValue !=
                                category[index].categoryName
                            ? Colors.grey
                            : Colors.black),
                  ),
                ),
              ),
            );
          }),
    );
  }

  //get all categories items
  Future<void> getCategoryItems() async {
    setState(() {
      loadingCategories = true;
    });
    category.clear();
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/categorydetails/AllCategoryDetails";

    var response = await http.get(
      url,
      headers: headers,
    );

    if (response.statusCode == 200) {
      setState(() {
        loadingCategories = false;
      });
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));
      for (Map i in jsonResponse) {
        category.add(CategoryModel.map(i));
      }
    } else {
      setState(() {
        loadingCategories = false;
      });
      debugPrint(response.statusCode.toString());
    }
  }

  Future<void> filteredProduct(String category) async {
    addedProducts.clear();
    setState(() {
      loadingProduct = true;
    });

    //code to get filtered products
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/Productdetails/ProductDetailsbyCategory";

    Map<String, dynamic> data = {
      "Category": category,
    };

    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    if (response.statusCode == 200) {
      var jsonResponse = json.decode(response.body.replaceAll("ï»¿", ""));

      setState(() {
        loadingProduct = false;
      });

      for (Map i in jsonResponse) {
        addedProducts.add(AddEditProductsModel.map(i));
      }
    } else {
      setState(() {
        loadingProduct = false;
      });
      debugPrint(response.statusCode.toString());
    }
  }
}

class CounterRow extends StatefulWidget {
  final AddEditProductsModel product;

  CounterRow({Key key, this.product}) : super(key: key);

  @override
  _CounterRowState createState() => _CounterRowState();
}

class _CounterRowState extends State<CounterRow> {
  int _currentQty = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        GestureDetector(
          child: Container(
            width: 35,
            height: 25,
            child: Image(
              image: AssetImage("assets/images/add.png"),
              fit: BoxFit.cover,
            ),
          ),
          onTap: () {
            debugPrint("increase");
            setState(() {
              myCartProducts.add(widget.product);
              _currentQty += 1;
            });
          },
        ),
        _currentQty > 0
            ? SizedBox(
                height: 25,
                child: Text(
                  "$_currentQty",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontFamily: "OkraMedium"),
                ),
              )
            : Container(),
        _currentQty > 0
            ? GestureDetector(
                child: Container(
                  height: 25,
                  width: 35,
                  child: Image(
                    image: AssetImage("assets/images/sub.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                onTap: () {
                  if (_currentQty == 0) {
                    return;
                  } else {
                    setState(() {
                      myCartProducts.remove(widget.product);
                      _currentQty -= 1;
                    });
                  }
                })
            : Container()
      ],
    );
  }
}

/*distributionUrl=https\://services.gradle.org/distributions/gradle-5.6.4-all.zip
*/
