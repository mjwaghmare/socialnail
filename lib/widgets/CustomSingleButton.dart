import 'dart:io';

import 'package:flutter/material.dart';
import 'package:micro_ecommerce/utils/constants.dart';

class CustomSingleButton extends StatelessWidget {
  final String title;
  final Function onTapped;
  CustomSingleButton({this.title, this.onTapped});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 10,
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        height: Platform.isIOS ? 70 : 60,
        child: Container(
          color: Colors.white,
          child: GestureDetector(
            onTap: onTapped,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 90, vertical: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [
                        Colors.pink[300],
                        Colors.pink[300],
                        Colors.pink[400],
                        Colors.pink[400],
                      ])),
              child: Center(
                child: Text(
                  title,
                  style: TextStyle(
                    fontFamily: kOkraMedium,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Colors.white,
                    letterSpacing: 2.0,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
