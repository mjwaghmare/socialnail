import 'package:flutter/material.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/HomeTab/MyProfileScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/utils/utilities.dart';

import '../../login_screen.dart';
import 'creategroup_screen.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  ScrollController _scrollController;
  Color _theme;
  bool _pic;

//  List<int> eventMembersList;

  @override
  void initState() {
    super.initState();
    _theme = Colors.white;
    _pic = false; //not expanded

    _scrollController = ScrollController()
      ..addListener(
        () => _isAppBarExpanded
            ? _theme != kAppColor
                ? setState(
                    () {
                      _pic = true;
                      _theme = kAppColor;
                      debugPrint('setState is called');
                    },
                  )
                : {}
            : _theme != Colors.white
                ? setState(() {
                    _pic = false;
                    debugPrint('setState is called');
                    _theme = Colors.white;
                  })
                : {},
      );
  }

  bool get _isAppBarExpanded {
    return _scrollController.hasClients &&
        _scrollController.offset > (200 - kToolbarHeight);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              backgroundColor:
                  _pic == false ? Colors.transparent : Colors.white,
              leading: _pic == false
                  ? Container()
                  : Container(
                      child: Image.asset(
                        'assets/images/logonew2.png',
                        fit: BoxFit.cover,
                      ),
                    ),
              actions: <Widget>[
                PopupMenuButton(
                  icon: _pic == false
                      ? Icon(
                          Icons.add,
                          size: 25,
                          color: Colors.white,
                        )
                      : Material(
                          color: Colors.white,
                          elevation: 5,
                          child: Container(
                              margin: EdgeInsets.all(1),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8),
                                  shape: BoxShape.rectangle,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.white.withOpacity(0.55),
                                        offset: Offset(1, 1),
                                        spreadRadius: 1)
                                  ]),
                              child: Icon(
                                Icons.add,
                                size: 25,
                                color: kAppColor,
                              )),
                        ),
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    PopupMenuItem<String>(
                      value: 'group creation',
                      child: Text(
                        'Group Creation',
                        style: TextStyle(fontSize: 15, fontFamily: kOkraMedium),
                      ),
                    ),
                    /* PopupMenuItem<String>(
                      value: 'add event',
                      child: Text(
                        'Add Event',
                        style: TextStyle(fontSize: 15, fontFamily: kOkraMedium),
                      ),
                    )*/
                  ],
                  onSelected: (value) {
                    if (value == "group creation") {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (BuildContext context) => CreateGroup()),
                      );
                    }
                    /*else if (value == "add event") {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AddEditEventListScreen()),
                      );
                    }*/
                  },
                ),
                Icon(
                  Icons.notifications,
                  color: _theme,
                ),
                SizedBox(width: 10),
                PopupMenuButton(
                  icon: Icon(
                    Icons.more_vert,
                    size: 25,
                    color: _theme,
                  ),
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    PopupMenuItem<String>(
                      value: 'My Profile',
                      child: Text(
                        'My Profile',
                        style: TextStyle(fontSize: 15, fontFamily: kOkraMedium),
                      ),
                    ),
                    PopupMenuItem<String>(
                      value: 'Logout',
                      child: Text(
                        'Logout',
                        style: TextStyle(fontSize: 15, fontFamily: kOkraMedium),
                      ),
                    ),
                  ],
                  onSelected: (value) {
                    if (value == "My Profile") {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                MyProfileScreen()),
                      );
                    } else if (value == "Logout") {
                      Utils.setIsLoggedOutFlag();
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (BuildContext context) => LoginScreen(),
                          ),
                          (Route<dynamic> route) => false);
                    }
                  },
                ),
                SizedBox(width: 5),
              ],
              expandedHeight: MediaQuery.of(context).size.height * 0.40,
              pinned: true,
              floating: false,
              flexibleSpace: FlexibleSpaceBar(
                background: Image.network(
                  "https://images.pexels.com/photos/807598/pexels-photo-807598.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ];
        },
        body: Center(
          child: Text('Home'),
        ),
      ),
    );
  }
}
