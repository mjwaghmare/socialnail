/// id : 2
/// categoryName : "technology"
///

class SizeModel {
  int _id;
  String _sizeName;
  bool selectedSize = false;

  int get id => _id;
  String get sizeName => _sizeName;

  SizeModel(this._id, this._sizeName);

  SizeModel.map(dynamic obj) {
    this._id = obj["id"];
    this._sizeName = obj["size"];
  }

  void setId(int id) {
    id = id;
  }

  int getId() {
    return id;
  }

  void setsizeName(String sizeName) {
    sizeName = sizeName;
  }

  String getSizeName() {
    return sizeName;
  }

  void setSelectedSize(bool selectedSize) {
    selectedSize = selectedSize;
  }

  bool getSelectedSize() {
    return selectedSize;
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = _id;
    map["sizeName"] = _sizeName;
    return map;
  }
}
