import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/CustomSingleButton.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../dashboard.dart';
import 'AddMembersToEventsScreen.dart';

class AddEvent extends StatefulWidget {
  final String grpId;

  AddEvent({Key key, this.grpId}) : super();

  @override
  _AddEventState createState() => _AddEventState();
}

class _AddEventState extends State<AddEvent> {
  bool _addingEvent = false;
  TextEditingController titleController = new TextEditingController();
  TextEditingController locationController = new TextEditingController();
  TextEditingController agendaController = new TextEditingController();
  TextEditingController agendaDescController = new TextEditingController();
  TextEditingController dateController = new TextEditingController();
  TextEditingController timeController = new TextEditingController();

  String eventImage1,
      eventImage2,
      eventImage3,
      eventImage4,
      eventImage5,
      eventImage6;

  //Event Type
  var _eventTypes = ['Select Event Type', 'Free', 'Paid', 'Premium'];
  var _currentEventType = 'Select Event Type';

  List<File> eventImages = List<File>();
  List<String> eventImagesBase64 = <String>[];
  List<int> eventMembersList = [];

  DateTime _date;
  TimeOfDay _time = TimeOfDay.now();
  TimeOfDay _pickedTime;
  LocationResult _pickedLocation;

  Future<Null> selectTime(BuildContext context) async {
    _pickedTime = await showTimePicker(context: context, initialTime: _time);
    setState(() {
      _time = _pickedTime;
      String eventTime = _time.hour.toString() + ":" + _time.minute.toString();
      timeController =
          new TextEditingController(text: _time == null ? "" : eventTime);
    });
  }

  selectDate() {
    showDatePicker(
        firstDate: DateTime.now(),
        lastDate: DateTime(2099),
        initialDate: _date == null ? DateTime.now() : _date,
        context: context)
        .then((date) {
      setState(() {});
      _date = date;
      String eventDate = _date.year.toString() +
          "-" +
          _date.month.toString() +
          "-" +
          _date.day.toString();
      dateController =
          TextEditingController(text: _date == null ? "" : eventDate);
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery
        .of(context)
        .size;
    return Scaffold(
      bottomSheet: _addingEvent == false
          ? CustomSingleButton(title: "Add Event", onTapped: addEvent)
          : Material(
        elevation: 5,
        child: Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          color: Colors.white,
          height: Platform.isIOS ? 70 : 60,
          child: Center(
            child: Container(
              height: 30,
              width: 30,
              child: CircularProgressIndicator(
                backgroundColor: kAppColor,
              ),
            ),
          ),
        ),
      ),
      appBar: AppBar(
        title: Text(
          "Add Event",
          style: TextStyle(
              fontFamily: kOkraMedium, fontSize: 18, color: kAppTextColor),
        ),
        titleSpacing: 0.0,
        elevation: 5.0,
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: kAppColor,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            "assets/images/background.png",
            fit: BoxFit.cover,
          ),
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //Event Title
                  editText(titleController, "Title"),
                  //Date of Event
                  SizedBox(height: 15),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: dateController,
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: kOkraMedium,
                              color: kAppTextColor),
                          decoration: InputDecoration(
                              labelText: "Date of event",
                              labelStyle: TextStyle(color: Colors.grey)),
                          enabled: false,
                        ),
                      ),
                      IconButton(
                        icon:
                        Icon(Icons.date_range, color: kAppColor, size: 18),
                        onPressed: () {
                          selectDate();
                        },
                      )
                    ],
                  ),

                  //Time of Event
                  SizedBox(height: 15),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: timeController,
                          style: TextStyle(
                              fontSize: 18,
                              fontFamily: kOkraMedium,
                              color: kAppTextColor),
                          decoration: InputDecoration(
                              labelText: "Time of event",
                              labelStyle: TextStyle(color: Colors.grey)),
                          enabled: false,
                        ),
                      ),
                      IconButton(
                        icon:
                        Icon(Icons.access_time, color: kAppColor, size: 18),
                        onPressed: () {
                          selectTime(context);
                        },
                      )
                    ],
                  ),

                  //Google Map Link
                  SizedBox(height: 15),
                  Text(
                    "Google Map Link",
                    style: TextStyle(
                        color: kAppTextColor, fontFamily: kOkraMedium),
                  ),
                  InkWell(
                    onTap: () async {
                      LocationResult result = await showLocationPicker(
                          context, mapApi,
                          automaticallyAnimateToCurrentLocation: true,
                          myLocationButtonEnabled: true,
                          layersButtonEnabled: false,
                          requiredGPS: true,
                          resultCardAlignment: Alignment.bottomCenter,
                          hintText: "Search Location");
                      debugPrint("result = $result");
                      setState(() => _pickedLocation = result);
                      debugPrint(_pickedLocation.toString());
                    },
                    child: Text("//map code here..."),
                  ),

                  //Location Event
                  SizedBox(height: 10),
                  editText(locationController, "Event Location"),

                  //Agenda
                  editText(agendaController, "Agenda"),

                  //Agenda Description
                  editText(agendaDescController, "Agenda Description"),

                  //Select EventType Spinner
                  SizedBox(height: 15),
                  eventTypeSpinner(),

                  //Add Members Spinner
                  SizedBox(height: 15),
                  Text(
                    "Add Members",
                    style: TextStyle(
                        color: kAppTextColor, fontFamily: kOkraMedium),
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  AddMembersToEventsScreen(),
                            ),
                          );
                        },
                        child: RichText(
                          text: TextSpan(
                            children: [
                              WidgetSpan(
                                child: Icon(
                                  Icons.group_add,
                                  size: 18,
                                  color: kAppColor,
                                ),
                              ),
                              TextSpan(
                                text: " Click to add members in event.",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: kOkraMedium,
                                    color: Colors.grey),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),

                  //Event Images
                  SizedBox(height: 20),
                  Text(
                    "Event images ",
                    style: TextStyle(
                        color: kAppTextColor, fontFamily: kOkraMedium),
                  ),
                  SizedBox(height: 10),
                  eventImagesPicker(),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      height: eventImages.length > 0 ? size.height * .3 : 5.0,
                      child: buildGridView(eventImages)),
                  //extra height
                  SizedBox(height: 50),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> addEvent() async {
    setState(() {
      _addingEvent = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String eventImg;
    int len = eventImagesBase64.length;
    int i = 0;
    for (var v = 0; v < len; v++) {
      try {
        eventImg = eventImagesBase64[i];
      } on Exception catch (e) {
        debugPrint(e.toString());
      }
      if (i == 0) {
        eventImage1 = eventImg;
      } else if (i == 1) {
        eventImage2 = eventImg;
      } else if (i == 2) {
        eventImage3 = eventImg;
      } else if (i == 3) {
        eventImage4 = eventImg;
      } else if (i == 4) {
        eventImage5 = eventImg;
      } else if (i == 5) {
        eventImage6 = eventImg;
      }
      i++;
    }
    const headers = {'Content-Type': 'application/json'};
    String url =
        "https://microecommerce.flicklead.com/api/EventDetails/CreateEvent";

    Map<String, dynamic> data = {
      "CreatedByUserId": prefs.getString("userId"), //userId
      "Title": titleController.text,
      "DatOfEvent": dateController.text,
      "TimeOfEvent": dateController.text + " , " + timeController.text,
      "GoogleMapLink": "na",
      "LocationEvent": locationController.text,
      "Agenda": agendaController.text,
      "AgendaDescription": agendaDescController.text,
      "EventType": _currentEventType,
      "Members": "",
      "EventImage1": eventImage1,
      "EventImage2": eventImage2,
      "EventImage3": eventImage3,
      "EventImage4": eventImage4,
      "EventImage5": eventImage5,
      "EventImage6": eventImage6,
      "GroupId": widget.grpId, //new
      "AttendingMembers": "", //new
      "DeclineMembers": "", //new
    };

    debugPrint("=================================");

    debugPrint(data.toString());

    debugPrint("=================================");
    var response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    debugPrint("1st api response" + response.statusCode.toString());
    //1st api response
    if (response.statusCode == 200) {
      debugPrint(response.body.toString());

      String urlImg =
          "https://microecommercephoto.flicklead.com/api/EventPhoto/savephoto";
      Map<String, dynamic> data = {
        "imgname": titleController.text,
        "img1": eventImage1 == null ? "" : eventImage1,
        "img2": eventImage2 == null ? "" : eventImage2,
        "img3": eventImage3 == null ? "" : eventImage3,
        "img4": eventImage4 == null ? "" : eventImage4,
        "img5": eventImage5 == null ? "" : eventImage5,
        "img6": eventImage6 == null ? "" : eventImage6,
      };

      var responseImg = await http.post(
        urlImg,
        headers: headers,
        body: json.encode(data),
      );

      //2nd api response
      debugPrint("2nd api response" + responseImg.statusCode.toString());
      if (responseImg.statusCode == 200) {
        setState(() {
          _addingEvent = false;
        });
        Fluttertoast.showToast(
            msg: "Event created Successfully",
            textColor: Colors.white,
            backgroundColor: kAppColor,
            gravity: ToastGravity.CENTER);

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => Dashboard()),
                (Route<dynamic> route) => false);
      } else {
        setState(() {
          _addingEvent = false;
        });
      }
    } else {
      setState(() {
        _addingEvent = false;
      });
      debugPrint(response.statusCode.toString());
    }
  }

  Widget editText(TextEditingController controller, String labelText) {
    return TextField(
      controller: controller,
      style: TextStyle(
          fontSize: 18, fontFamily: kOkraMedium, color: kAppTextColor),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: kAppTextColor),
        ),
        labelText: labelText,
        labelStyle: TextStyle(color: Colors.grey),
      ),
    );
  }

  Widget eventTypeSpinner() {
    return Container(
      width: double.infinity,
      child: DropdownButton<String>(
        icon: Icon(
          Icons.keyboard_arrow_down,
        ),
        iconSize: 25,
        iconEnabledColor: Colors.grey,
        items: _eventTypes.map((String dropDownStringItem) {
          return DropdownMenuItem<String>(
            value: dropDownStringItem,
            child: Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width - 90,
              child: Text(
                dropDownStringItem,
                style: TextStyle(color: Colors.black, fontFamily: kOkraMedium),
              ),
            ),
          );
        }).toList(),
        onChanged: (value) {
          _eventItemSelected(value);
        },
        value: _currentEventType,
      ),
    );
  }

  void _eventItemSelected(String value) {
    setState(() {
      this._currentEventType = value;
    });
  }

  eventImagesPicker() {
    return InkWell(
      splashColor: kAppColor,
      onTap: () {
        _galleryPhotoIdImagePicker();
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: RichText(
          textAlign: TextAlign.justify,
          text: TextSpan(
            children: [
              WidgetSpan(
                child: Icon(
                  Icons.add_photo_alternate,
                  size: 20,
                  color: Colors.grey,
                ),
              ),
              TextSpan(
                text: " Click here to select images (max 6)",
                style: TextStyle(
                    fontFamily: kOkraSemiBold,
                    color: Colors.grey,
                    fontSize: 14),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future _galleryPhotoIdImagePicker() async {
    File galleryFile;
    galleryFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (galleryFile == null) return;
    String base64Image = base64Encode(galleryFile.readAsBytesSync());
    eventImages.add(galleryFile);
    eventImagesBase64.add(base64Image);
    setState(() {
      buildGridView(eventImages);
    });
  }

  Widget buildGridView(List<File> eventImages) {
    return GridView.count(
      primary: false,
      crossAxisCount: 3,
      children: List.generate(eventImages.length, (index) {
        File file = eventImages[index];
        return Stack(
          children: <Widget>[
            Card(
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(4)),
                child: Container(
                  width: 100,
                  height: 100,
                  child: Image.file(
                    file,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 6,
              right: 6,
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    eventImages.removeAt(index);
                  });
                },
                child: Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.85),
                          offset: Offset(0, 0),
                          spreadRadius: 0)
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Icon(
                      Icons.close,
                      size: 15,
                      color: kAppColor,
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      }),
    );
  }
}
