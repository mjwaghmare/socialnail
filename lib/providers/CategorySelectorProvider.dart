import 'package:flutter/foundation.dart';

/**Created by "Manoj Waghmare" on 28,Jul,2020 **/

class CategorySelectorProvider extends ChangeNotifier {
  String _selectedCategory = "All";

  String get _getSelectedCategory => _selectedCategory;

  void setSelectedCategory(String value) {
    _selectedCategory = value;
    notifyListeners();
  }
}
