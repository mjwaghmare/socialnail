import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:micro_ecommerce/models/group_model.dart';
import 'package:micro_ecommerce/screens/DashboardTabs/ChatTab/ChatTabSection/GroupTabSection/GroupActivityScreen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/utils/utilities.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GroupsTab extends StatefulWidget {
  @override
  _GroupsTabState createState() => _GroupsTabState();
}

class _GroupsTabState extends State<GroupsTab> {
  @override
  void initState() {
    super.initState();
    fetchGroupsList();
  }

  List<GroupModel> groupListData = [];
  bool loading = false;
  var time;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                "assets/images/background.png",
              ),
              fit: BoxFit.cover),
        ),
        child: ListView(
          shrinkWrap: false,
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            loading
                ? Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 2,
                    child: Center(
                      child: CircularProgressIndicator(
                        backgroundColor: kAppColor,
                      ),
                    ),
                  )
                : groupsChatListView()
          ],
        ),
      ),
    );
  }

  Future<void> fetchGroupsList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    debugPrint(prefs.getString("userId"));
    setState(() {
      loading = true;
    });
    const headers = {'Content-Type': 'application/json'};
    String url =
        "http://microecommerce.flicklead.com/api/Groupdetails/AllGroupDetails";

    Map<String, dynamic> data = {"CreatedBy": prefs.getString("userId")};

    final response = await http.post(
      url,
      headers: headers,
      body: json.encode(data),
    );

    debugPrint(response.statusCode.toString());
    debugPrint(response.body);

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body.replaceAll("ï»¿", ""));
      setState(() {
        for (Map i in data) {
          groupListData.add(GroupModel.fromJson(i));
          debugPrint(groupListData.toString());
        }
        loading = false;
      });
    } else {
      setState(() {
        loading = false;
        Fluttertoast.showToast(
            msg: "No Response from server",
            backgroundColor: kAppColor,
            textColor: Colors.white);
      });
      debugPrint(response.statusCode.toString());
    }
    debugPrint(groupListData.toString());
  }

  Widget groupsChatListView() {
    return Container(
      child: _buildGroupList(),
    );
  }

  _buildGroupList() {
    List<Widget> groupList = [];
    groupListData.forEach((GroupModel groupChatModel) {
      time = Utils.convertTime(groupChatModel.createdDate);
      groupList.add(
        Container(
          padding: EdgeInsets.symmetric(horizontal: 4.0, vertical: 2.0),
          child: GestureDetector(
            onTap: () {
              debugPrint("tapped on group");
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => GroupActivityScreen(
                    groupChatModel.groupName,
                    groupChatModel.avatarUrl,
                    groupChatModel.id.toString(),
                  ),
                ),
              );
            },
            child: Container(
              height: 70,
              decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.55),
                        offset: Offset(0, 1),
                        spreadRadius: 0.4)
                  ]),
              padding: EdgeInsets.all(8),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: ClipOval(
                      child: Image(
                        height: 50.0,
                        width: 50.0,
                        image: groupChatModel.avatarUrl == null ||
                                groupChatModel.avatarUrl == ""
                            ? AssetImage('assets/images/male_avatar.png')
                            : NetworkImage(groupChatModel.avatarUrl),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(width: 8.0),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          groupChatModel.groupName,
                          style: TextStyle(
                              color: kAppTextColor,
                              fontSize: 16.0,
                              fontFamily: kOkraSemiBold),
                        ),
                        Text(
                          groupChatModel.groupDescription,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: kAppTextColor,
                            fontSize: 14.0,
                            fontFamily: kOkraRegular,
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(width: 8.0),
                  Text(
                    time,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 14.0,
                    ),
                  ),
                  SizedBox(width: 8.0),
                ],
              ),
            ),
          ),
        ),
      );
    });
    return Column(children: groupList);
  }
}
