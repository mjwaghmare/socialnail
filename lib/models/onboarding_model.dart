class SliderModel {
  String imagePath, title, desc;

  SliderModel({this.imagePath, this.title, this.desc});

  void setImageAssetPath(String getImagepath) {
    imagePath = getImagepath;
  }

  void setTitle(String getTitle) {
    title = getTitle;
  }

  void setDesc(String getDesc) {
    desc = getDesc;
  }

  String getImageAssetPath() {
    return imagePath;
  }

  String getTitle() {
    return title;
  }

  String getDesc() {
    return desc;
  }
}

List<SliderModel> getSlides() {
  List<SliderModel> slides = new List<SliderModel>();
  SliderModel sliderModel = new SliderModel();

  //1 Screen
  sliderModel.setTitle("SocialLane");
  sliderModel.setImageAssetPath("assets/images/illustration.png");
  sliderModel
      .setDesc("The smarter and easy way to Chat and sell your products.");
  slides.add(sliderModel);
  sliderModel = new SliderModel();

  //2 Screen
  sliderModel.setTitle("SocialLane");
  sliderModel.setImageAssetPath("assets/images/onboardind2.png");
  sliderModel.setDesc(
      "Easy to create groups and events in your profile and get notifications.");
  slides.add(sliderModel);
  sliderModel = new SliderModel();

  //3 Screen
  sliderModel.setTitle("SocialLane");
  sliderModel.setImageAssetPath("assets/images/onboardind3.png");
  sliderModel.setDesc(
      "Create your own E-commerce groups and sell products hassle free.");
  slides.add(sliderModel);
  sliderModel = new SliderModel();

  return slides;
}
