import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:micro_ecommerce/screens/dashboard.dart';
import 'package:micro_ecommerce/screens/login_screen.dart';
import 'package:micro_ecommerce/utils/constants.dart';
import 'package:micro_ecommerce/widgets/ButtonBottomSheet.dart';

class EnterOtp extends StatefulWidget {
  final String id;

  EnterOtp({Key key, this.id}) : super(key: key);

  @override
  _EnterOtpState createState() => _EnterOtpState();
}

class _EnterOtpState extends State<EnterOtp> {
  TextEditingController mailOtpController = new TextEditingController();
  TextEditingController mobileOtpController = new TextEditingController();
  FirebaseAuth _auth = FirebaseAuth.instance;
  String errorMessage = '';

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        bottomSheet: ButtonBottomSheet(
          title1: "Next",
          onTapped1: () async {
            if (mailOtpController.toString().isEmpty &&
                mobileOtpController.toString().isEmpty) {
              Fluttertoast.showToast(
                  msg: "Please enter both Otps.",
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 2);
              return;
            } else {
              try {
                final otp = mobileOtpController.text.trim().toString();
                if (otp.isNotEmpty) {
                  AuthCredential credential = PhoneAuthProvider.credential(
                      smsCode: otp, verificationId: null);
                  UserCredential result =
                      await _auth.signInWithCredential(credential);

                  User user = result.user;

                  //also check here the email otp verified response in if
                  // condition...
                  if (user != null) {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (BuildContext context) => Dashboard()),
                        (Route<dynamic> route) => false);
                  } else {
                    Fluttertoast.showToast(
                        msg: "Something went wrong. Please check the Otp",
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 2);
                  }
                }
              } catch (e) {
                handleError(e);
              }
            }
          },
          title2: "Login",
          onTapped2: () {
            Navigator.pop(context,
                MaterialPageRoute(builder: (context) => LoginScreen()));
          },
        ),
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: Image.asset(
                  "assets/images/background.png",
                  fit: BoxFit.fill,
                ).image),
              ),
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 40,
                    left: 20,
                    right: 20,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.8,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0, 1),
                            blurRadius: 1.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 100,
                    left: 0,
                    right: 0,
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 45),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset("assets/images/logo.png"),
                          SizedBox(height: 30),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                "Enter register details for ",
                                style: GoogleFonts.openSans(
                                  fontSize: 14.0,
                                ),
                              ),
                              Text(
                                "SocialNail ",
                                style: GoogleFonts.openSans(
                                  fontSize: 14.0,
                                  color: kAppColor,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Text(
                                "with us.",
                                style: GoogleFonts.openSans(
                                  fontSize: 14.0,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 50),
                          sectionEnterMailOtp(
                              "Enter Email Otp", Icons.phonelink_lock),
                          SizedBox(height: 50),
                          sectionEnterMobileOtp(
                              "Enter Mobile Otp", Icons.phonelink_lock),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  sectionEnterMobileOtp(String title, IconData iconData) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: TextFormField(
        maxLength: 6,
        keyboardType: TextInputType.number,
        controller: mobileOtpController,
        style: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w400, fontSize: 10),
        decoration: InputDecoration(
          labelText: title,
          prefixIcon: Icon(iconData),
        ),
      ),
    );
  }

  sectionEnterMailOtp(String title, IconData iconData) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: TextFormField(
        maxLength: 6,
        keyboardType: TextInputType.number,
        controller: mailOtpController,
        style: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w400, fontSize: 20),
        decoration: InputDecoration(
          labelText: title,
          prefixIcon: Icon(iconData),
        ),
      ),
    );
  }

  handleError(PlatformException error) {
    debugPrint(error.toString());
    switch (error.code) {
      case 'ERROR_INVALID_VERIFICATION_CODE':
        mobileOtpController.clear();
        Fluttertoast.showToast(
            msg: "OTP is invalid. Enter correct OTP",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 2);
        break;
      default:
        setState(() {
          errorMessage = error.message;
        });

        break;
    }
  }
}
