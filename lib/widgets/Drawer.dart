import 'package:flutter/material.dart';

class DrawerMenu extends StatelessWidget {
  DrawerMenu({this.menuIcon, this.menuName, this.menuSubTitle, this.onPress});

  final Function onPress;
  final IconData menuIcon;
  final String menuName;
  final String menuSubTitle;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black26, offset: Offset(0, 2), blurRadius: 5.0),
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Icon(
              menuIcon,
              size: 40,
            ),
            Text(
              menuName,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 16.0,
                  color: Colors.pinkAccent),
            ),
            Text(
              menuSubTitle,
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 11.0,
                  color: Colors.blueGrey),
            ),
          ],
        ),
      ),
    );
  }
}
