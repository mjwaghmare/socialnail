/// id : 81
/// name : "Suyash"
/// mobileNumber : "8888370203"
/// emailId : "sp@gmail.com"
/// userName : "yash"
/// password : "11234"
/// termAndCondition : "true"
/// photoIdfileName : "suyash.jpg"
/// photoId : null
/// panAdharFileName : null
/// panAdharPhoto : null
/// panAdharNumber : null
/// panAdharName : null
/// entryDate : "2020-05-09T02:26:51-07:00"
/// apkVersionNo : "1.3"
/// apkVersionEntryDate : "2020-05-09T02:26:51-07:00"
/// deviceName : "Oppo"
/// deviceOs : "8.0"
/// address : null
/// addressLine1 : null
/// addressLine2 : null
/// pincode : null
/// cardName : null
/// cardNumber : null
/// cvv : null
/// expiredDate : "2020-05-09T02:26:51-07:00"
/// "userRole": "Admin"

class ProfileModel {
  int _id;
  String _name;
  String _mobileNumber;
  String _emailId;
  String _userName;
  String _password;
  String _termAndCondition;
  String _photoIdfileName;
  dynamic _photoId;
  dynamic _panAdharFileName;
  dynamic _panAdharPhoto;
  dynamic _panAdharNumber;
  dynamic _panAdharName;
  String _entryDate;
  String _apkVersionNo;
  String _apkVersionEntryDate;
  String _deviceName;
  String _deviceOs;
  dynamic _address;
  dynamic _addressLine1;
  dynamic _addressLine2;
  dynamic _pincode;

  dynamic _cardName1, _cardName2, _cardName3, _cardName4;
  dynamic _cardNumber1, _cardNumber2, _cardNumber3, _cardNumber4;
  dynamic _cvv1, _cvv2, _cvv3, _cvv4;
  String _expiredDate1, _expiredDate2, _expiredDate3, _expiredDate4;

  dynamic get cardName1 => _cardName1;

  set cardName1(dynamic value) {
    _cardName1 = value;
  }

  String _userRole;

  int get id => _id;

  String get name => _name;

  String get mobileNumber => _mobileNumber;

  String get emailId => _emailId;

  String get userName => _userName;

  String get password => _password;

  String get termAndCondition => _termAndCondition;

  String get photoIdfileName => _photoIdfileName;

  dynamic get photoId => _photoId;

  dynamic get panAdharFileName => _panAdharFileName;

  dynamic get panAdharPhoto => _panAdharPhoto;

  dynamic get panAdharNumber => _panAdharNumber;

  dynamic get panAdharName => _panAdharName;

  String get entryDate => _entryDate;

  String get apkVersionNo => _apkVersionNo;

  String get apkVersionEntryDate => _apkVersionEntryDate;

  String get deviceName => _deviceName;

  String get deviceOs => _deviceOs;

  dynamic get address => _address;

  dynamic get addressLine1 => _addressLine1;

  dynamic get addressLine2 => _addressLine2;

  dynamic get pincode => _pincode;

  /*dynamic get cardName => _cardName1;

  dynamic get cardNumber => _cardNumber;

  dynamic get cvv => _cvv;

  String get expiredDate => _expiredDate;*/

  String get userRole => _userRole;

  ProfileModel(
      this._id,
      this._name,
      this._mobileNumber,
      this._emailId,
      this._userName,
      this._password,
      this._termAndCondition,
      this._photoIdfileName,
      this._photoId,
      this._panAdharFileName,
      this._panAdharPhoto,
      this._panAdharNumber,
      this._panAdharName,
      this._entryDate,
      this._apkVersionNo,
      this._apkVersionEntryDate,
      this._deviceName,
      this._deviceOs,
      this._address,
      this._addressLine1,
      this._addressLine2,
      this._pincode,
      this._userRole);

  ProfileModel.map(dynamic obj) {
    this._id = obj["id"];
    this._name = obj["name"];
    this._mobileNumber = obj["mobileNumber"];
    this._emailId = obj["emailId"];
    this._userName = obj["userName"];
    this._password = obj["password"];
    this._termAndCondition = obj["termAndCondition"];
    this._photoIdfileName = obj["photoIdfileName"];
    this._photoId = obj["photoId"];
    this._panAdharFileName = obj["panAdharFileName"];
    this._panAdharPhoto = obj["panAdharPhoto"];
    this._panAdharNumber = obj["panAdharNumber"];
    this._panAdharName = obj["panAdharName"];
    this._entryDate = obj["entryDate"];
    this._apkVersionNo = obj["apkVersionNo"];
    this._apkVersionEntryDate = obj["apkVersionEntryDate"];
    this._deviceName = obj["deviceName"];
    this._deviceOs = obj["deviceOs"];
    this._address = obj["address"];
    this._addressLine1 = obj["addressLine1"];
    this._addressLine2 = obj["addressLine2"];
    this._pincode = obj["pincode"];
    this._cardName1 = obj["cardName1"];
    this._cardName2 = obj["cardName2"];
    this._cardName3 = obj["cardName3"];
    this._cardName4 = obj["cardName4"];

    this._cardNumber1 = obj["cardNumber1"];
    this._cardNumber2 = obj["cardNumber2"];
    this._cardNumber3 = obj["cardNumber3"];
    this._cardNumber4 = obj["cardNumber4"];

    this._cvv1 = obj["cvv1"];
    this._cvv2 = obj["cvv2"];
    this._cvv3 = obj["cvv3"];
    this._cvv4 = obj["cvv4"];

    this._expiredDate1 = obj["expiredDate1"];
    this._expiredDate2 = obj["expiredDate2"];
    this._expiredDate3 = obj["expiredDate3"];
    this._expiredDate4 = obj["expiredDate4"];

    this._userRole = obj["userRole"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = _id;
    map["name"] = _name;
    map["mobileNumber"] = _mobileNumber;
    map["emailId"] = _emailId;
    map["userName"] = _userName;
    map["password"] = _password;
    map["termAndCondition"] = _termAndCondition;
    map["photoIdfileName"] = _photoIdfileName;
    map["photoId"] = _photoId;
    map["panAdharFileName"] = _panAdharFileName;
    map["panAdharPhoto"] = _panAdharPhoto;
    map["panAdharNumber"] = _panAdharNumber;
    map["panAdharName"] = _panAdharName;
    map["entryDate"] = _entryDate;
    map["apkVersionNo"] = _apkVersionNo;
    map["apkVersionEntryDate"] = _apkVersionEntryDate;
    map["deviceName"] = _deviceName;
    map["deviceOs"] = _deviceOs;
    map["address"] = _address;
    map["addressLine1"] = _addressLine1;
    map["addressLine2"] = _addressLine2;
    map["pincode"] = _pincode;

    map["cardName1"] = _cardName1;
    map["cardName2"] = _cardName2;
    map["cardName3"] = _cardName3;
    map["cardName4"] = _cardName4;

    map["cardNumber1"] = _cardNumber1;
    map["cardNumber2"] = _cardNumber2;
    map["cardNumber3"] = _cardNumber3;
    map["cardNumber4"] = _cardNumber4;

    map["cvv1"] = _cvv1;
    map["cvv2"] = _cvv2;
    map["cvv3"] = _cvv3;
    map["cvv4"] = _cvv4;

    map["expiredDate1"] = _expiredDate1;
    map["expiredDate2"] = _expiredDate2;
    map["expiredDate3"] = _expiredDate3;
    map["expiredDate4"] = _expiredDate4;

    map["userRole"] = _userRole;
    return map;
  }

  get cardName2 => _cardName2;

  set cardName2(value) {
    _cardName2 = value;
  }

  get cardName3 => _cardName3;

  set cardName3(value) {
    _cardName3 = value;
  }

  get cardName4 => _cardName4;

  set cardName4(value) {
    _cardName4 = value;
  }

  dynamic get cardNumber1 => _cardNumber1;

  set cardNumber1(dynamic value) {
    _cardNumber1 = value;
  }

  get cardNumber2 => _cardNumber2;

  set cardNumber2(value) {
    _cardNumber2 = value;
  }

  get cardNumber3 => _cardNumber3;

  set cardNumber3(value) {
    _cardNumber3 = value;
  }

  get cardNumber4 => _cardNumber4;

  set cardNumber4(value) {
    _cardNumber4 = value;
  }

  dynamic get cvv1 => _cvv1;

  set cvv1(dynamic value) {
    _cvv1 = value;
  }

  get cvv2 => _cvv2;

  set cvv2(value) {
    _cvv2 = value;
  }

  get cvv3 => _cvv3;

  set cvv3(value) {
    _cvv3 = value;
  }

  get cvv4 => _cvv4;

  set cvv4(value) {
    _cvv4 = value;
  }

  String get expiredDate1 => _expiredDate1;

  set expiredDate1(String value) {
    _expiredDate1 = value;
  }

  get expiredDate2 => _expiredDate2;

  set expiredDate2(value) {
    _expiredDate2 = value;
  }

  get expiredDate3 => _expiredDate3;

  set expiredDate3(value) {
    _expiredDate3 = value;
  }

  get expiredDate4 => _expiredDate4;

  set expiredDate4(value) {
    _expiredDate4 = value;
  }
}
